import jwtDecode from 'jwt-decode';
import axios from 'src/utils/axios';
import { client } from 'src/utils/apollo'
import gql from 'graphql-tag'

const LOGIN = gql`
  mutation login($loginInput: LoginInput!){
    login(loginInput: $loginInput) {
      token
    }
  }
`

const AUTHEN = gql`
  query findAuthenUser{
  findAuthenUser{
    _id
    idRole
    username
    urlAvatar
    firstname
    lastname
    email
    numberphone
    isVerified
    isLocked
    createdAt
    createdBy
    updatedAt
    updatedBy
  }
}
`

class AuthService {
  setAxiosInterceptors = ({ onLogout }) => {
    axios.interceptors.response.use(
      (response) => response,
      (error) => {
        if (error.response && error.response.status === 401) {
          this.setSession(null);

          if (onLogout) {
            onLogout();
          }
        }

        return Promise.reject(error);
      }
    );
  };

  handleAuthentication() {
    const accessToken = this.getAccessToken();

    if (!accessToken) {
      return;
    }

    if (this.isValidToken(accessToken)) {
      this.setSession(accessToken);
    } else {
      this.setSession(null);
    }
  }

  loginWithEmailAndPassword = (email, password) => new Promise((resolve, reject) => {
    client.mutate({
      mutation: LOGIN,
      variables: {
        loginInput: {
          username: email,
          password
        }
      }
    })
      .then((response) => {
        if (response.data.login) {
          this.setSession(response.data.login.token);
          resolve(response.data.login.token);
        } else {
          reject(response.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  })

  loginInWithToken = () => new Promise((resolve, reject) => {
    client.query({
      query: AUTHEN,
      fetchPolicy: 'network-only'
    })
      .then((response) => {
        if (response.data.findAuthenUser) {
          resolve(response.data.findAuthenUser);
        } else {
          reject(response.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  })

  logout = () => {
    this.setSession(null);
  }

  setSession = (accessToken) => {
    if (accessToken) {
      localStorage.setItem('accessToken', accessToken);
      axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    } else {
      localStorage.removeItem('accessToken');
      delete axios.defaults.headers.common.Authorization;
    }
  }

  getAccessToken = () => localStorage.getItem('accessToken');

  isValidToken = (accessToken) => {
    if (!accessToken) {
      return false;
    }

    const decoded = jwtDecode(accessToken);
    const currentTime = Date.now() / 1000;

    return decoded.exp > currentTime;
  }

  isAuthenticated = () => !!this.getAccessToken()
}

const authService = new AuthService();

export default authService;
