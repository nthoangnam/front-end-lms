import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormHelperText,
  Grid,
  Switch,
  TextField,
  Typography,
  makeStyles,
  FormControlLabel
} from '@material-ui/core';
import { updateProfile } from 'src/actions/accountActions';
import gql from 'graphql-tag'
import { useMutation, useLazyQuery } from 'react-apollo';

const useStyles = makeStyles(() => ({
  root: {}
}));

const CREATE_ONE_REQUEST = gql`
  mutation createOneRequest($inputRequest: InputRequest!){
    createOneRequest(inputRequest: $inputRequest)
  }
`

const FIND_MANY_REQUEST = gql`
  query findManyRequest($inputRequest: InputRequest!){
    findManyRequest(inputRequest: $inputRequest) {
      _id
      idUser
      isDenied
      isAccepted
    }
  }
`

function GeneralSettings({ user, className, ...rest }) {
  const classes = useStyles();
  const [isChangePassword, setIsChangePassword] = useState(false)
  const [createOneRequest] = useMutation(CREATE_ONE_REQUEST)
  const [findManyRequest, { data: findManyRequestResponse, called: findManyRequestCalled, refetch: findManyRequestRefetch }] = useLazyQuery(FIND_MANY_REQUEST)
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const handleCheckChangePass = useCallback(() => {
    setIsChangePassword(p => !p)
  }, [])

  const handleRequest = useCallback((values, {
    resetForm,
    setErrors,
    setStatus,
    setSubmitting
  }) => {
    console.log({ user })
    if (!user) {
      return
    }
    if (isChangePassword) {
      delete values.passwordConfirm
    }
    createOneRequest({
      variables: {
        inputRequest: {
          idUser: user._id,
          updateUser: values
        }
      }
    })
      .then(res => {
        const { data } = res
        if (data.createOneRequest) {
          if (findManyRequestCalled) {
            findManyRequestRefetch()
          }
          setSubmitting(false)
          enqueueSnackbar(
            'Congratulations! Creating course successfully.',
            {
              variant: 'success',
              anchorOrigin: {
                horizontal: 'center',
                vertical: 'top'
              }
            }
          )
        }
      })
      .catch(() => {
        enqueueSnackbar(
          'Sorry! Something happens error.',
          {
            variant: 'error',
            anchorOrigin: {
              horizontal: 'center',
              vertical: 'top'
            }
          }
        )
      })
  }, [user, findManyRequestCalled, isChangePassword])

  useEffect(() => {
    if (!user) {
      return
    }
    findManyRequest({
      variables: {
        inputRequest: {
          idUser: user._id,
          isDenied: false,
          isAccepted: false
        }
      }
    })
  }, [user])

  return (
    <Formik
      enableReinitialize
      initialValues={{
        ...isChangePassword ? {
          passwordConfirm: '',
          password: ''
        } : {},
        address: user.address || '',
        username: user.username || '',
        email: user.email || '',
        firstname: user.firstname || '',
        lastname: user.lastname || '',
        numberphone: user.numberphone || '',
      }}
      validationSchema={Yup.object().shape({
        username: Yup.string().max(255).required('Username is required'),
        address: Yup.string().max(255).required('Address is required'),
        email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
        numberphone: Yup.string().max(255).required('Number phone is required'),
        firstname: Yup.string().max(255).required('First name is required'),
        lastname: Yup.string().max(255).required('Last name is required'),
        ...isChangePassword ? {
          password: Yup.string().max(255).required('Password is required'),
          passwordConfirm: Yup.string().max(255).required('Password confirm is required')
        } : {}
      })}
      onSubmit={handleRequest}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form onSubmit={handleSubmit}>
            <Card
              className={clsx(classes.root, className)}
              {...rest}
            >
              <CardHeader title="Profile" />
              <Divider />
              <CardContent>
                <Grid
                  container
                  spacing={4}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.firstname && errors.firstname)}
                      fullWidth
                      helperText={touched.firstname && errors.firstname}
                      label="First Name"
                      name="firstname"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      type="firstname"
                      value={values.firstname}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.lastname && errors.lastname)}
                      fullWidth
                      helperText={touched.lastname && errors.lastname}
                      label="Last Name"
                      name="lastname"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      type="lastname"
                      value={values.lastname}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.address && errors.address)}
                      fullWidth
                      helperText={touched.address && errors.address}
                      label="Address"
                      name="address"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.address}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.username && errors.username)}
                      fullWidth
                      helperText={touched.username && errors.username}
                      label="Username"
                      name="username"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.username}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email ? errors.email : 'We will use this email to contact you'}
                      label="Email Address"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      type="email"
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.numberphone && errors.numberphone)}
                      fullWidth
                      helperText={touched.numberphone && errors.numberphone}
                      label="Phone Number"
                      name="numberphone"
                      required
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.numberphone}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isChangePassword}
                          onChange={handleCheckChangePass}
                          name="checkedB"
                          color="primary"
                        />
                      }
                      label="Change Password"
                    />
                  </Grid>
                  {isChangePassword && (
                    <>
                      <Grid
                        item
                        sm={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.password && errors.password)}
                          fullWidth
                          helperText={touched.password && errors.password}
                          label="Password"
                          name="password"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="password"
                          value={values.password}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        sm={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.passwordConfirm && errors.passwordConfirm)}
                          fullWidth
                          helperText={touched.passwordConfirm && errors.passwordConfirm}
                          label="Password Confirmation"
                          name="passwordConfirm"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type="password"
                          value={values.passwordConfirm}
                          variant="outlined"
                        />
                      </Grid>
                    </>
                  )}
                </Grid>
                {errors.submit && (
                  <Box mt={3}>
                    <FormHelperText error>
                      {errors.submit}
                    </FormHelperText>
                  </Box>
                )}
              </CardContent>
              <Divider />
              <Box
                p={2}
                display="flex"
                justifyContent="flex-end"
              >
                <Button
                  color="secondary"
                  disabled={findManyRequestResponse && findManyRequestResponse.findManyRequest.length > 0}
                  type="submit"
                  variant="contained"
                >
                  {`${findManyRequestResponse && findManyRequestResponse.findManyRequest.length > 0 ? "Requested" : "Request"}`}
                </Button>
              </Box>
            </Card>
          </form>
        )}
    </Formik>
  );
}

GeneralSettings.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object.isRequired
};

export default GeneralSettings;
