import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Typography,
  Breadcrumbs,
  Link,
  makeStyles
} from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import styled from '@emotion/styled'

const useStyles = makeStyles(() => ({
  root: {
    marginLeft: '12px',
    '& a': {
      color: '#fff'
    }
  }
}));

function Header({ className, ...rest }) {
  const classes = useStyles();

  return (
    <Wrapper
      className={clsx(classes.root, className)}
      {...rest}
    >
      <BreadcrumbsCustom
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        <Link color="inherit" to="/app" component={RouterLink}>
          Dashboard
        </Link>
        <Typography color="textPrimary">

        </Typography>
      </BreadcrumbsCustom>
      <Typography
        variant="h3"
        color="textPrimary"
      >
        Course Management
      </Typography>
    </Wrapper>
  );
}

Header.propTypes = {
  className: PropTypes.string
};

export default Header;

const Wrapper = styled.div`
  z-index: 10;
`

const BreadcrumbsCustom = styled(Breadcrumbs)`
  color: #fff;
`
