import { Box, Grid, makeStyles, SvgIcon, Typography } from '@material-ui/core';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import {
  AlertTriangle as AlertIcon,
  Calendar as CalendarIcon,
  Check as CheckIcon
} from 'react-feather';
// import Application from './Application';
import { useSelector } from 'react-redux';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';

const useStyles = makeStyles(theme => ({
  root: {},
  badge: {
    display: 'flex',
    alignItems: 'center',
    margin: theme.spacing(2)
  },
  badgeIcon: {
    marginRight: theme.spacing(1)
  },
  action: {
    marginBottom: theme.spacing(1),
    '& + &': {
      marginLeft: theme.spacing(1)
    }
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

function Header({
  setHaveAVideo,
  haveAVideo,
  setUpload,
  title,
  setTitle,
  createOneMedia,
  data,
  project,
  className,
  ...rest
}) {
  const { user } = useSelector(state => state.account);
  const classes = useStyles();
  const [openApplication, setOpenApplication] = useState(false);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const GET_DETAIL_EXAM = gql`
    query findOneExamination($idExamination: ID!) {
      findOneExamination(idExamination: $idExamination) {
        _id
        idLesson
        idContentLesson
        title
      }
    }
  `;

  console.log(window.location.pathname.slice(101));

  const { data: lesson } = useQuery(GET_DETAIL_EXAM, {
    variables: {
      idExamination: window.location.pathname.slice(101)
    }
  });

  console.log(lesson && lesson);

  return (
    <Grid
      container
      spacing={3}
      justify="space-between"
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid item>
        <Typography variant="h3" color="textPrimary">
          {lesson && lesson.findOneExamination && lesson.findOneExamination.title}
        </Typography>
        <Box
          mx={-2}
          display="flex"
          color="text.secondary"
          alignItems="center"
          flexWrap="wrap"
        >
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              {true === true ? <CheckIcon /> : <AlertIcon />}
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {'Active'}
            </Typography>
          </div>
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              <CalendarIcon />
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {`Uploaded ${moment(data && data.createdAt).fromNow()}`}
            </Typography>
          </div>
        </Box>
      </Grid>
    </Grid>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

Header.defaultProps = {};

export default Header;
