import React, { useState, useEffect } from 'react';
import { Drawer, InputNumber, Button, Alert } from 'rsuite';
import gql from 'graphql-tag';
import { useQuery, useMutation, useLazyQuery } from 'react-apollo';
import './index.scss';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';

const DATA_ANSWER = gql`
  query aggregateQuestionUser($filterQuestionUser: FilterQuestionUser) {
    aggregateQuestionUser(filterQuestionUser: $filterQuestionUser) {
      score
      correct
      examination {
        type
        title
      }
      user {
        username
        _id
      }
      question {
        _id
        title
        type
      }
      answer {
        _id
        title
      }
    }
  }
`;

const UPDATE_SCORE = gql`
  mutation updateOneQuestionUser(
    $idQuestionUser: InputIndexQuestionUser!
    $inputQuestionUser: InputQuestionUser!
  ) {
    updateOneQuestionUser(
      idQuestionUser: $idQuestionUser
      inputQuestionUser: $inputQuestionUser
    )
  }
`;

const UPDATE_SCORE_EXAM = gql`
  mutation updateScoreExaminationUser(
    $idExaminationUser: InputIndexExaminationUser!
  ) {
    updateScoreExaminationUser(idExaminationUser: $idExaminationUser)
  }
`;

const QUESTION_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const QuizDetail = props => {
  const {
    open,
    setOpen,
    userID,
    setUserID,
    idExamination,
    setGrade,
    item
  } = props;
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useSelector(state => state.account);

  const { data: dataAnswer, refetch: refetchDataAnswer } = useQuery(
    DATA_ANSWER,
    {
      variables: {
        filterQuestionUser: {
          idUser: userID,
          idExamination: idExamination
        }
      }
    }
  );

  // const {
  //   data: dataQuestionAnswer,
  //   refetch: refetchDataQuestionAnswer
  // } = useQuery(QUESTION_ANSWER, {
  //   variables: {
  //     filterQuestionAnswer: {
  //       idQuestion: ''
  //     }
  //   }
  // });

  const [questionID, setQuestionID] = useState('');

  // const [
  //   loadQuestionAnswer,
  //   { called, loading, data: dataQuestionAnswer }
  // ] = useLazyQuery(QUESTION_ANSWER, {
  //   variables: {
  //     filterQuestionAnswer: {
  //       idQuestion: questionID
  //     }
  //   }
  // });

  const [updateOneQuestionUser] = useMutation(UPDATE_SCORE);
  const [updateScoreExaminationUser] = useMutation(UPDATE_SCORE_EXAM);

  const [score, setScore] = useState([]);

  // useEffect(() => {
  //   let arr = [];
  //   dataAnswer &&
  //     dataAnswer.aggregateQuestionUser.map((item, idx) => {
  //       arr[idx] = item.score;
  //     });
  //   console.log(arr);
  //   setScore(arr);
  // }, [dataAnswer, item]);

  const handleSubmit = () => {
    let flag = dataAnswer && dataAnswer.aggregateQuestionUser.length;
    score.map((item, idx) => {
      console.log(typeof item);
      if (typeof item === 'number') {
        flag = flag - 1;
      }
    });
    if (flag > 0) {
      Alert.warning('Please fill all required field!');
    } else {
      dataAnswer &&
        dataAnswer.aggregateQuestionUser.map((item, idx) => {
          updateOneQuestionUser({
            variables: {
              idQuestionUser: {
                idExamination: idExamination,
                idQuestion: item.question._id,
                idUser: userID
              },
              inputQuestionUser: {
                idExamination: idExamination,
                idQuestion: item.question._id,
                idUser: userID,
                idAnswer: item.answer._id,
                score: score[idx]
              }
            }
          })
            .then(res => {
              refetchDataAnswer();
              setGrade(true);
            })
            .catch(err => console.log(err));
        });
      enqueueSnackbar('Successfully!', {
        variant: 'success'
      });
      updateScoreExaminationUser({
        variables: {
          idExaminationUser: {
            idUser: userID,
            idExamination: idExamination
          }
        }
      })
        .then(res => console.log(res))
        .catch(err => console.log(err));
      setOpen(false);
    }
  };

  // useEffect(() => {
  //   console.log(dataQuestionAnswer);
  // }, [questionID]);

  useEffect(() => {
    let arr = [];
    console.log(dataAnswer && dataAnswer.aggregateQuestionUser);
    dataAnswer &&
      dataAnswer.aggregateQuestionUser.map((item, idx) => {
        console.log(idx);
        if (item.question.type === 'MULTIPLE_CHOICE' && item.correct === true) {
          arr[idx] = 100;
        } else if (
          item.question.type === 'MULTIPLE_CHOICE' &&
          item.correct === false
        ) {
          arr[idx] = 0;
        }
      });
    console.log(arr);
    setScore(arr);
  }, [dataAnswer, userID]);

  return (
    <Drawer show={open} onHide={() => setOpen(false)} backdrop={false}>
      <Drawer.Header>
        <Drawer.Title>
          {dataAnswer &&
            dataAnswer.aggregateQuestionUser[0] &&
            dataAnswer.aggregateQuestionUser[0]['examination'] &&
            dataAnswer.aggregateQuestionUser[0]['examination']['title']}
        </Drawer.Title>
      </Drawer.Header>
      <Drawer.Body>
        {dataAnswer &&
          dataAnswer.aggregateQuestionUser.map((item, idx) => {
            console.log(item);
            // setQuestionID(item.question._id);
            // loadQuestionAnswer();
            return (
              <div style={{ marginBottom: '20px' }}>
                <div
                  style={{
                    fontSize: '20px',
                    fontWeight: 'bold',
                    marginBottom: '10px'
                  }}
                >
                  {/* {item && item.question && item.question.title} */}
                  {item.question.title}
                </div>
                <div
                  style={{
                    fontSize: '18px',
                    fontWeight: '300',
                    marginBottom: '10px'
                  }}
                >
                  {item.answer.title}
                  {/* {console.log(item)} */}
                </div>
                <InputNumber
                  max={100}
                  placeholder="Score"
                  className="required"
                  disabled={
                    item.question.type === 'MULTIPLE_CHOICE' ? true : false
                  }
                  defaultValue={item.score}
                  value={score[idx]}
                  onChange={e => {
                    let arr = [...score];
                    arr[idx] = parseFloat(e);
                    setScore(arr);
                  }}
                />
              </div>
            );
          })}
        <div>
          <Button
            style={{
              backgroundColor: '#3949ab',
              marginRight: '10px',
              color: '#fff'
            }}
            onClick={() => handleSubmit()}
          >
            Submit
          </Button>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
        </div>
      </Drawer.Body>
    </Drawer>
  );
};

export default QuizDetail;
