import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  Container,
  colors,
  Button,
  Grid
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Page from 'src/components/Page';
import React, { useState, useEffect } from 'react';
// import Brief from './Brief';
// import Members from './Members';
import Header from './Header';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
// import QuizList from './quizList';
// import QuizItem from './quizItem';
import { useSelector } from 'react-redux';
import QuizDetail from './quizDetail';
import CardItem from './CardItem';
import { Progress } from 'rsuite';
const { Line } = Progress;
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  },
  group: {
    '& + &': {
      marginTop: theme.spacing(4)
    }
  },
  activity: {
    position: 'relative',
    '& + &': {
      marginTop: theme.spacing(3),
      '&:before': {
        position: 'absolute',
        content: '" "',
        height: 20,
        width: 1,
        top: -20,
        left: 20,
        backgroundColor: theme.palette.divider
      }
    }
  }
}));

const avatarsMap = {
  upload_file: {
    icon: GetAppIcon,
    className: 'avatarBlue'
  },
  join_team: {
    icon: PersonAddIcon,
    className: 'avatarOrange'
  },
  price_change: {
    icon: AttachMoneyIcon,
    className: 'avatarGreen'
  },
  contest_created: {
    icon: DashboardIcon,
    className: 'avatarIndigo'
  }
};

const GET_ALL_EXAM = gql`
  query aggregateExamination($filterExamination: FilterExamination) {
    aggregateExamination(filterExamination: $filterExamination) {
      _id
      title
    }
  }
`;

const GET_DATA_ANSWER = gql`
  query aggregateQuestionUser($filterQuestionUser: FilterQuestionUser) {
    aggregateQuestionUser(filterQuestionUser: $filterQuestionUser) {
      score
      user {
        _id
        username
      }
      question {
        _id
        title
      }
      answer {
        _id
        title
      }
    }
  }
`;

const GET_DATA_QUESTION_ANSWER = gql`
  query aggregateLessonUser($filterLessonUser: FilterLessonUser) {
    aggregateLessonUser(filterLessonUser: $filterLessonUser) {
      lesson {
        _id
        title
        idCourse
      }
      flag
      user {
        _id
        idRole
        username
        firstname
        lastname
        email
      }
    }
  }
`;

const Assignmment = ({ className, ...rest }) => {
  const classes = useStyles();
  const history = useHistory();
  const avatar = avatarsMap['contest_created'];
  console.log(window.location.pathname.slice(64));

  const { user } = useSelector(state => state.account);

  console.log(window.location.pathname.slice(68));
  const { data: lessonUser } = useQuery(GET_DATA_QUESTION_ANSWER, {
    variables: {
      filterLessonUser: {
        idLesson: window.location.pathname.slice(68)
      }
    }
  });

  console.log(lessonUser);

  const { data: allExam } = useQuery(GET_ALL_EXAM, {
    variables: {
      filterExamination: {
        idLesson: window.location.pathname.slice(64)
      }
    }
  });

  const { data: detailAnswer } = useQuery(GET_DATA_ANSWER, {
    variables: {
      filterQuestionUser: {
        idExamination: window.location.pathname.slice(101)
      }
    }
  });

  const [userData, setUserData] = useState([]);

  useEffect(() => {
    let arr = [];
    detailAnswer &&
      detailAnswer.aggregateQuestionUser.map((item, idx) => {
        arr.push(item.user);
      });
    let newArr = new Set(arr);
    let finalArr = [...newArr];
    setUserData(finalArr);
  }, [detailAnswer]);

  useEffect(() => {
    console.log(userData);
  }, [userData]);

  console.log(detailAnswer && detailAnswer);

  const [open, setOpen] = useState(false);
  const [userID, setUserID] = useState('');

  const [grade, setGrade] = useState(false);

  return (
    <Page className={classes.root} title="Project Details">
      <Container maxWidth="lg">
        <Header />
        <Grid
          className={clsx(classes.root, className)}
          container
          spacing={3}
          {...rest}
        >
          {lessonUser &&
            lessonUser.aggregateLessonUser.map((item, idx) => {
              console.log(item);
              // return <CardItem item={item} />;
              console.log(user);
              if (user.idRole === 'LECTURER' || user.idRole === 'ADMIN')
                return (
                  <div
                    style={{
                      backgroundColor: '#fff',
                      padding: '1rem',
                      width: '100%',
                      borderRadius: '10px',
                      fontSize: '15px',
                      marginBottom: '1rem'
                      // display: 'flex',
                      // justifyContent: 'space-between'
                    }}
                  >
                    {/* {user.idRole === 'LECTURER' || user.idRole === 'ADMIN' ? ( */}
                    {/* <> */}
                    <div style={{ fontWeight: 'bold' }}>
                      {item.user.lastname} {item.user.firstname}
                    </div>
                    <div>
                      <Line
                        strokeColor="#58b15b"
                        percent={Math.round(parseFloat(item.flag.slice(0, -1)))}
                        strokeWidth={10}
                        style={{ paddingLeft: 0 }}
                      />
                    </div>{' '}
                    {/* </> */}
                    {/* ) : user.idRole === 'STUDENT' &&
                      user._id === item.user._id ? (
                      <>
                        <div style={{ fontWeight: 'bold' }}>
                          {item.user.lastname} {item.user.firstname}
                        </div>
                        <div>
                          <Line
                            strokeColor="#58b15b"
                            percent={Math.round(
                              parseFloat(item.flag.slice(0, -1))
                            )}
                            strokeWidth={10}
                            style={{ paddingLeft: 0 }}
                          />
                        </div>{' '}
                      </>
                    ) : (
                      ''
                    )} */}
                  </div>
                );
              else if (
                user.idRole === 'STUDENT' &&
                user._id === item.user._id
              ) {
                return (
                  <div
                    style={{
                      backgroundColor: '#fff',
                      padding: '1rem',
                      width: '100%',
                      borderRadius: '10px',
                      fontSize: '15px',
                      marginBottom: '1rem'
                      // display: 'flex',
                      // justifyContent: 'space-between'
                    }}
                  >
                    <div style={{ fontWeight: 'bold' }}>
                      {item.user.lastname} {item.user.firstname}
                    </div>
                    <div>
                      <Line
                        strokeColor="#58b15b"
                        percent={Math.round(parseFloat(item.flag.slice(0, -1)))}
                        strokeWidth={10}
                        style={{ paddingLeft: 0 }}
                      />
                    </div>{' '}
                  </div>
                );
              }
            })}
        </Grid>
      </Container>
    </Page>
  );
};

export default Assignmment;
