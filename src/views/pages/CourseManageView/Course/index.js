import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  CardHeader,
  List,
  ListItem,
  ListItemText,
  Typography,
  makeStyles,
  LinearProgress,
  SvgIcon
} from '@material-ui/core';
import {
  Edit3 as PlusCircleIcon,
  Delete as DeleteIcon,
  Info as InfoIcon
} from 'react-feather';
import gql from 'graphql-tag';
import { useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import { useSnackbar } from 'notistack';
import CourseEditDrawer from './courseEditDrawer';
const DELETE_COURSE = gql`
  mutation deleteOneCourse($idCourse: ID!) {
    deleteOneCourse(idCourse: $idCourse)
  }
`;
const FIND_MANY_COURSE = gql`
  {
    findManyCourse {
      _id
      description
      name
      credit
      status
      title
      isActive
      final
      mid
      present
      quiz
      idDepartment
      idTeacher
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  progress: {
    margin: theme.spacing(0, 1),
    flexGrow: 1
  }
}));

const Course = props => {
  const { data } = props;
  const classes = useStyles();
  const [deleteOneCourse] = useMutation(DELETE_COURSE);
  const { enqueueSnackbar } = useSnackbar();
  const [open, setOpen] = useState(false);

  const history = useHistory();

  const deleteCourse = _id => {
    deleteOneCourse({
      variables: {
        idCourse: _id
      },
      refetchQueries: [{ query: FIND_MANY_COURSE }]
    })
      .then(res => {
        console.log(res);
        enqueueSnackbar('Deleted course successfully!', {
          variant: 'success'
        });
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Card>
        <CardHeader
          subheader={data.name}
          subheaderTypographyProps={{
            color: 'textSecondary',
            variant: 'body2'
          }}
          title={data.title}
          titleTypographyProps={{ color: 'textPrimary' }}
        />
        <Box p={2} display="flex" alignItems="center" flexWrap="wrap">
          <Typography variant="h5" color="textPrimary">
            50 %
          </Typography>
          <LinearProgress
            className={classes.progress}
            value={50}
            color="secondary"
            variant="determinate"
          />
        </Box>
        <Box p={2} display="flex" justifyContent="space-between">
          <Button size="small" to="#" onClick={() => deleteCourse(data._id)}>
            <SvgIcon fontSize="small" className={classes.actionIcon}>
              <DeleteIcon />
            </SvgIcon>
          </Button>
          <Button size="small" to="#" onClick={() => setOpen(true)}>
            <SvgIcon fontSize="small" className={classes.actionIcon}>
              <PlusCircleIcon />
            </SvgIcon>
          </Button>
          <Button
            size="small"
            to="#"
            onClick={() => history.push(`/app/courseManagement/${data._id}`)}
          >
            <SvgIcon fontSize="small" className={classes.actionIcon}>
              <InfoIcon />
            </SvgIcon>
          </Button>
        </Box>
      </Card>
      <CourseEditDrawer open={open} setOpen={setOpen} data={data} />
    </>
  );
};

export default Course;
