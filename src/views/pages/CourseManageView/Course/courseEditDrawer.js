import { Box, Button, Card, CardContent, CardHeader, Divider, Drawer, FormHelperText, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { Formik } from 'formik';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-apollo';
import QuillEditor from 'src/components/QuillEditor';
import * as Yup from 'yup';
import FilesDropzone from './FilesDropzone';
import styled from '@emotion/styled'
import Wallpaper from '../../../../assets/img/wallpaper.png'

const useStyles = makeStyles(() => ({
  drawerDesktopRoot: {
    width: window.innerWidth,
  },
  drawerDesktopPaper: {
    position: 'relative',
    padding: 100
  },
  drawerMobilePaper: {
    position: 'relative',
    width: 280
  },
  drawerMobileBackdrop: {
    position: 'absolute'
  },
  editor: {
    background: '#fff',
    '& .ql-editor': {
      color: '#000!important',
    },

    '& .ql-picker-label': {
      color: '#000',
    },

    '& .ql-toolbar': {
      borderBottom: '1px solid #000',
    },

    '& svg': {
      filter: 'invert(1)'
    }
  }
}));

const FIND_MANY_COURSE = gql`
{
  findManyCourse{
    _id
    description
    name
    credit
    status
    title
    isActive
    final
    mid
    present
    idDepartment
    idTeacher
  }
}
`

const FIND_MAYNY_DEPARTMENT = gql`
query findManyDepartment($filterDepartment: FilterDepartment){
    findManyDepartment(filterDepartment: $filterDepartment){
      _id
      name
      description
      code
      numOfYear
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`
const GET_ALL_USER = gql`
{
    findManyUser{
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`

const UPDATE_COURSE = gql`
mutation updateOneCourse($idCourse: ID!, $inputCourse: InputCourse!){
    updateOneCourse(idCourse: $idCourse, inputCourse: $inputCourse)
  }
`
const CourseEditDrawer = (props, { className, ...rest }) => {
  const { open, setOpen, data } = props
  const { enqueueSnackbar } = useSnackbar()
  const [updateOneCourse] = useMutation(UPDATE_COURSE)

  const [idImage, setIdImage] = useState('')

  const { data: findManyUser } = useQuery(GET_ALL_USER)
  const [optionTeacher, setOptionTeacher] = useState([])
  const [selectTeacher, setSelectTeacher] = useState('')
  const { data: manyDepartment } = useQuery(FIND_MAYNY_DEPARTMENT)

  useEffect(() => {
    console.log(optionTeacher && optionTeacher)
    if (optionTeacher.length > 0) {
      setSelectTeacher(optionTeacher[0].value)
    }
  }, [optionTeacher])

  useEffect(() => {
    let array = []
    console.log(findManyUser && findManyUser)
    findManyUser && findManyUser.findManyUser.map((item, key) => {
      if (item.idRole === 'LECTURER') {
        const newItem = {
          value: item._id,
          label: item.username
        }
        array = [...array, newItem]
        setOptionTeacher(array)
      }
    })
  }, [findManyUser])

  const [selectOption, setSelectOption] = useState([])

  const [sort, setSort] = useState('');

  useEffect(() => {
    if (selectOption.length > 0) {
      setSort(selectOption[0].value)
    }
  }, [selectOption])


  useEffect(() => {
    let array = []
    manyDepartment && manyDepartment.findManyDepartment.map((item, key) => {
      const newItem = {
        value: item._id,
        label: item.name
      }
      array = [...array, newItem]
    })
    setSelectOption(array)
  }, [manyDepartment])

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSortChangeTeacher = (event) => {
    event.persist();
    setSelectTeacher(event.target.value);
  };

  console.log(data)
  const classes = useStyles();
  return <>
    <DrawerCustom
      anchor='right'
      open={open}
      classes={{
        root: classes.drawerDesktopRoot,
        paper: classes.drawerDesktopPaper
      }}
    >
      <Formik
        initialValues={{
          name: data.name,
          description: data.description,
          images: [],
          title: data.title,
          credit: data.credit,
          department: data.idDepartment,
          teacher: data.idTeacher
          // status: data.status,
          // year: data.year,
          // final: data.final,
          // mid: data.mid,
          // present: data.present,
          // quiz: data.quiz
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string().max(255).required(),
          description: Yup.string().max(5000).required(),
          images: Yup.array(),
          title: Yup.string().max(255).required(),
          credit: Yup.string().max(255).required(),
          department: Yup.string().max(255).required(),
          teacher: Yup.string().max(255).required()
          // status: Yup.string().max(255).required(),
          // year: Yup.string().max(255).required(),
          // final: Yup.string().max(255).required(),
          // mid: Yup.string().max(255).required(),
          // present: Yup.string().max(255).required(),
          // quiz: Yup.string().max(255).required()
        })}
        onSubmit={async (values, {
          setErrors,
          setStatus,
          setSubmitting
        }) => {
          try {
            // Do api call
            console.log(values)
            const { name, description, title, credit, status, year, final, mid, present, quiz, teacher, department } = values
            updateOneCourse({
              variables: {
                inputCourse: {
                  name,
                  description,
                  title,
                  credit: parseFloat(credit),
                  idDepartment: department,
                  idTeacher: teacher,
                  // urlImage: 'http://localhost:16043/images/' + idImage
                },
                idCourse: data._id
              },
              refetchQueries: [{ query: FIND_MANY_COURSE }]
            }).then(res => {
              if (res && res.data.updateOneCourse !== null) {
                enqueueSnackbar('Course Update successfully!', {
                  variant: 'success'
                })
                setOpen(false)
              }
            })
              .catch(err => console.log(err))
          } catch (err) {
            setErrors({ submit: err.message });
            setStatus({ success: false });
            setSubmitting(false);
          }
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue,
          touched,
          values
        }) => (
          <form
            onSubmit={handleSubmit}
            className={clsx(classes.root, className)}
            {...rest}
          >
            <Grid
              container
              spacing={3}
            >
              <Grid
                item
                xs={12}
                lg={12}
              >
                <Card>
                  <CardContentCustom>
                    <TextFieldCustom
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Course name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.name}
                      variant="outlined"
                    />
                    <Box
                      mt={3}
                      mb={1}
                    >
                      <TypographyCustom
                        variant="subtitle2"
                        color="textSecondary"
                      >
                        Description
                      </TypographyCustom>
                    </Box>
                    <Paper variant="outlined">
                      <QuillEditor
                        className={classes.editor}
                        value={values.description}
                        onChange={(value) => setFieldValue('description', value)}
                      />
                    </Paper>
                    {(touched.description && errors.description) && (
                      <Box mt={2}>
                        <FormHelperText error>
                          {errors.description}
                        </FormHelperText>
                      </Box>
                    )}
                  </CardContentCustom>
                </Card>
                {/* <Box mt={3}>
                                        <Card>
                                            <CardHeader title="Upload Images" />
                                            <Divider />
                                            <CardContent>
                                                <FilesDropzone setIdImage={setIdImage} idImage={idImage} />
                                            </CardContent>
                                        </Card>
                                    </Box> */}
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.title && errors.title)}
                    fullWidth
                    helperText={touched.title && errors.title}
                    label="Title"
                    name="title"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.title}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.credit && errors.credit)}
                    fullWidth
                    helperText={touched.credit && errors.credit}
                    label="Credit"
                    name="credit"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.credit}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.department && errors.department)}
                    helperText={touched.department && errors.department}
                    fullWidth
                    label="Department"
                    name="department"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    select
                    SelectProps={{ native: true }}
                    variant="outlined"
                    value={values.department}
                  >
                    {selectOption.map((option) => (
                      <option
                        key={option.value}
                        value={option.value}
                      >
                        {option.label}
                      </option>
                    ))}
                  </TextField>
                </Box>

                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.teacher && errors.teacher)}
                    helperText={touched.teacher && errors.teacher}
                    fullWidth
                    label="Teacher"
                    name="teacher"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    select
                    SelectProps={{ native: true }}
                    variant="outlined"
                    value={values.teacher}
                  >
                    {optionTeacher.map((option) => (
                      <option
                        key={option.value}
                        value={option.value}
                      >
                        {option.label}
                      </option>
                    ))}
                  </TextField>
                </Box>
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.status && errors.status)}
                                            fullWidth
                                            helperText={touched.status && errors.status}
                                            label="Status"
                                            name="status"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.status}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.year && errors.year)}
                                            fullWidth
                                            helperText={touched.year && errors.year}
                                            label="Year"
                                            name="year"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.year}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.final && errors.final)}
                                            fullWidth
                                            helperText={touched.final && errors.final}
                                            label="Final Percent"
                                            name="final"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.final}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.mid && errors.mid)}
                                            fullWidth
                                            helperText={touched.mid && errors.mid}
                                            label="Mid Percent"
                                            name="mid"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.mid}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.present && errors.present)}
                                            fullWidth
                                            helperText={touched.present && errors.present}
                                            label="Present Percent"
                                            name="present"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.present}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.quiz && errors.quiz)}
                                            fullWidth
                                            helperText={touched.quiz && errors.quiz}
                                            label="Quiz Percent"
                                            name="quiz"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.quiz}
                                            variant="outlined"
                                        />
                                    </Box> */}
              </Grid>
            </Grid>
            {errors.submit && (
              <Box mt={3}>
                <FormHelperText error>
                  {errors.submit}
                </FormHelperText>
              </Box>
            )}
            <Box mt={2}>
              <Button
                color="secondary"
                variant="contained"
                type="submit"
                disabled={isSubmitting}
                style={{ marginRight: 20 }}
              >
                Update Course
              </Button>
              <Button
                color="default"
                variant="contained"
                // type="submit"
                onClick={() => setOpen(false)}
              >
                Cancel
              </Button>
            </Box>
          </form>
        )}
      </Formik>
    </DrawerCustom>
  </>
}

export default CourseEditDrawer

const DrawerCustom = styled(Drawer)`
  .MuiDrawer-paper {
    background-image: url(${Wallpaper});
  }
`;

const TextFieldCustom = styled(TextField)`
  .MuiOutlinedInput-input {
    color: #000;
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }
`;

const TypographyCustom = styled(Typography)`
  color: #000;
`;

const CardContentCustom = styled(CardContent)`
  background: #fff;
`;