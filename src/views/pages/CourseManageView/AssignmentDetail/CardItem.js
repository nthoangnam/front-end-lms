import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  Container,
  colors,
  Button,
  Grid
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Page from 'src/components/Page';
import React, { useState, useEffect } from 'react';
// import Brief from './Brief';
// import Members from './Members';
import Header from './Header';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
// import QuizList from './quizList';
// import QuizItem from './quizItem';
import { useSelector } from 'react-redux';
import QuizDetail from './quizDetail';
// import { useSelector } from 'react-redux';

const DATA_ANSWER = gql`
  query aggregateQuestionUser($filterQuestionUser: FilterQuestionUser) {
    aggregateQuestionUser(filterQuestionUser: $filterQuestionUser) {
      score
      examination {
        type
        title
      }
      user {
        username
        _id
      }
      question {
        _id
        title
        type
      }
      answer {
        _id
        title
      }
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  },
  group: {
    '& + &': {
      marginTop: theme.spacing(4)
    }
  },
  activity: {
    position: 'relative',
    '& + &': {
      marginTop: theme.spacing(3),
      '&:before': {
        position: 'absolute',
        content: '" "',
        height: 20,
        width: 1,
        top: -20,
        left: 20,
        backgroundColor: theme.palette.divider
      }
    }
  }
}));

const CardItem = props => {
  const classes = useStyles();
  const { item } = props;
  const [userID, setUserID] = useState('');
  const [grade, setGrade] = useState(true);
  const [open, setOpen] = useState(false);

  const { data: dataAnswer } = useQuery(DATA_ANSWER, {
    variables: {
      filterQuestionUser: {
        idUser: item._id,
        idExamination: window.location.pathname.slice(101)
      }
    }
  });
  console.log(dataAnswer && dataAnswer);

  const [aveScore, setAveScore] = useState(0);

  useEffect(() => {
    let arr = [];
    dataAnswer &&
      dataAnswer.aggregateQuestionUser.map((ele, idx) => {
        console.log(ele);
        console.log(ele.user._id, item._id);
        if (ele.user._id === item._id) {
          if (ele.score === null) {
            setGrade(false);
          } else {
            arr.push(ele.score);
          }
        }
      });
    let tmpScore = 0;
    arr.map((ele, idx) => {
      tmpScore = tmpScore + ele;
    });
    setAveScore(Math.floor(tmpScore / arr.length));
  }, [dataAnswer, open]);
  const { user } = useSelector(state => state.account);
  return (
    <>
      <Card
        className={classes.card}
        style={{ width: '100%', marginBottom: '1rem' }}
        onClick={() => {
          setUserID(item._id);

          if (user && user.idRole !== 'STUDENT') {
            setOpen(true);
          }
        }}
      >
        {/* <div>{item.username}</div> */}
        <Typography
          variant="body1"
          color="textPrimary"
          onClick={() => {
            if (user && user.idRole !== 'STUDENT') {
              setOpen(true);
            }
          }}
        >
          {item.username}
        </Typography>
        {/* {grade== null ? (
                    <span style={{ color: 'green', fontWeight: 'bold' }}>
                      Graded
                    </span>
                  ) : (
                    <span style={{ color: 'red', fontWeight: 'bold' }}>
                      Not grade
                    </span>
                  )} */}
        {grade === false ? (
          <span style={{ color: 'red', fontWeight: 'bold' }}>Not grade</span>
        ) : (
          <span style={{ color: 'green', fontWeight: 'bold' }}>Graded</span>
        )}
        {<span>{aveScore}/100</span>}
      </Card>
      <QuizDetail
        setOpen={setOpen}
        open={open}
        userID={userID}
        setUserID={setUserID}
        idExamination={window.location.pathname.slice(101)}
        setGrade={setGrade}
        item={item}
      />
    </>
  );
};

export default CardItem;
