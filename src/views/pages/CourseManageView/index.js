import React, { useEffect, useState } from 'react';
import Page from 'src/components/Page';
import {
  Box,
  Container,
  makeStyles,
  Grid,
  Button,
  SvgIcon
} from '@material-ui/core';
import Header from './Header';
import clsx from 'clsx';
import Course from './Course/index';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import { PlusCircle as PlusCircleIcon } from 'react-feather';
import { Link as RouterLink } from 'react-router-dom';
import ProjectCard from './ProjectCard';
import { useSelector } from 'react-redux';
import { Modal, SelectPicker, Paragraph } from 'rsuite';
import ModalImport from './modalImport';
import styled from '@emotion/styled'
import Wallpaper from '../../../assets/img/wallpaper.png'

const useStyles = makeStyles(theme => ({
  root: {
    // backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    // paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  container: {
    paddingTop: theme.spacing(3),
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 64,
      paddingRight: 64
    }
  }
}));

const FIND_MANY_COURSE = gql`
  {
    findManyCourse {
      _id
      description
      name
      credit
      status
      title
      isActive
      final
      mid
      present
      quiz
      urlImage
      idDepartment
      idTeacher
    }
  }
`;

const CourseManageView = ({ className, ...rest }) => {
  const { user } = useSelector(state => state.account);
  console.log({ user });
  const { data: allCourse, refetch: refetchCourse } = useQuery(
    FIND_MANY_COURSE,
    {
      fetchPolicy: 'network-only'
    }
  );

  console.log(allCourse && allCourse);
  const classes = useStyles();

  const [open, setOpen] = useState(false);

  useEffect(() => {
    refetchCourse();
  }, []);

  return (
    <PageCustom className={classes.root} title="Course Management">
      <img src={Wallpaper} alt="background" />
      <Container maxWidth={false} className={classes.container}>
        <Grid
          className={clsx(classes.root, className)}
          container
          justify="space-between"
          spacing={2}
          {...rest}
        >
          <Header />
          <Grid item>
            {(user && user.idRole === 'STUDENT') ||
              (user && user.idRole === 'LECTURER') ? (
              ''
            ) : (
              <>
                <ButtonCustom
                  component={RouterLink}
                  color="secondary"
                  variant="contained"
                  className={classes.action}
                  to="/app/courseManagement/create"
                >
                  <SvgIcon fontSize="small" className={classes.actionIcon}>
                    <PlusCircleIcon />
                  </SvgIcon>
                  New Course
                </ButtonCustom>
                <ButtonCustom
                  component={RouterLink}
                  color="primary"
                  style={{ marginLeft: '1rem' }}
                  // variant="contained"
                  // className={classes.action}
                  onClick={() => setOpen(true)}
                // to="/app/courseManagement/create"
                >
                  <SvgIcon fontSize="small" className={classes.actionIcon}>
                    <PlusCircleIcon />
                  </SvgIcon>
                  Import
                </ButtonCustom>
              </>
            )}
          </Grid>
        </Grid>
        {/* <Grid
          container
          spacing={3}>
          {
            allCourse && allCourse.findManyCourse.map((item, key) => {
              return <Grid
                item
                lg={3}
                sm={6}
                xs={12}
              >
                <Course data={item} key={item._id} />
              </Grid>
            })
          }
        </Grid> */}
        <Grid container spacing={3}>
          {/* {projects.map((project) => (
          <Grid
            item
            key={project.id}
            lg={4}
            lx={4}
            md={6}
            xs={12}
          >
            <ProjectCard project={project} />
          </Grid>
        ))} */}
          {allCourse &&
            allCourse.findManyCourse.map((item, key) => {
              return (
                <GridCustom item key={item.id} md={4} sm={6} xs={12}>
                  <ProjectCard project={item} key={item._id} />
                </GridCustom>
              );
            })}
        </Grid>
      </Container>
      <ModalImport
        setOpen={setOpen}
        open={open}
        refetchCourse={refetchCourse}
      />
    </PageCustom>
  );
};

export default CourseManageView;

const PageCustom = styled(Page)`
  position: relative;
  
  img {
    position: absolute;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`

const GridCustom = styled(Grid)`
  z-index: 2;
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }

  &.MuiButton-textPrimary {
    color: #38aae8;
  }
`