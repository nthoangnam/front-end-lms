import {
  Avatar,
  Box,
  Card,
  CardMedia,
  colors,
  Divider,
  Grid,
  Link,
  makeStyles,
  Typography,
  Button
} from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import getInitials from 'src/utils/getInitials';
import parse from 'html-react-parser';
import CourseEditDrawer from './Course/courseEditDrawer';
import { useHistory } from 'react-router';
import { useSnackbar } from 'notistack';
import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import { useSelector } from 'react-redux';
import { Modal } from 'rsuite';
import styled from '@emotion/styled'

const DELETE_COURSE = gql`
  mutation deleteOneCourse($idCourse: ID!) {
    deleteOneCourse(idCourse: $idCourse)
  }
`;

const FIND_MANY_COURSE = gql`
  {
    findManyCourse {
      _id
      description
      name
      credit
      status
      title
      isActive
      final
      mid
      present
      quiz
      urlImage
      idTeacher
    }
  }
`;

const FIND_ONE_USER = gql`
  query findOneUser($userId: ID) {
    findOneUser(userId: $userId) {
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`;

const ModalConfirm = props => {
  const { open, setOpen, deleteCourse, id } = props;
  return (
    <Modal show={open} onHide={() => setOpen(false)}>
      <Modal.Header>
        <Modal.Title>Delete this course?</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>Are you sure?</div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => deleteCourse(id)} color="primary">
          Ok
        </Button>
        <Button onClick={() => setOpen(false)}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    background: '#fff'
  },
  media: {
    height: 200,
    backgroundColor: theme.palette.background.dark
  },
  likedButton: {
    color: colors.red[600]
  },
  subscribersIcon: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1)
  }
}));

function ProjectCard({ project, className, ...rest }) {
  const { user } = useSelector(state => state.account);
  const classes = useStyles();
  const [liked, setLiked] = useState(project.liked);
  const [likes, setLikes] = useState(project.likes);
  const [deleteOneCourse] = useMutation(DELETE_COURSE);
  const { data: findOneUser } = useQuery(FIND_ONE_USER, {
    variables: {
      userId: project && project.idTeacher
    }
  });

  const [confirm, setConfirm] = useState(false);

  let decs =
    parse(`${project.description}`).props === undefined
      ? project.description
      : parse(`${project.description}`).props.children;
  console.log(decs);

  const { enqueueSnackbar } = useSnackbar();
  const [open, setOpen] = useState(false);
  const history = useHistory();

  const [course, setCourse] = useState(project);

  console.log(course);

  const handleLike = () => {
    setLiked(true);
    setLikes(prevLikes => prevLikes + 1);
  };

  const handleUnlike = () => {
    setLiked(false);
    setLikes(prevLikes => prevLikes - 1);
  };

  const deleteCourse = _id => {
    deleteOneCourse({
      variables: {
        idCourse: _id
      },
      refetchQueries: [{ query: FIND_MANY_COURSE }]
    })
      .then(res => {
        console.log(res);
        enqueueSnackbar('Deleted course successfully!', {
          variant: 'success'
        });
      })
      .catch(err => console.log(err));
  };

  const [openModal, setOpenModal] = useState(false);

  return (
    <>
      <Card className={clsx(classes.root, className)} {...rest}>
        <Box p={3}>
          <CardMedia className={classes.media} image={project.urlImage} />
          <Box display="flex" alignItems="center" mt={2}>
            <Avatar alt="Author" src={'project.author.avatar'}>
              {getInitials('project.author.name')}
            </Avatar>
            <Box ml={2}>
              <CustomLink
                color="textPrimary"
                component={RouterLink}
                to="#"
                variant="h5"
                onClick={() =>
                  history.push(`/app/courseManagement/${project._id}`)
                }
              >
                {project.title}
              </CustomLink>
              <Typography variant="body2" color="textSecondary">
                by{' '}
                <CustomLink
                  color="textPrimary"
                  component={RouterLink}
                  to="#"
                  variant="h6"
                >
                  {findOneUser && findOneUser.findOneUser.username}
                </CustomLink>{' '}
                | Updated {/* {moment(project.updatedAt).fromNow()} */}
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box pb={2} px={3}>
          <TypographyCustom
            color="textSecondary"
            variant="body2"
            className="description"
          >
            {decs}
          </TypographyCustom>
        </Box>
        <Box py={2} px={3}>
          <Grid
            alignItems="center"
            container
            justify="space-between"
            spacing={3}
          >
            <Grid item>
              <TypographyCustom variant="h5" color="textPrimary">
                Credits
              </TypographyCustom>
              <TypographyCustom variant="body2" color="textSecondary">
                {project.credit}
              </TypographyCustom>
            </Grid>
            <Grid item>
              <TypographyCustom variant="h5" color="textPrimary">
                Status
              </TypographyCustom>
              <TypographyCustom variant="body2" color="textSecondary">
                {project.status}
              </TypographyCustom>
            </Grid>
            <Grid item>
              {/* <Typography
              variant="h5"
              color="textPrimary"
            >
              {'project.technology'}
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
            >
              Technology
            </Typography> */}
              {(user && user.idRole === 'STUDENT') ||
                (user && user.idRole === 'LECTURER') ? (
                ''
              ) : (
                <>
                  <ButtonCustom
                    color="secondary"
                    variant="contained"
                    onClick={() => setOpen(true)}
                  >
                    Edit
                  </ButtonCustom>
                  <Button
                    variant="contained"
                    onClick={() => setOpenModal(true)}
                    style={{ marginLeft: '1rem' }}
                  >
                    Delete
                  </Button>
                </>
              )}
            </Grid>
          </Grid>
        </Box>
        <Divider />
      </Card>
      <CourseEditDrawer open={open} setOpen={setOpen} data={project} />
      <ModalConfirm
        open={openModal}
        setOpen={setOpenModal}
        deleteCourse={deleteCourse}
        id={project._id}
      />
    </>
  );
}

ProjectCard.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default ProjectCard;

const CustomLink = styled(Link)`
  color: #000;

  &:hover {
    color: #1675e0;
  }
`;

const TypographyCustom = styled(Typography)`
  color: #000;
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;