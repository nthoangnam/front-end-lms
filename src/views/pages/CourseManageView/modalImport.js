import React, { useEffect, useState } from 'react';
// import { Button, Modal } from 'rsuite';
import * as XLSX from 'xlsx';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useSnackbar } from 'notistack';
import { Modal, SelectPicker, Paragraph, Button} from 'rsuite';

const CREATE_A_COURSE = gql`
  mutation createOneCourse($inputCourse: InputCourse!) {
    createOneCourse(inputCourse: $inputCourse)
  }
`;

const ModalImport = props => {
  const { open, setOpen, refetchCourse } = props;

  const [fileUploaded, setFileUploaded] = useState([]);
  const [createOneCourse] = useMutation(CREATE_A_COURSE);
  const [loading, setLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const handleUpload = e => {
    e.preventDefault();

    var files = e.target.files,
      f = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      var data = e.target.result;
      let readedData = XLSX.read(data, { type: 'binary' });
      const wsname = readedData.SheetNames[0];
      const ws = readedData.Sheets[wsname];

      /* Convert array to json*/
      const dataParse = XLSX.utils.sheet_to_json(ws, { header: 1 });
      setFileUploaded(dataParse);
    };
    reader.readAsBinaryString(f);
  };

  const handleSubmit = () => {
    fileUploaded &&
      fileUploaded.map((item, idx) => {
        createOneCourse({
          variables: {
            inputCourse: {
              name: item[0],
              description: item[4],
              title: item[0],
              credit: parseFloat(item[3]),
              idDepartment: item[5],
              idTeacher: item[1],
              urlImage: item[2]
            }
          }
        })
          .then(res => {
            if (
              res &&
              res.data.createOneCourse !== null &&
              idx === fileUploaded.length - 1
            ) {
              refetchCourse();
              enqueueSnackbar('Course created successfully!', {
                variant: 'success'
              });
              setLoading(false);
              setOpen(false);
            }
          })
          .catch(err => console.log(err));
      });
  };

  useEffect(() => {
    console.log(fileUploaded);
  }, [fileUploaded]);

  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)} size="lg">
        <Modal.Header>
          <Modal.Title>Import courses</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div style={{ marginBottom: '1rem', fontSize: '1rem' }}>
            Select file to import
          </div>
          <input type="file" onChange={e => handleUpload(e)} accept=".xlsx" />
          <table
            className="table"
            style={{
              width: '100%',
              borderCollapse: 'separate',
              borderSpacing: '10px 10px',
              textAlign: 'center'
            }}
          >
            <thead>
              <tr>
                {fileUploaded &&
                  fileUploaded &&
                  fileUploaded[0] &&
                  fileUploaded[0].map((item, idx) => {
                    return <th>{item}</th>;
                  })}
              </tr>
            </thead>
            <tbody>
              {fileUploaded &&
                fileUploaded.map((item, idx) => {
                  if (idx !== 0) {
                    return (
                      <tr>
                        {item.map((ele, index) => {
                          return <td>{ele}</td>;
                        })}
                      </tr>
                    );
                  }
                })}
            </tbody>
          </table>
          {/* <Paragraph rows={80} /> */}
          <SelectPicker
            data={[
              {
                label: 'Eugenia',
                value: 'Eugenia',
                role: 'Master'
              },
              {
                label: 'Kariane',
                value: 'Kariane',
                role: 'Master'
              },
              {
                label: 'Louisa',
                value: 'Louisa',
                role: 'Master'
              },
              {
                label: 'Marty',
                value: 'Marty',
                role: 'Master'
              },
              {
                label: 'Kenya',
                value: 'Kenya',
                role: 'Master'
              },
              {
                label: 'Hal',
                value: 'Hal',
                role: 'Developer'
              },
              {
                label: 'Julius',
                value: 'Julius',
                role: 'Developer'
              },
              {
                label: 'Travon',
                value: 'Travon',
                role: 'Developer'
              },
              {
                label: 'Vincenza',
                value: 'Vincenza',
                role: 'Developer'
              },
              {
                label: 'Dominic',
                value: 'Dominic',
                role: 'Developer'
              },
              {
                label: 'Pearlie',
                value: 'Pearlie',
                role: 'Guest'
              },
              {
                label: 'Tyrel',
                value: 'Tyrel',
                role: 'Guest'
              },
              {
                label: 'Jaylen',
                value: 'Jaylen',
                role: 'Guest'
              },
              {
                label: 'Rogelio',
                value: 'Rogelio',
                role: 'Guest'
              }
            ]}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            color="violet"
            disabled={loading}
            onClick={() => handleSubmit()}
          >
            Save
          </Button>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalImport;
