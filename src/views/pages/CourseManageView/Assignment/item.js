import React from 'react';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useEffect, useState } from 'react';
import { Bar, Line } from 'react-chartjs-2';

const GET_ALL_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
        isActive
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const Item = props => {
  const { idQuestion, data } = props;

  const [title, setTitle] = useState('');
  const [listAns, setListAns] = useState([]);
  const [listCount, setListCount] = useState([]);

  const { data: dataQuestionAnswer, refetch: refetchQuestionAnswer } = useQuery(
    GET_ALL_ANSWER,
    {
      variables: {
        filterQuestionAnswer: {
          idQuestion: idQuestion && idQuestion
        }
      }
    }
  );

  useEffect(() => {
    console.log(data);
  }, [data]);

  useEffect(() => {
    console.log(dataQuestionAnswer && dataQuestionAnswer);
    setTitle(
      dataQuestionAnswer &&
        dataQuestionAnswer.aggregateQuestionAnswer[0].question.title
    );
    let arr = [];
    let count = [];

    dataQuestionAnswer &&
      dataQuestionAnswer.aggregateQuestionAnswer.map((item, idx) => {
        if (item.answer !== null) {
          arr.push(item.answer.title);
          count.push(0);
        }
      });
    console.log(arr);
    setListAns(arr);

    data && data.map((ele, index) => {
      console.log(arr.indexOf(ele.answer.title));
      if (arr.indexOf(ele.answer.title) !== -1) {
        console.log(count[arr.indexOf(ele.answer.title)]);
        // if (count[arr.indexOf(ele.answer.title)]) {
        //   count[arr.indexOf(ele.answer.title)] =
        //     count[arr.indexOf(ele.answer.title)] + 1;
        // } else {
        //   count[arr.indexOf(ele.answer.title)] = 0;
        // }
        count[arr.indexOf(ele.answer.title)] =
          count[arr.indexOf(ele.answer.title)] + 1;
      }
    });
    setListCount(count);
    console.log(count);
  }, [dataQuestionAnswer, data]);

  const data1 = {
    labels: listAns,
    datasets: [
      {
        label: 'My First dataset',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: listCount
      }
    ]
  };

  useEffect(() => {
    console.log(data1);
  }, [data1]);

  return (
    <>
      <div>{title}</div>
      <div>
        {listAns.length > 0 ? <Bar data={data1} /> : ''}
        {/* <Line ref="chart" data={data1} /> */}
      </div>
    </>
  );
};

export default Item;
