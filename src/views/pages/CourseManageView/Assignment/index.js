import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  Container,
  colors,
  Button,
  Grid
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Page from 'src/components/Page';
import React, { useState, useEffect } from 'react';
// import Brief from './Brief';
// import Members from './Members';
import Header from './Header';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
// import QuizList from './quizList';
// import QuizItem from './quizItem';
import { useSelector } from 'react-redux';
import ModalChart from './chart';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  },
  group: {
    '& + &': {
      marginTop: theme.spacing(4)
    }
  },
  activity: {
    position: 'relative',
    '& + &': {
      marginTop: theme.spacing(3),
      '&:before': {
        position: 'absolute',
        content: '" "',
        height: 20,
        width: 1,
        top: -20,
        left: 20,
        backgroundColor: theme.palette.divider
      }
    }
  }
}));

const avatarsMap = {
  upload_file: {
    icon: GetAppIcon,
    className: 'avatarBlue'
  },
  join_team: {
    icon: PersonAddIcon,
    className: 'avatarOrange'
  },
  price_change: {
    icon: AttachMoneyIcon,
    className: 'avatarGreen'
  },
  contest_created: {
    icon: DashboardIcon,
    className: 'avatarIndigo'
  }
};

const GET_ALL_EXAM = gql`
  query aggregateExamination($filterExamination: FilterExamination) {
    aggregateExamination(filterExamination: $filterExamination) {
      title
      _id
      course {
        _id
      }
      lesson {
        _id
      }
    }
  }
`;

const Assignmment = ({ className, ...rest }) => {
  const classes = useStyles();
  const history = useHistory();
  const avatar = avatarsMap['contest_created'];
  console.log(window.location.pathname.slice(64));

  const { data: allExam } = useQuery(GET_ALL_EXAM, {
    variables: {
      filterExamination: {
        idLesson: window.location.pathname.slice(64)
      }
    }
  });

  console.log(allExam && allExam);

  const [open, setOpen] = useState(false);
  const [examId, setExamID] = useState('');
  return (
    <>
      <Page className={classes.root} title="Project Details">
        <Container maxWidth="lg">
          <Header />
          <Grid
            className={clsx(classes.root, className)}
            container
            spacing={3}
            {...rest}
          >
            {allExam &&
              allExam.aggregateExamination.map((item, idx) => {
                console.log(item);
                return (
                  <Card
                    className={classes.card}
                    style={{ width: '100%', marginBottom: '1rem' }}
                  >
                    <Typography variant="body1" color="textPrimary">
                      <Link
                        color="textPrimary"
                        component={RouterLink}
                        to={`${window.location.pathname}/${item._id}`}
                        variant="h6"
                      >
                        {item && item.title}
                      </Link>{' '}
                      <Button
                        onClick={() => {
                          setExamID(item && item._id);
                          setOpen(true);
                        }}
                      >
                        View chart
                      </Button>
                    </Typography>
                  </Card>
                );
              })}
          </Grid>
        </Container>
      </Page>
      <ModalChart open={open} setOpen={setOpen} idExamination={examId} />
    </>
  );
};

export default Assignmment;
