import React from 'react';
import { useEffect, useState } from 'react';
import { Modal, InputNumber } from 'rsuite';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import Item from './item';

const GET_DATA_ANSWER = gql`
  query aggregateQuestionUser($filterQuestionUser: FilterQuestionUser) {
    aggregateQuestionUser(filterQuestionUser: $filterQuestionUser) {
      score
      correct
      examination {
        type
        title
      }
      user {
        username
        _id
      }
      question {
        _id
        title
        type
      }
      answer {
        _id
        title
      }
    }
  }
`;

const GET_QUESTION = gql`
  query aggregateOneExaminationQuestion(
    $idExaminationQuestion: InputIndexExaminationQuestion!
  ) {
    aggregateOneExaminationQuestion(
      idExaminationQuestion: $idExaminationQuestion
    ) {
      question {
        _id
        title
      }
    }
  }
`;

const GET_ALL_QUESTION = gql`
  query aggregateExaminationQuestion(
    $filterExaminationQuestion: FilterExaminationQuestion
  ) {
    aggregateExaminationQuestion(
      filterExaminationQuestion: $filterExaminationQuestion
    ) {
      question {
        _id
        title
        type
        isActive
      }
      examination {
        _id
        idLesson
        idContentLesson
        idCourse
        idDepartment
        type
      }
    }
  }
`;

const GET_ALL_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
        isActive
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const ModalChart = props => {
  const { open, setOpen, idExamination } = props;

  useEffect(() => {
    console.log(allQuestion && allQuestion.aggregateExaminationQuestion);
  }, [allQuestion]);
  useEffect(() => {
    console.log(detailAnswer && detailAnswer);
  }, [detailAnswer]);

  const { data: detailAnswer } = useQuery(GET_DATA_ANSWER, {
    variables: {
      filterQuestionUser: {
        idExamination: idExamination && idExamination
      }
    }
  });

  const { data: allQuestion, refetch: refetchQuestion } = useQuery(
    GET_ALL_QUESTION,
    {
      variables: {
        filterExaminationQuestion: {
          idExamination: idExamination && idExamination
        }
      }
    }
  );

  const { data: dataQuestionAnswer, refetch: refetchQuestionAnswer } = useQuery(
    GET_ALL_ANSWER
  );

  const [score, setScore] = useState([]);
  useEffect(() => {
    console.log(detailAnswer && detailAnswer.aggregateQuestionUser);
    console.log(allQuestion && allQuestion.aggregateExaminationQuestion);
    let arr = [];
    let arrNum = [];
    allQuestion &&
      allQuestion.aggregateExaminationQuestion.map((item, idx) => {
        arr.push(item.question._id);
      });
    setListQuestion(arr);
    console.log(arr);
  }, [detailAnswer, allQuestion]);

  const [listQuestion, setListQuestion] = useState([]);

  return (
    <>
      <Modal
        size="lg"
        show={open}
        onHide={() => {
          setOpen(false);
        }}
      >
        <Modal.Header>
          <Modal.Title>Chart report</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {listQuestion.map((item, idx) => {
            return <Item idQuestion={item} data={detailAnswer && detailAnswer.aggregateQuestionUser}/>;
          })}
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ModalChart;
