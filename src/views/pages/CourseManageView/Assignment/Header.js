import {
  Box,
  Grid,
  makeStyles,
  SvgIcon,
  Typography,
  Button
} from '@material-ui/core';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import {
  AlertTriangle as AlertIcon,
  Calendar as CalendarIcon,
  Check as CheckIcon
} from 'react-feather';
// import Application from './Application';
import { useSelector } from 'react-redux';
import gql from 'graphql-tag';
// import { Button } from 'rsuite';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory, useParams } from 'react-router';

const useStyles = makeStyles(theme => ({
  root: {},
  badge: {
    display: 'flex',
    alignItems: 'center',
    margin: theme.spacing(2)
  },
  badgeIcon: {
    marginRight: theme.spacing(1)
  },
  action: {
    marginBottom: theme.spacing(1),
    '& + &': {
      marginLeft: theme.spacing(1)
    }
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

const UPDATE_LESSON_USER_SCORE = gql`
  mutation updateScoreLessonUser($idLessonUser: InputIndexLessonUser!) {
    updateScoreLessonUser(idLessonUser: $idLessonUser)
  }
`;

function Header({
  setHaveAVideo,
  haveAVideo,
  setUpload,
  title,
  setTitle,
  createOneMedia,
  data,
  project,
  className,
  ...rest
}) {
  const { user } = useSelector(state => state.account);
  const classes = useStyles();
  const [openApplication, setOpenApplication] = useState(false);
  const { idLesson } = useParams();
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const GET_DETAIL_LESSON = gql`
    query findOneLesson($idLesson: ID!) {
      findOneLesson(idLesson: $idLesson) {
        _id
        title
        idCourse
        isActive
        createdAt
        updatedAt
        createdBy
        updatedBy
      }
    }
  `;

  //   console.log(window.location.pathname);

  const { data: lesson } = useQuery(GET_DETAIL_LESSON, {
    variables: {
      idLesson: window.location.pathname.slice(64)
    }
  });

  const [updateScoreLessonUser] = useMutation(UPDATE_LESSON_USER_SCORE);

  console.log(idLesson && idLesson);
  const handleFinishCourse = () => {
    updateScoreLessonUser({
      variables: {
        idLessonUser: {
          idLesson: idLesson,
          idUser: user._id
        }
      }
    })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  };
  return (
    <Grid
      container
      spacing={3}
      justify="space-between"
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid item>
        <Typography variant="h3" color="textPrimary">
          {lesson && lesson.findOneLesson.title}
        </Typography>
        <Box
          mx={-2}
          display="flex"
          color="text.secondary"
          alignItems="center"
          flexWrap="wrap"
        >
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              {true === true ? <CheckIcon /> : <AlertIcon />}
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {'Active'}
            </Typography>
          </div>
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              <CalendarIcon />
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {`Uploaded ${moment(data && data.createdAt).fromNow()}`}
            </Typography>
          </div>
        </Box>
        <Button color="primary" onClick={() => handleFinishCourse()}>
          Finish
        </Button>
      </Grid>
    </Grid>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

Header.defaultProps = {};

export default Header;
