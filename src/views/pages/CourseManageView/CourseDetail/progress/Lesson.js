import React, { useState } from 'react';
import { Link as RouterLink, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  colors,
  Button
} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
// import ModalEdit from './modalEdit';
// import DeleteModal from './deleteConfirm';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import { useMutation, useQuery } from 'react-apollo';

const DELETE_LESSON = gql`
  mutation deleteOneLesson($idLesson: ID!) {
    deleteOneLesson(idLesson: $idLesson)
  }
`;



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  }
}));

const avatarsMap = {
  upload_file: {
    icon: GetAppIcon,
    className: 'avatarBlue'
  },
  join_team: {
    icon: PersonAddIcon,
    className: 'avatarOrange'
  },
  price_change: {
    icon: AttachMoneyIcon,
    className: 'avatarGreen'
  },
  contest_created: {
    icon: DashboardIcon,
    className: 'avatarIndigo'
  }
};

function Activity({
  user,
  GET_ALL_LESSON,
  refetchAllDataLesson,
  activity,
  className,
  ...rest
}) {
  const classes = useStyles();
  const avatar = avatarsMap['contest_created'];
  const { enqueueSnackbar } = useSnackbar();
  const [deleteOneLesson] = useMutation(DELETE_LESSON);
  const { data: allLesson, refetch: refetchLesson } = useQuery(GET_ALL_LESSON);
  const [openEdit, setOpenEdit] = useState(false);
  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const [openApplication, setOpenApplication] = useState(false);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleDelete = () => {
    deleteOneLesson({
      variables: {
        idLesson: activity && activity._id
      },
      refetchQueries: [{ query: GET_ALL_LESSON }]
    })
      .then(res => {
        if (res && res.data.deleteOneLesson !== null) {
          handleApplicationClose();
          refetchAllDataLesson();
          console.log(allLesson && allLesson);
          enqueueSnackbar('Lesson delete successfully!', {
            variant: 'success'
          });
        }
      })
      .catch(err => console.log(err));
  };


  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Avatar className={clsx(classes.avatar, classes[avatar.className])}>
        <avatar.icon />
      </Avatar>
      <Card className={classes.card}>
        <Typography variant="body1" color="textPrimary">
          <Link
            color="textPrimary"
            component={RouterLink}
            to={`/app/courseManagement/${activity.idCourse}/progress/${activity._id}`}
            variant="h6"
          >
            {activity.title}
          </Link>{' '}
          {activity.description}
        </Typography>

        <Typography className={classes.date} variant="caption">
          {moment(activity.createdAt).fromNow()}
        </Typography>
        {/* <ModalEdit
          activity={activity}
          onApply={handleEditClose}
          onClose={handleEditClose}
          open={openEdit}
          GET_ALL_LESSON={GET_ALL_LESSON}
        /> */}
        {/* <DeleteModal
          onApply={handleApplicationClose}
          onClose={handleApplicationClose}
          open={openApplication}
          onDelete={handleDelete}
          GET_ALL_LESSON={GET_ALL_LESSON}
          refetchAllDataLesson={refetchAllDataLesson}
        /> */}
      </Card>
    </div>
  );
}

Activity.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Activity;
