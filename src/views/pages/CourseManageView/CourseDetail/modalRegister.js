import React from 'react';
import { Modal } from 'rsuite';
import gql from 'graphql-tag';
import { useMutation } from 'react-apollo';
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

const CREATE_ONE_COURSE_USER = gql`
  mutation createOneCourseUser($inputCourseUser: InputCourseUser!) {
    createOneCourseUser(inputCourseUser: $inputCourseUser) {
      idUser
      idCourse
    }
  }
`;

const ModalRegister = props => {
  const { open, setOpen } = props;
  const { id } = useParams();

  const [createOneCourseUser] = useMutation(CREATE_ONE_COURSE_USER);

  const { user } = useSelector(state => state.account);
  const { enqueueSnackbar } = useSnackbar();
  console.log(id, user);

  const handleSubmit = () => {
    createOneCourseUser({
      variables: {
        inputCourseUser: {
          idUser: user._id,
          idCourse: id
        }
      }
    })
      .then(res => {
        console.log(res);
        if (res.data.createOneCourseUser !== null) {
          enqueueSnackbar('Registration successfully!', {
            variant: 'success'
          });
          setOpen(false);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Modal
        show={open}
        onHide={() => setOpen(false)}
        backdrop={true}
        size="sm"
        overflow={true}
      >
        <Modal.Header>
          <Modal.Title>Confirm register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div style={{ fontSize: '20px' }}>
            Do you want to register this course?
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            style={{
              padding: '10px',
              fontSize: '20px',
              borderRadius: '10px',
              marginLeft: '10px',
              paddingLeft: '30px',
              paddingRight: '30px'
            }}
            onClick={() => setOpen(false)}
          >
            No
          </button>
          <button
            style={{
              padding: '10px',
              fontSize: '20px',
              borderRadius: '10px',
              marginLeft: '10px',
              paddingLeft: '30px',
              paddingRight: '30px',
              backgroundColor: '#5850EC',
              color: '#fff'
            }}
            onClick={() => handleSubmit()}
          >
            Yes
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalRegister;
