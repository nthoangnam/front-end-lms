import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Box, Typography, TextField, makeStyles, Button } from '@material-ui/core';

const AddLessonDialog = (props) => {

    const { open, onCancel, handleSubmit } = props
    const [lesson, setLesson] = useState('')
    const useStyles = makeStyles((theme) => ({
        root: {},
        confirmButton: {
            marginLeft: theme.spacing(2)
        }
    }));

    const classes = useStyles();

    return (
        <>
            <Dialog
                maxWidth="sm"
                fullWidth
                onClose={onCancel}
                open={open}
            >
                <Box p={3}>
                    <Typography
                        align="center"
                        gutterBottom
                        variant="h3"
                        color="textPrimary"
                    >
                        Add Lesson
                    </Typography>
                </Box>
                <Box p={3}
                    ref={(el) => {
                        if (el) {
                            el.style.setProperty('padding-top', '0', 'important');
                        }
                    }}
                >
                    <TextField
                        label="Title"
                        name="title"
                        onChange={(e) => setLesson(e.target.value)}
                        value={lesson}
                        variant="outlined"
                        fullWidth


                    />
                </Box>
                <Box
                    p={2}
                    display="flex"
                    alignItems="center"
                >
                    <Box flexGrow={1} />
                    <Button onClick={onCancel}>
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        type="submit"
                        color="secondary"
                        className={classes.confirmButton}
                        onClick={() => handleSubmit(lesson)}
                    >
                        Confirm
            </Button>
                </Box>
            </Dialog>
        </>
    )
}

AddLessonDialog.propTypes = {
    onCancel: PropTypes.func,
    open: PropTypes.bool
};

AddLessonDialog.defaultProps = {
    onCancel: () => { },
    open: false
};


export default AddLessonDialog