import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  colors,
  Button
} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import ModalEdit from './modalEdit';
import DeleteModal from './deleteConfirm';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import { useMutation, useQuery } from 'react-apollo';
import { useSelector } from 'react-redux';

const DELETE_LESSON = gql`
  mutation deleteOneLesson($idLesson: ID!) {
    deleteOneLesson(idLesson: $idLesson)
  }
`;

const DELETE_ONE_COURSE_USER = gql`
  mutation deleteOneCourseUser($idCourseUser: InputIndexCourseUser!) {
    deleteOneCourseUser(idCourseUser: $idCourseUser)
  }
`;

const UPDATE_A_COURSE_USER = gql`
  mutation updateOneCourseUser(
    $idCourseUser: InputIndexCourseUser!
    $inputCourseUser: InputCourseUser!
  ) {
    updateOneCourseUser(
      idCourseUser: $idCourseUser
      inputCourseUser: $inputCourseUser
    )
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  }
}));

const avatarsMap = {
  upload_file: {
    icon: GetAppIcon,
    className: 'avatarBlue'
  },
  join_team: {
    icon: PersonAddIcon,
    className: 'avatarOrange'
  },
  price_change: {
    icon: AttachMoneyIcon,
    className: 'avatarGreen'
  },
  contest_created: {
    icon: DashboardIcon,
    className: 'avatarIndigo'
  }
};

function Activity({
  user,
  GET_ALL_LESSON,
  refetchAllDataLesson,
  activity,
  className,
  refetchOneUserCourse,
  ...rest
}) {
  const classes = useStyles();
  const avatar = avatarsMap['contest_created'];
  const { enqueueSnackbar } = useSnackbar();
  const [deleteOneLesson] = useMutation(DELETE_LESSON);
  const { data: allLesson, refetch: refetchLesson } = useQuery(GET_ALL_LESSON);
  const [openEdit, setOpenEdit] = useState(false);
  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const [deleteOneCourseUser] = useMutation(DELETE_ONE_COURSE_USER);

  const { user: user1 } = useSelector(state => state.account);

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  console.log('asdasd', activity);

  const [openApplication, setOpenApplication] = useState(false);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleDelete = () => {
    console.log(activity.user._id);
    console.log(activity.course._id);
    deleteOneCourseUser({
      variables: {
        idCourseUser: {
          idUser: activity.user._id,
          idCourse: activity.course._id
        }
      }
      // refetchQueries: [{ query: GET_ALL_LESSON }]
    })
      .then(res => {
        if (res && res.data.deleteOneCourseUser !== null) {
          // handleApplicationClose();
          refetchOneUserCourse();
          // console.log(allLesson && allLesson);
          enqueueSnackbar('Lesson delete successfully!', {
            variant: 'success'
          });
        }
      })
      .catch(err => console.log(err));
  };

  const [updateOneCourseUser] = useMutation(UPDATE_A_COURSE_USER);

  const handleAccept = () => {
    console.log(activity.user._id);
    console.log(activity.course._id);
    updateOneCourseUser({
      variables: {
        idCourseUser: {
          idUser: activity.user._id,
          idCourse: activity.course._id
        },
        inputCourseUser: {
          isVerified: true
        }
      }
    })
      .then(res => {
        if (res.data.updateOneCourseUser === true) {
          enqueueSnackbar('Successfully!', {
            variant: 'success'
          });
          refetchOneUserCourse();
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Avatar className={clsx(classes.avatar, classes[avatar.className])}>
        <avatar.icon />
      </Avatar>
      <Card className={classes.card}>
        <Typography variant="body1" color="textPrimary">
          <Link
            color="textPrimary"
            component={RouterLink}
            // to={`/app/courseManagement/${activity.idCourse}/quiz/${activity._id}`}
            variant="h6"
          >
            {activity.user.firstname} {activity.user.lastname}
          </Link>{' '}
          {activity.description}
        </Typography>

        {user && user.idRole === 'STUDENT' ? (
          ''
        ) : (
          <>
            <div>
              <button
                style={{
                  marginRight: '10px',
                  padding: '10px',
                  borderRadius: '5px',
                  backgroundColor: "#38aae8",
                  color: "#000"
                }}
                onClick={() => handleAccept()}
              >
                Accept
              </button>
              <button
                style={{ padding: '10px', borderRadius: '5px', color: "#000" }}
                onClick={() => handleDelete()}
              >
                Ingrone
              </button>
            </div>
          </>
        )}
      </Card>
    </div >
  );
}

Activity.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Activity;
