import { Box, Button, Dialog, makeStyles, Typography, Input, TextField } from '@material-ui/core';
import clsx from 'clsx';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useMutation } from 'react-apollo';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

const CREATE_ONE_ITEM = gql`
mutation createOneDepartment($inputDepartment: InputDepartment!){
  createOneDepartment(inputDepartment:$inputDepartment)
}
`

function DeleteModal({
  refetch,
  setUpload,
  data,
  createOneMedia,
  author,
  open,
  onClose,
  onApply,
  className,
  onDelete,
  GET_ALL_LESSON,
  ...rest
}) {
  const classes = useStyles();


  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        className={clsx(classes.root, className)}
        {...rest}
      >
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
        >
          Are you sure to delete this item
        </Typography>
        <Box
          mt={3}
          p={3}
        >
          <Button
            variant="contained"
            fullWidth
            color="primary"
            onClick={() => onDelete()}
          >
            SUBMIT
          </Button>
        </Box>
      </div>
    </Dialog>
  );
}

DeleteModal.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

DeleteModal.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default DeleteModal;
