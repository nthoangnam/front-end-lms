import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useParams } from 'react-router';
import {
  Avatar,
  Box,
  Button,
  Container,
  Hidden,
  IconButton,
  Tooltip,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import AddPhotoIcon from '@material-ui/icons/AddPhotoAlternate';
import MoreIcon from '@material-ui/icons/MoreVert';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import ModalRegister from './modalRegister';
import { useSelector } from 'react-redux';
import ModalFinish from './modalFinish';
import styled from '@emotion/styled'

const FIND_ONE_COURSE = gql`
  query aggregateOneCourse($idCourse: ID!) {
    aggregateOneCourse(idCourse: $idCourse) {
      teacher {
        username
        firstname
        lastname
      }
    }
  }
`;

const FIND_ONE_USER = gql`
  query findOneUser($userId: ID) {
    findOneUser(userId: $userId) {
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`;

const ONE_COURSE_USER = gql`
  query aggregateCourseUser($inputCourseUser: InputFilterCourseUser) {
    aggregateCourseUser(inputCourseUser: $inputCourseUser) {
      user {
        _id
        idRole
        username
        firstname
        lastname
      }
      isVerified
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    background: "#ebe8e4",
  },
  cover: {
    position: 'relative',
    height: 460,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    '&:before': {
      position: 'absolute',
      content: '" "',
      top: 0,
      left: 0,
      height: '100%',
      width: '100%',
      backgroundImage:
        'linear-gradient(-180deg, rgba(0,0,0,0.00) 58%, rgba(0,0,0,0.32) 100%)'
    },
    '&:hover': {
      '& $changeButton': {
        visibility: 'visible'
      }
    }
  },
  changeButton: {
    visibility: 'hidden',
    position: 'absolute',
    bottom: theme.spacing(3),
    right: theme.spacing(3),
    backgroundColor: colors.blueGrey[900],
    color: theme.palette.common.white,
    [theme.breakpoints.down('md')]: {
      top: theme.spacing(3),
      bottom: 'auto'
    },
    '&:hover': {
      backgroundColor: colors.blueGrey[900]
    }
  },
  addPhotoIcon: {
    marginRight: theme.spacing(1)
  },
  avatar: {
    border: `2px solid ${theme.palette.common.white}`,
    height: 120,
    width: 120,
    top: -60,
    left: theme.spacing(3),
    position: 'absolute'
  },
  action: {
    marginLeft: theme.spacing(1)
  }
}));

function Header({ data, user, className, idCourse, ...rest }) {
  const { user: user1 } = useSelector(state => state.account);
  const classes = useStyles();
  const [connectedStatus, setConnectedStatus] = useState(user.connectedStatus);
  const { data: oneCourse } = useQuery(FIND_ONE_USER, {
    variables: {
      userId: data && data.idTeacher
    }
  });

  const { id } = useParams();

  console.log(user && user);

  const { data: oneUserCourse } = useQuery(ONE_COURSE_USER, {
    variables: {
      inputCourseUser: {
        idCourse: id,
        idUser: user1 && user1._id
      }
    }
  });

  console.log(oneUserCourse && oneUserCourse);

  const handleConnectToggle = () => {
    setConnectedStatus(prevConnectedStatus =>
      prevConnectedStatus === 'not_connected' ? 'pending' : 'not_connected'
    );
  };

  const [open, setOpen] = useState(false);
  const [openFinish, setOpenFinish] = useState(false);

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <div
        className={classes.cover}
        style={{ backgroundImage: `url(${user.cover})` }}
      >
        <Button className={classes.changeButton} variant="contained">
          <AddPhotoIcon className={classes.addPhotoIcon} />
          Change Cover
        </Button>
      </div>
      <ContainerCustom maxWidth="lg">
        <Box position="relative" mt={1} display="flex" alignItems="center">
          <Avatar alt="Person" className={classes.avatar} src={user.avatar} />
          <Box marginLeft="160px">
            <Typography variant="overline" color="textSecondary">
              {oneCourse &&
                oneCourse.findOneUser &&
                oneCourse.findOneUser.username}
            </Typography>
            <TypographyCustom variant="h4" color="textPrimary">
              {data && data.name}
            </TypographyCustom>
          </Box>
          <Box flexGrow={1} />
          <Hidden smDown>
            {/* {connectedStatus === 'not_connected' && (
              <Button
                onClick={handleConnectToggle}
                size="small"
                variant="outlined"
                className={classes.action}
              >
                Connect
              </Button>
            )}
            {connectedStatus === 'pending' && (
              <Button
                onClick={handleConnectToggle}
                size="small"
                variant="outlined"
                className={classes.action}
              >
                Pending
              </Button>
            )} */}
            {user1.idRole !== 'ADMIN' && (
              <>
                <ButtonFinish
                  onClick={() => setOpenFinish(true)}
                  size="small"
                  variant="outlined"
                  className={classes.action}
                >
                  Finish
                </ButtonFinish>
                <ButtonCustom
                  color="secondary"
                  component={RouterLink}
                  size="small"
                  // to="/app/chat"
                  variant="contained"
                  onClick={() => setOpen(true)}
                  className={classes.action}
                  disabled={
                    oneUserCourse &&
                    oneUserCourse.aggregateCourseUser &&
                    oneUserCourse.aggregateCourseUser[0] &&
                    oneUserCourse.aggregateCourseUser[0].isVerified
                  }
                >
                  Register
                </ButtonCustom>
              </>
            )}

          </Hidden>
          <Tooltip title="More options">
            <IconButton className={classes.action}>
              <MoreIcon />
            </IconButton>
          </Tooltip>
        </Box>
      </ContainerCustom>
      <ModalRegister open={open} setOpen={setOpen} />
      <ModalFinish open={openFinish} setOpen={setOpenFinish} />
    </div>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object.isRequired
};

export default Header;

const ButtonCustom = styled(Button)`
    &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;

const ButtonFinish = styled(Button)`
  &.MuiButton-root {
    color: #000;
    border-color: #000;
  }
`;

const ContainerCustom = styled(Container)`
  /* background: #fff; */
`;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;