import React, { useCallback, useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
  makeStyles
} from '@material-ui/core';
import axios from 'src/utils/axios';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Page from 'src/components/Page';
import Header from './Header';
import Info from './info';
import Quiz from './quiz';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import Projects from './register';
import { useSelector } from 'react-redux';
import Progress from './progress';
import styled from '@emotion/styled';

const FIND_A_COURSE = gql`
  query findOneCourse($idCourse: ID!) {
    findOneCourse(idCourse: $idCourse) {
      _id
      description
      name
      credit
      status
      title
      urlImage
      idDepartment
      isActive
      final
      mid
      present
      quiz
      idTeacher
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const tabs = [
  { value: 'info', label: 'info' },
  { value: 'connections', label: 'Assignment' },
  { value: 'projects', label: 'Registration' },
  { value: 'reviews', label: 'Progress' }
];

const tabsStudent = [{ value: 'info', label: 'info' }];

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#ebe8e4',
    minHeight: '100%'
  }
}));

function ProfileView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const [currentTab, setCurrentTab] = useState('info');
  const [user, setUser] = useState(null);
  const history = useHistory();
  const { data: course } = useQuery(FIND_A_COURSE, {
    variables: {
      idCourse: history.location.pathname.slice(22)
    }
  });

  const { user: user1 } = useSelector(state => state.account);

  console.log(user1 && user1);

  useEffect(() => {
    setUser({
      avatar: '/static/images/avatars/logo.png',
      bio: 'Sales Manager',
      connectedStatus: 'not_connected',
      cover: '/static/images/covers/cover.png',
      currentCity: 'Bucharest',
      currentJob: { title: 'Product Designer', company: 'Devias IO' },
      email: 'katarina.smith@devias.io',
      id: '5e86809283e28b96d2d38537',
      name: 'Katarina Smith',
      originCity: 'Rm. Valcea',
      previousJob: {
        title: 'UX Designer at',
        company: 'Focus Aesthetic Dyanmics'
      },
      profileProgress: 50,
      quote:
        'Everyone thinks of changing the world, but no one thinks of changing himself.'
    });
  }, []);

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  const getPosts = useCallback(() => {
    axios.get('/api/social/profile').then(response => {
      if (isMountedRef.current) {
        setUser(response.data.user);
      }
    });
  }, [isMountedRef]);

  useEffect(() => {
    getPosts();
  }, [getPosts]);

  if (!user) {
    return null;
  }

  return (
    <Page className={classes.root} title="Course Details">
      <Header
        user={user}
        data={course && course.findOneCourse}
        idCourse={history.location.pathname.slice(22)}
      />
      <Container maxWidth="lg">
        <Box mt={3}>
          <Tabs
            onChange={handleTabsChange}
            scrollButtons="auto"
            value={currentTab}
            textColor="secondary"
            variant="scrollable"
          >
            {/* {user1.idRole !== 'STUDENT' ? (
              <>
                {
                  tabs.map(tab => (
                    <TabCustom key={tab.value} label={tab.label} value={tab.value} />
                  ))
                }
              </>
            ) : (
              <>
                {
                  tabsStudent.map(tab => (
                    <TabCustom key={tab.value} label={tab.label} value={tab.value} />
                  ))
                }
              </>
            )} */}

            {tabs.map(tab => (
              <TabCustom key={tab.value} label={tab.label} value={tab.value} />
            ))}

          </Tabs>
        </Box>
        <Divider />
        <Box py={3} pb={6}>
          {currentTab === 'info' && <Info user={user} />}
          {currentTab === 'connections' && <Quiz />}
          {currentTab === 'projects' && <Projects />}
          {currentTab === 'reviews' && <Progress />}
        </Box>
      </Container>
    </Page>
  );
}

export default ProfileView;

const TabCustom = styled(Tab)`
  &.MuiTab-textColorSecondary {
    color: #000;
  }

  &.MuiTab-textColorSecondary.Mui-selected {
    color: #38aae8;
  }
`;
