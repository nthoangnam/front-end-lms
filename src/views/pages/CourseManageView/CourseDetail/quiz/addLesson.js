import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Box, Typography, TextField, makeStyles, Button } from '@material-ui/core';
import styled from '@emotion/styled'

const AddLessonDialog = (props) => {

  const { open, onCancel, handleSubmit } = props
  const [lesson, setLesson] = useState('')
  const useStyles = makeStyles((theme) => ({
    root: {
      background: '#fff'
    },
    confirmButton: {
      marginLeft: theme.spacing(2)
    }
  }));

  const classes = useStyles();

  return (
    <>
      <Dialog
        maxWidth="sm"
        fullWidth
        onClose={onCancel}
        open={open}
      >
        <Box p={3}>
          <TypographyCustom
            align="center"
            gutterBottom
            variant="h3"
            color="textPrimary"
          >
            Add Lesson
          </TypographyCustom>
        </Box>
        <Box p={3}
          ref={(el) => {
            if (el) {
              el.style.setProperty('padding-top', '0', 'important');
            }
          }}
        >
          <TextField
            label="Title"
            name="title"
            onChange={(e) => setLesson(e.target.value)}
            value={lesson}
            variant="outlined"
            fullWidth


          />
        </Box>
        <Box
          p={2}
          display="flex"
          alignItems="center"
        >
          <Box flexGrow={1} />
          <Button onClick={onCancel}>
            Cancel
          </Button>
          <ButtonCustom
            variant="contained"
            type="submit"
            color="secondary"
            className={classes.confirmButton}
            onClick={() => handleSubmit(lesson)}
          >
            Confirm
          </ButtonCustom>
        </Box>
      </Dialog>
    </>
  )
}

AddLessonDialog.propTypes = {
  onCancel: PropTypes.func,
  open: PropTypes.bool
};

AddLessonDialog.defaultProps = {
  onCancel: () => { },
  open: false
};


export default AddLessonDialog

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;