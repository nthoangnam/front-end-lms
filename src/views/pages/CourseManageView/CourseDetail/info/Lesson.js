import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Avatar,
  Card,
  Typography,
  Link,
  makeStyles,
  colors,
  Button
} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonAddIcon from '@material-ui/icons/PersonAddOutlined';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import ModalEdit from './modalEdit';
import DeleteModal from './deleteConfirm';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import { useMutation, useQuery } from 'react-apollo';
import { useParams } from 'react-router';
import styled from '@emotion/styled'

const DELETE_LESSON = gql`
  mutation deleteOneLesson($idLesson: ID!) {
    deleteOneLesson(idLesson: $idLesson)
  }
`;

const GET_ONE_USER_COURSE = gql`
  query aggregateCourseUser($inputCourseUser: InputFilterCourseUser) {
    aggregateCourseUser(inputCourseUser: $inputCourseUser) {
      course {
        _id
      }
      user {
        _id
        idRole
        username
        firstname
        lastname
      }
      isVerified
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center'
  },
  card: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(2),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  date: {
    marginLeft: 'auto',
    flexShrink: 0
  },
  avatar: {
    color: colors.common.white
  },
  avatarBlue: {
    backgroundColor: colors.blue[500]
  },
  avatarGreen: {
    backgroundColor: colors.green[500]
  },
  avatarOrange: {
    backgroundColor: colors.orange[500]
  },
  avatarIndigo: {
    backgroundColor: colors.indigo[500]
  }
}));

const avatarsMap = {
  upload_file: {
    icon: GetAppIcon,
    className: 'avatarBlue'
  },
  join_team: {
    icon: PersonAddIcon,
    className: 'avatarOrange'
  },
  price_change: {
    icon: AttachMoneyIcon,
    className: 'avatarGreen'
  },
  contest_created: {
    icon: DashboardIcon,
    className: 'avatarIndigo'
  }
};

function Activity({
  user,
  GET_ALL_LESSON,
  refetchAllDataLesson,
  activity,
  className,
  ...rest
}) {
  const classes = useStyles();
  const avatar = avatarsMap['contest_created'];
  const { enqueueSnackbar } = useSnackbar();
  const [deleteOneLesson] = useMutation(DELETE_LESSON);
  const { data: allLesson, refetch: refetchLesson } = useQuery(GET_ALL_LESSON);
  const [openEdit, setOpenEdit] = useState(false);
  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const [openApplication, setOpenApplication] = useState(false);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleDelete = () => {
    deleteOneLesson({
      variables: {
        idLesson: activity && activity._id
      },
      refetchQueries: [{ query: GET_ALL_LESSON }]
    })
      .then(res => {
        if (res && res.data.deleteOneLesson !== null) {
          handleApplicationClose();
          refetchAllDataLesson();
          console.log(allLesson && allLesson);
          enqueueSnackbar('Lesson delete successfully!', {
            variant: 'success'
          });
        }
      })
      .catch(err => console.log(err));
  };

  const { id } = useParams();

  const { data: oneUserCourse } = useQuery(GET_ONE_USER_COURSE, {
    variables: {
      inputCourseUser: {
        idUser: user && user._id,
        idCourse: id
      }
    }
  });

  console.log(oneUserCourse && oneUserCourse);
  console.log(user);

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Avatar className={clsx(classes.avatar, classes[avatar.className])}>
        <avatar.icon />
      </Avatar>
      <Card className={classes.card}>
        <Typography variant="body1" color="textPrimary">
          {user.idRole === 'ADMIN' || user.idRole === 'LECTURER' ? (
            <Link
              color="textPrimary"
              component={RouterLink}
              to={`/app/courseManagement/${activity.idCourse}/${activity._id}`}
              variant="h6"
            >
              {activity.title}
            </Link>
          ) : oneUserCourse &&
            oneUserCourse.aggregateCourseUser[0] &&
            oneUserCourse.aggregateCourseUser[0].isVerified === true &&
            oneUserCourse &&
            oneUserCourse.aggregateCourseUser[0] &&
            oneUserCourse.aggregateCourseUser[0].user.idRole === 'STUDENT' ? (
            <Link
              color="textPrimary"
              component={RouterLink}
              to={`/app/courseManagement/${activity.idCourse}/${activity._id}`}
              variant="h6"
            >
              {activity.title}
            </Link>
          ) : (
            activity.title
          )}

          {activity.description}
        </Typography>

        <Typography className={classes.date} variant="caption">
          {user && user.idRole === 'STUDENT' ? (
            ''
          ) : (
            <div>
              <ButtonCustom
                style={{ marginRight: '10px' }}
                color="secondary"
                variant="contained"
                onClick={() => handleEditOpen()}
              >
                Edit
              </ButtonCustom>
              <Button
                style={{ marginRight: '10px' }}
                onClick={() => handleApplicationOpen()}
              >
                Delete
              </Button>
            </div>
          )}

          {moment(activity.createdAt).fromNow()}
        </Typography>
        <ModalEdit
          activity={activity}
          onApply={handleEditClose}
          onClose={handleEditClose}
          open={openEdit}
          GET_ALL_LESSON={GET_ALL_LESSON}
        />
        <DeleteModal
          onApply={handleApplicationClose}
          onClose={handleApplicationClose}
          open={openApplication}
          onDelete={handleDelete}
          GET_ALL_LESSON={GET_ALL_LESSON}
          refetchAllDataLesson={refetchAllDataLesson}
        />
      </Card>
    </div>
  );
}

Activity.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Activity;

const ButtonCustom = styled(Button)`
 &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;