import { Box, Button, Dialog, makeStyles, Typography, Input, TextField } from '@material-ui/core';
import clsx from 'clsx';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useMutation } from 'react-apollo';
import styled from '@emotion/styled'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

const CREATE_ONE_ITEM = gql`
mutation createOneDepartment($inputDepartment: InputDepartment!){
  createOneDepartment(inputDepartment:$inputDepartment)
}
`

function DeleteModal({
  refetch,
  setUpload,
  data,
  createOneMedia,
  author,
  open,
  onClose,
  onApply,
  className,
  onDelete,
  GET_ALL_LESSON,
  ...rest
}) {
  const classes = useStyles();


  return (
    <DialogCustom
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        className={clsx(classes.root, className)}
        {...rest}
      >
        <TypographyCustom
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
        >
          Are you sure to delete this item?
        </TypographyCustom>
        <Box
          mt={3}
          p={3}
        >
          <ButtonCustom
            variant="contained"
            fullWidth
            color="primary"
            onClick={() => onDelete()}
          >
            SUBMIT
          </ButtonCustom>
        </Box>
      </div>
    </DialogCustom>
  );
}

DeleteModal.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

DeleteModal.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default DeleteModal;

const DialogCustom = styled(Dialog)`
  .MuiPaper-root {
    background-color: #fff;
  }
`;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;
