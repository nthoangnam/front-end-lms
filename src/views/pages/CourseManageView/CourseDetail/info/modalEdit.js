import {
  Box,
  Button,
  Dialog,
  makeStyles,
  Typography,
  Input,
  TextField
} from '@material-ui/core';
import clsx from 'clsx';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useMutation } from 'react-apollo';
import styled from '@emotion/styled'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

const UPDATE_ONE_LESSON = gql`
  mutation updateOneLesson($idLesson: ID!, $inputLesson: InputLesson!) {
    updateOneLesson(idLesson: $idLesson, inputLesson: $inputLesson)
  }
`;

function EditModal({
  refetch,
  setUpload,
  data,
  createOneMedia,
  author,
  open,
  onClose,
  onApply,
  className,
  activity,
  GET_ALL_LESSON,
  ...rest
}) {
  const [value, setValue] = useState('');
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [name, setName] = useState(activity && activity.title);

  const [updateOneLesson] = useMutation(UPDATE_ONE_LESSON);

  const handleSubmit = () => {
    if (name === '') {
      enqueueSnackbar('Title and Description can not be null!', {
        variant: 'error'
      });
    } else {
      updateOneLesson({
        variables: {
          idLesson: activity && activity._id,
          inputLesson: {
            title: name
          }
        },
        refetchQueries: [{ query: GET_ALL_LESSON }]
      })
        .then(res => {
          if (res && res.data.createOneDepartment !== null) {
            enqueueSnackbar('Lesson updated successfully!', {
              variant: 'success'
            });
            onClose();
          }
        })
        .catch(err => console.log(err));
    }
  };

  useEffect(() => { }, [name]);

  return (
    <DialogCustom maxWidth="lg" onClose={onClose} open={open}>
      <div className={clsx(classes.root, className)} {...rest}>
        <TypographyCustom
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
        >
          Update lesson
        </TypographyCustom>
        <Box
          mt={3}
        // p={3}
        >
          <TextField
            style={{ width: '25rem ' }}
            fullWidth
            label="Lesson name"
            multiline
            variant="outlined"
            onChange={e => setName(e.target.value)}
            value={name}
          />
        </Box>
        <Box mt={3} p={3}>
          <ButtonCustom
            variant="contained"
            fullWidth
            color="primary"
            onClick={() => handleSubmit()}
          >
            SUBMIT
          </ButtonCustom>
        </Box>
      </div>
    </DialogCustom>
  );
}

EditModal.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

EditModal.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default EditModal;

const DialogCustom = styled(Dialog)`
  .MuiPaper-root {
    background-color: #fff;
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }

  .MuiInputBase-input {
    color: #000;
  }
`;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;
