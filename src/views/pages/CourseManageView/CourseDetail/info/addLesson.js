import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dialog, Box, Typography, TextField, makeStyles, Button } from '@material-ui/core';
import styled from '@emotion/styled'

const AddLessonDialog = (props) => {

  const { open, onCancel, handleSubmit } = props
  const [lesson, setLesson] = useState('')
  const useStyles = makeStyles((theme) => ({
    root: {
      background: '#fff'
    },
    confirmButton: {
      marginLeft: theme.spacing(2)
    }
  }));

  const classes = useStyles();

  return (
    <>
      <DialogCustom
        maxWidth="sm"
        fullWidth
        onClose={onCancel}
        open={open}
      >
        <Box p={3}>
          <TypographyCustom
            align="center"
            gutterBottom
            variant="h3"
            color="textPrimary"
          >
            Add Lesson
          </TypographyCustom>
        </Box>
        <Box p={3}
          ref={(el) => {
            if (el) {
              el.style.setProperty('padding-top', '0', 'important');
            }
          }}
        >
          <TextField
            label="Title"
            name="title"
            onChange={(e) => setLesson(e.target.value)}
            value={lesson}
            variant="outlined"
            fullWidth


          />
        </Box>
        <Box
          p={2}
          display="flex"
          alignItems="center"
        >
          <Box flexGrow={1} />
          <ButtonCancel onClick={onCancel}>
            Cancel
          </ButtonCancel>
          <ButtonCustom
            variant="contained"
            type="submit"
            color="secondary"
            className={classes.confirmButton}
            onClick={() => handleSubmit(lesson)}
          >
            Confirm
          </ButtonCustom>
        </Box>
      </DialogCustom>
    </>
  )
}

AddLessonDialog.propTypes = {
  onCancel: PropTypes.func,
  open: PropTypes.bool
};

AddLessonDialog.defaultProps = {
  onCancel: () => { },
  open: false
};


export default AddLessonDialog

const DialogCustom = styled(Dialog)`
  .MuiPaper-root {
    background-color: #fff;
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }

  .MuiInputBase-input {
    color: #000;
  }
`;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;

const ButtonCancel = styled(Button)`
  .MuiButton-label {
    color: #000;
  }
`;