import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import clsx from 'clsx';
import { Typography, makeStyles, Button } from '@material-ui/core';
import Activity from './Lesson';
import AddDialog from './addLesson';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const useStyles = makeStyles(theme => ({
  root: {},
  title: {
    marginBottom: theme.spacing(3),
    color: '#000'
  },
  group: {
    '& + &': {
      marginTop: theme.spacing(4)
    }
  },
  activity: {
    position: 'relative',
    '& + &': {
      marginTop: theme.spacing(3),
      '&:before': {
        position: 'absolute',
        content: '" "',
        height: 20,
        width: 1,
        top: -20,
        left: 20,
        backgroundColor: theme.palette.divider
      }
    }
  }
}));

const GET_ALL_LESSON = gql`
  query findManyLesson($filterLesson: FilterLesson) {
    findManyLesson(filterLesson: $filterLesson) {
      _id
      title
      idCourse
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const CREATE_ONE_LESSON = gql`
  mutation createOneLesson($inputLesson: InputLesson!) {
    createOneLesson(inputLesson: $inputLesson)
  }
`;

function About({ className, ...rest }) {
  const { user } = useSelector(state => state.account);
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [modal, setModal] = useState(false);

  const [createOneLesson] = useMutation(CREATE_ONE_LESSON);

  const history = useHistory();

  console.log(history.location.pathname.slice(22));

  const { data: lessonData, refetch: refetchAllDataLesson } = useQuery(
    GET_ALL_LESSON,
    {
      variables: {
        filterLesson: {
          idCourse: history.location.pathname.slice(22)
        }
      }
    }
  );

  const [lessonDt, setLessonDt] = useState(lessonData);

  // eslint-disable-next-line no-restricted-syntax

  const handleSubmit = lesson => {
    console.log(lesson, history.location.pathname.slice(22));
    createOneLesson({
      variables: {
        inputLesson: {
          title: lesson,
          idCourse: history.location.pathname.slice(22)
        }
      },
      refetchQueries: [
        {
          query: GET_ALL_LESSON,
          variables: {
            filterLesson: {
              idCourse: history.location.pathname.slice(22)
            }
          }
        }
      ]
    })
      .then(res => {
        if (res && res.data.createOneLesson !== null) {
          enqueueSnackbar('Create lesson successfully!', {
            variant: 'success'
          });
          setModal(false);
        }
      })
      .catch(err => {
        enqueueSnackbar('Create lesson error!', {
          variant: 'error'
        });
      });
  };

  useEffect(() => {
    console.log('reload');
  }, [lessonDt]);

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Typography className={classes.title} variant="h3" color="textPrimary">
        All lesson
      </Typography>
      <div className={classes.group}>
        {lessonData &&
          lessonData.findManyLesson.map(activity => (
            <Activity
              activity={activity}
              className={classes.activity}
              key={activity.id}
              GET_ALL_LESSON={GET_ALL_LESSON}
              refetchAllDataLesson={refetchAllDataLesson}
              user={user && user}
            />
          ))}
      </div>
      <div
        className={clsx(classes.root, className)}
        style={{ marginTop: '20px' }}
      >
        {user && user.idRole === 'STUDENT' ? (
          ''
        ) : (
          <ButtonCustom
            variant="contained"
            color="secondary"
            fullWidth
            onClick={() => setModal(true)}
          >
            ADD NEW LESSON
          </ButtonCustom>
        )}
      </div>
      <div className={classes.group}></div>
      <AddDialog
        open={modal}
        onCancel={() => setModal(false)}
        handleSubmit={handleSubmit}
      />
    </div>
  );
}

About.propTypes = {
  activities: PropTypes.array.isRequired,
  className: PropTypes.string
};

export default About;

const ButtonCustom = styled(Button)`
 &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;