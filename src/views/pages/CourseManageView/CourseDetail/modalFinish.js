import React from 'react';
import { Button, Modal } from 'rsuite';

const ModalFinish = props => {
  const { open, setOpen } = props;
  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>Finish this course?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Are you sure?</div>
        </Modal.Body>
        <Modal.Footer>
          <Button>Yes</Button>
          <Button onClick={() => setOpen(false)}>No</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalFinish;
