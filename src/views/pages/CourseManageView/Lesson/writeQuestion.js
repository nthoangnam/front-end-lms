import React, { useState, useEffect } from 'react';
import {
  TextField,
  Typography,
  Paper,
  Box,
  Radio,
  Button,
  Input
} from '@material-ui/core';

import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import { Modal, Drawer } from 'rsuite';
import { useSnackbar } from 'notistack';
import styled from '@emotion/styled'

const GET_ALL_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
        isActive
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const UPDATE_QUESTION = gql`
  mutation updateOneQuestion($idQuestion: ID!, $inputQuestion: InputQuestion!) {
    updateOneQuestion(idQuestion: $idQuestion, inputQuestion: $inputQuestion)
  }
`;

const UPDATE_ANSWER = gql`
  mutation updateOneAnswer($idAnswer: ID!, $inputAnswer: InputAnswer!) {
    updateOneAnswer(idAnswer: $idAnswer, inputAnswer: $inputAnswer)
  }
`;

const DELETE_ONE_ANSWER = gql`
  mutation deleteOneAnswer($idAnswer: ID!) {
    deleteOneAnswer(idAnswer: $idAnswer)
  }
`;

const DELETE_ONE_QUESTION = gql`
  mutation deleteOneQuestion($idQuestion: ID!) {
    deleteOneQuestion(idQuestion: $idQuestion)
  }
`;

const CREATE_ONE_ANSWER = gql`
  mutation createOneAnswer($inputAnswer: InputAnswer!) {
    createOneAnswer(inputAnswer: $inputAnswer)
  }
`;

const CREATE_ONE_QUESTION_ANSWER = gql`
  mutation createOneQuestionAnswer($inputQuestionAnswer: InputQuestionAnswer!) {
    createOneQuestionAnswer(inputQuestionAnswer: $inputQuestionAnswer) {
      idQuestion
      idAnswer
    }
  }
`;

const ModalUpdateQuestion = props => {
  const { enqueueSnackbar } = useSnackbar();

  const { open, setOpen, item, refetchQuestion } = props;
  const [updateOneQuestion] = useMutation(UPDATE_QUESTION);
  const [title, setTitle] = useState(
    item && item.question && item.question.title
  );

  const handleUpdate = () => {
    updateOneQuestion({
      variables: {
        idQuestion: item.question._id,
        inputQuestion: {
          title: title
        }
      }
    })
      .then(res => {
        if (res.data.updateOneQuestion === true) {
          enqueueSnackbar('Question updated successfully!', {
            variant: 'success'
          });
          refetchQuestion();
          setOpen(false);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Modal
        show={open}
        onHide={() => setOpen(false)}
      // style={{ zIndex: '9999' }}
      >
        <Modal.Header>
          <Modal.Title>Update question title</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Question title:</div>
          <div>
            <TextField
              fullWidth
              label=""
              multiline
              variant="outlined"
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            color="primary"
            variant="contained"
            onClick={() => handleUpdate()}
            style={{ marginRight: '1rem' }}
          >
            Update
          </Button>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

const ModalUpdateAnswer = props => {
  const { enqueueSnackbar } = useSnackbar();

  const {
    openAnswer,
    setOpenAnswer,
    item,
    refetchQuestion,
    refecthAns
  } = props;
  const [updateOneQuestion] = useMutation(UPDATE_ANSWER);
  const [title, setTitle] = useState(item && item.title);
  useEffect(() => {
    console.log(item);
    setTitle(item.title);
  }, [item]);
  const handleUpdate = () => {
    updateOneQuestion({
      variables: {
        idAnswer: item._id,
        inputAnswer: {
          title: title
        }
      }
    })
      .then(res => {
        if (res.data.updateOneAnswer === true) {
          enqueueSnackbar('Answer updated successfully!', {
            variant: 'success'
          });
          refecthAns();
          setOpenAnswer(false);
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Modal
        show={openAnswer}
        onHide={() => setOpenAnswer(false)}
      // style={{ zIndex: '9999' }}
      >
        <Modal.Header>
          <Modal.Title>Update answer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Answer:</div>
          <div>
            <TextField
              fullWidth
              label=""
              multiline
              variant="outlined"
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            color="primary"
            variant="contained"
            onClick={() => handleUpdate()}
            style={{ marginRight: '1rem' }}
          >
            Update
          </Button>
          <Button onClick={() => setOpenAnswer(false)}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

const ModalConfirm = props => {
  const { open, setOpen, item, refecthAns, answerLength } = props;
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    console.log(answerLength);
  }, [open]);

  const [deleteOneAnswer] = useMutation(DELETE_ONE_ANSWER);
  const deleteCourse = () => {
    let flag = 0;
    answerLength.map((item, idx) => {
      if (item.answer !== null) {
        flag = flag + 1;
      }
    });
    if (flag <= 2) {
      enqueueSnackbar('At least two answers!', {
        variant: 'error'
      });
    } else {
      deleteOneAnswer({
        variables: {
          idAnswer: item._id
        }
      }).then(res => {
        if (res.data.deleteOneAnswer === true) {
          enqueueSnackbar('Answer updated successfully!', {
            variant: 'success'
          });
          refecthAns();
          setOpen(false);
        }
      });
    }
  };
  return (
    <Modal show={open} onHide={() => setOpen(false)}>
      <Modal.Header>
        <Modal.Title>Delete this answer?</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>Are you sure?</div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => deleteCourse()} color="primary">
          Ok
        </Button>
        <Button onClick={() => setOpen(false)}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
};

const ModalConfirmQuestion = props => {
  const { open, setOpen, item, refecthAns, refetchQuestion } = props;
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    console.log(item);
  }, [item]);

  const [deleteOneQuestion] = useMutation(DELETE_ONE_QUESTION);
  const deleteCourse = () => {
    deleteOneQuestion({
      variables: {
        idQuestion: item.question._id
      }
    }).then(res => {
      if (res.data.deleteOneQuestion === true) {
        enqueueSnackbar('Question deleted successfully!', {
          variant: 'success'
        });
        refecthAns();
        refetchQuestion();
        setOpen(false);
      }
    });
  };
  return (
    <Modal show={open} onHide={() => setOpen(false)}>
      <Modal.Header>
        <Modal.Title>Delete this question?</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>Are you sure?</div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => deleteCourse()} color="primary">
          Ok
        </Button>
        <Button onClick={() => setOpen(false)}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
};

const ModalAddAnswer = props => {
  const { enqueueSnackbar } = useSnackbar();
  const { open, setOpen, item, refetchQuestion, refecthAns } = props;
  const [answer, setAnswer] = useState('');
  const [createOneAnswer] = useMutation(CREATE_ONE_ANSWER);
  const [createOneQuestionAnswer] = useMutation(CREATE_ONE_QUESTION_ANSWER);
  const handleSubmit = () => {
    createOneAnswer({
      variables: {
        inputAnswer: {
          title: answer
        }
      }
    })
      .then(res => {
        createOneQuestionAnswer({
          variables: {
            inputQuestionAnswer: {
              idQuestion: item.question._id,
              idAnswer: res.data.createOneAnswer
            }
          }
        })
          .then(res => {
            if (res.data.createOneQuestionAnswer !== null) {
              enqueueSnackbar('Question deleted successfully!', {
                variant: 'success'
              });
              refecthAns();
              setOpen(false);
            }
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  };

  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>Add more answer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <TextField
            // error={Boolean(touched.credit && errors.credit)}
            fullWidth
            // helperText={touched.credit && errors.credit}
            label="Answer"
            name="write-question"
            // onBlur={handleBlur}
            onChange={e => {
              setAnswer(e.target.value);
            }}
            // value={values.credit}
            variant="outlined"
            style={{ marginBottom: '2rem' }}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button color="primary" onClick={() => handleSubmit()}>
            Submit
          </Button>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

const WriteQuestion = props => {
  const { item, setOpenDrawer, refetchQuestion } = props;
  console.log(item && item);
  const [idQuestion, setIdQuestion] = useState(
    item && item.question && item.question._id
  );
  console.log(idQuestion);
  const { data: allAnswer, refetch: refecthAns } = useQuery(GET_ALL_ANSWER, {
    variables: {
      filterQuestionAnswer: {
        idQuestion: idQuestion
      }
    }
  });

  useEffect(() => {
    refecthAns();
  }, [item]);

  const [openConfirmQuestion, setOpenConfirmQuestion] = useState(false);

  console.log(allAnswer && allAnswer);
  const [open, setOpen] = useState(false);
  const [openAnswer, setOpenAnswer] = useState(false);
  const [dataAnswer, setDataAnswer] = useState({});
  const [openConfirm, setOpenConfirm] = useState(false);
  useEffect(() => {
    console.log(dataAnswer);
  }, [dataAnswer]);
  const [openAddAnswer, setOpenAddAnswer] = useState(false);
  return (
    <>
      <div style={{ fontSize: '1rem', marginBottom: '1rem' }}>
        {item && item.question && item.question.title}{' '}
        <Button
          onClick={() => {
            setOpen(true);
          }}
          variant="contained"
          style={{ marginLeft: '0.5rem', marginRight: '0.5rem' }}
        >
          Edit
        </Button>
        <ButtonDelete
          onClick={() => setOpenConfirmQuestion(true)}
          style={{ marginLeft: '0.5rem' }}
        >
          Delete
        </ButtonDelete>
        {item && item.question && item.question.type === 'MULTIPLE_CHOICE' ? (
          <ButtonCustom
            variant="contained"
            color="primary"
            style={{ marginLeft: '0.5rem' }}
            onClick={() => setOpenAddAnswer(true)}
          >
            Add more answer
          </ButtonCustom>
        ) : (
          ''
        )}
      </div>
      {item && item.question && item.question.type === 'ESSAY' ? (
        <TextField
          // error={Boolean(touched.credit && errors.credit)}
          fullWidth
          // helperText={touched.credit && errors.credit}
          label="Question"
          name="write-question"
          // onBlur={handleBlur}
          // onChange={handleChange}
          // value={values.credit}
          variant="outlined"
          style={{ marginBottom: '2rem' }}
        />
      ) : (
        allAnswer &&
        allAnswer.aggregateQuestionAnswer.map((item, idx) => {
          return item && item.answer !== null ? (
            <Paper
              // display="flex"
              // alignItems="center"
              mt={3}
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
              p={2}
              mb={2}
              component={Box}
            >
              <Radio checked={item && item.correct} />
              <div>
                <Typography>
                  {item && item.answer && item.answer.title}{' '}
                  <Button
                    onClick={() => {
                      setDataAnswer(item && item.answer);
                      setOpenAnswer(true);
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                    onClick={() => {
                      setDataAnswer(item && item.answer);
                      setOpenConfirm(true);
                    }}
                  >
                    Delete
                  </Button>
                </Typography>
              </div>
            </Paper>
          ) : (
            ''
          );
        })
      )}
      <ModalUpdateQuestion
        setOpen={setOpen}
        open={open}
        item={item}
        refetchQuestion={refetchQuestion}
      />
      <ModalUpdateAnswer
        setOpenAnswer={setOpenAnswer}
        openAnswer={openAnswer}
        item={dataAnswer && dataAnswer}
        refetchQuestion={refetchQuestion}
        refecthAns={refecthAns}
      />
      <ModalConfirm
        setOpen={setOpenConfirm}
        open={openConfirm}
        item={dataAnswer && dataAnswer}
        refetchQuestion={refetchQuestion}
        refecthAns={refecthAns}
        answerLength={allAnswer && allAnswer.aggregateQuestionAnswer}
      />
      <ModalConfirmQuestion
        refetchQuestion={refetchQuestion}
        refecthAns={refecthAns}
        setOpen={setOpenConfirmQuestion}
        open={openConfirmQuestion}
        item={item}
      />
      <ModalAddAnswer
        setOpen={setOpenAddAnswer}
        open={openAddAnswer}
        item={item}
        refetchQuestion={refetchQuestion}
        refecthAns={refecthAns}
      />
    </>
  );
};

export default WriteQuestion;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;

const ButtonDelete = styled(Button)`
  .MuiButton-label {
    color: #000;
  }
`;