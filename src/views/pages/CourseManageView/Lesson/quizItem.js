import React, { useState } from 'react';
import clsx from 'clsx';
import {
  Box,
  Button,
  Dialog,
  makeStyles,
  Typography,
  Input,
  Card,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import DrawerQuizDetail from './drawerQuizDetail';
import ModalEditQuiz from './modalEditQuiz';
import { Modal } from 'rsuite';
import { useMutation, useQuery } from 'react-apollo';
import { useSnackbar } from 'notistack';
import gql from 'graphql-tag';
import { useSelector } from 'react-redux';
import styled from "@emotion/styled"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

const DELETE_ONE_EXAM = gql`
  mutation deleteOneExamination($idExamination: ID!) {
    deleteOneExamination(idExamination: $idExamination)
  }
`;

const ModalDelete = props => {
  const { enqueueSnackbar } = useSnackbar();
  const { open, setOpen, item, refetchExam } = props;
  const [deleteOneExamination] = useMutation(DELETE_ONE_EXAM);
  const handleSubmit = () => {
    deleteOneExamination({
      variables: {
        idExamination: item && item._id
      }
    })
      .then(res => {
        if (res.data.deleteOneExamination) {
          enqueueSnackbar('Delete successfully!', {
            variant: 'success'
          });
          refetchExam();
          setOpen(false);
        }
      })
      .catch(err => console.log(err));
  };
  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>Delete this exam?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Are you sure</div>
        </Modal.Body>
        <Modal.Footer>
          <ButtonCustom
            onClick={() => handleSubmit()}
            color="primary"
            variant="contained"
            style={{ marginRight: '1rem' }}
          >
            Submit
          </ButtonCustom>
          <ButtonCancel onClick={() => setOpen(false)}>Cancel</ButtonCancel>
        </Modal.Footer>
      </Modal>
    </>
  );
};

const QuizItem = ({ item, refetchExam, className, ...rest }) => {
  const [open, setOpen] = useState(false);
  console.log(item && item);
  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const { user } = useSelector(state => state.account);
  return (
    <>
      <Card
        className={clsx(classes.root, className)}
        {...rest}
        style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          fontSize: '1rem',
          borderStyle: 'dashed',
          borderWidth: '2px',
          borderColor: '#707070',
          marginRight: '0.5rem',
          marginBottom: '1rem'
        }}
      >
        {/* <CardContent style={{ Width: '100%' }}  className={clsx(classes.root, className)}> */}
        <Grid container spacing={3} style={{ with: '100%' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%',
              paddingLeft: '1rem',
              paddingRight: '1rem'
            }}
          >
            <div
              style={{
                width: '20%',
                display: 'flex',
                justifyContent: 'center',
                fontSize: '1.2rem',
                fontWeight: 'bold'
              }}
            >
              {item.title}
            </div>
            <div
              style={{
                width: '20%',
                display: 'flex',
                justifyContent: 'center',
                fontSize: '1.2rem',
                fontWeight: 'bold'
              }}
            >
              {item.flag}
            </div>
            <div
              style={{
                width: '50%',
                display: 'flex',
                justifyContent: 'space-between',
                fontSize: '1.2rem',
                fontWeight: 'bold'
              }}
            >
              {user && user.idRole !== 'STUDENT' ? (
                <>
                  <ButtonCustom
                    color="primary"
                    variant="contained"
                    style={{ marginRight: '1rem' }}
                    onClick={() => setOpenEdit(true)}
                  >
                    Edit
                  </ButtonCustom>
                  <Button onClick={() => setOpenDelete(true)}>Delete</Button>
                  <Button
                    color="default"
                    variant="contained"
                    onClick={() => setOpen(true)}
                  >
                    ADD QUESTION
                  </Button>{' '}
                </>
              ) : (
                ''
              )}
            </div>
          </div>
        </Grid>
        {/* </CardContent> */}
      </Card>
      <DrawerQuizDetail open={open} setOpen={setOpen} item={item} />
      <ModalEditQuiz
        setOpenEdit={setOpenEdit}
        openEdit={openEdit}
        item={item}
        refetchExam={refetchExam}
      />
      <ModalDelete
        open={openDelete}
        setOpen={setOpenDelete}
        item={item}
        refetchExam={refetchExam}
      />
    </>
  );
};

export default QuizItem;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;

const ButtonCancel = styled(Button)`
  .MuiButton-label {
    color: #000;
  }
`;