import {
  Box,
  Button,
  Dialog,
  makeStyles,
  Typography,
  Input,
  TextField
} from '@material-ui/core';
import clsx from 'clsx';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from 'react-apollo';
import ReactPlayer from 'react-player/lazy';
import styled from "@emotion/styled"

const GET_ONE_CONTENT_LESSON = gql`
  query findManyContentLesson($filterContentLesson: FilterContentLesson) {
    findManyContentLesson(filterContentLesson: $filterContentLesson) {
      _id
      idLesson
      title
      idMedia
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const GET_ONE_MEDIA = gql`
  query findOneMedia($idMedia: ID!) {
    findOneMedia(idMedia: $idMedia) {
      _id
      title
      source
      isActive
      type
    }
  }
`;

const CREATE_ONE_EXAM = gql`
  mutation createOneExamination($inputExamination: InputExamination!) {
    createOneExamination(inputExamination: $inputExamination)
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

function ModalAddQuiz({
  refetchExam,
  project,
  idLesson,
  setUpload,
  upload,
  setHaveAVideo,
  haveAVideo,
  open,
  onClose,
  onApply,
  FIND_ONE_EXAM,
  className,
  ...rest
}) {
  const [value, setValue] = useState('');
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [flag, setFlag] = useState(0);

  const [createOneExamination] = useMutation(CREATE_ONE_EXAM);

  const { data: contentLesson, refetch: refetchContentLeson } = useQuery(
    GET_ONE_CONTENT_LESSON,
    {
      variables: {
        filterContentLesson: {
          idLesson: idLesson
        }
      }
    }
  );

  const { data: media, refetch } = useQuery(GET_ONE_MEDIA, {
    variables: {
      idMedia:
        contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson.length > 0
          ? contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson[0].idMedia
          : ''
    }
  });

  useEffect(() => {
    refetchContentLeson();
  }, [upload]);

  useEffect(() => {
    refetch();
  }, [contentLesson]);

  useEffect(() => {
    if (media && media.findOneMedia && media.findOneMedia.source) {
      setHaveAVideo(true);
    }
  }, [media]);

  const [title, setTitle] = useState('');
  const [type, setType] = useState('QUIZ');

  const handleSubmit = () => {
    console.log(title, type);
    createOneExamination({
      variables: {
        inputExamination: {
          idLesson: idLesson,
          title,
          flag,
          type
        }
      },
      refetchQueries: [{ query: FIND_ONE_EXAM }]
    })
      .then(res => {
        if (res && res.data.createOneExamination !== null) {
          enqueueSnackbar(type + ' created successfully!', {
            variant: 'success'
          });
          refetchExam();
          onClose();
        }
      })
      .catch(err => console.log(err));
  };

  const optionType = [
    {
      label: 'Mid Term',
      value: 'MID_TERM'
    },
    {
      label: 'Final',
      value: 'FINAL'
    },
    {
      label: 'Assignment',
      value: 'ASSIGNMENT'
    },
    {
      label: 'Quiz',
      value: 'QUIZ'
    }
  ];

  return (
    <DialogCustom maxWidth="lg" onClose={onClose} open={open}>
      <div className={clsx(classes.root, className)} {...rest}>
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
        >
          Create a new quiz
        </Typography>
        {media && media.findOneMedia && media.findOneMedia.source ? (
          <ReactPlayer
            // onReady={e => console.log(e)}
            // onDuration={e => setFlag(e)}
            progressInterval={1000}
            onProgress={e => {
              setFlag(e.playedSeconds);
            }}
            playsinline={true}
            url={media && media.findOneMedia && media.findOneMedia.source}
            controls={true}
          />
        ) : (
          <span
            style={{
              margin: '5rem',
              color: '#5850EC',
              fontSize: '2rem',
              fontWeight: '500'
            }}
          >
            Nothing to display!
          </span>
        )}
        <Box
          mt={3}
        // p={3}
        >
          <TextField
            fullWidth
            label="Quiz at"
            multiline
            variant="outlined"
            value={flag}
            disabled
          />
        </Box>
        <Box
          mt={3}
        // p={3}
        >
          <TextField
            fullWidth
            label="Title"
            multiline
            variant="outlined"
            onChange={e => setTitle(e.target.value)}
          />
        </Box>
        <Box
          mt={3}
        // p={3}
        >
          <TextField
            fullWidth
            label="Type"
            multiline
            variant="outlined"
            select
            SelectProps={{ native: true }}
            onChange={e => setType(e.target.value)}
            value={type}
          >
            {optionType.map(option => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </TextField>
        </Box>
        <Box mt={3} p={3} style={{ display: 'flex' }}>
          <ButtonCustom
            variant="contained"
            fullWidth
            color="primary"
            style={{ marginRight: '1rem' }}
            onClick={() => handleSubmit()}
          >
            SUBMIT
          </ButtonCustom>
          <Button variant="contained" fullWidth onClick={onClose}>
            CANCEL
          </Button>
        </Box>
      </div>
    </DialogCustom>
  );
}

ModalAddQuiz.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

ModalAddQuiz.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default ModalAddQuiz;

const DialogCustom = styled(Dialog)`
  .MuiPaper-root {
    background-color: #fff;
  }

  .MuiFormLabel-root {
    color: #000;
  }

  .MuiFormLabel-root.Mui-disabled {
    color: #000;
  }

  .MuiTypography-colorTextPrimary {
    color: #000;
  }


  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }

  .MuiOutlinedInput-root.Mui-disabled {
    .MuiOutlinedInput-notchedOutline {
      border-color: #000;
    }
  }

  .MuiInputBase-input {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;