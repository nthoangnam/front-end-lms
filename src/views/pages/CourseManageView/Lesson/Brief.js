import { Button, Card, CardContent, Grid, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import ReactPlayer from 'react-player/lazy';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import DrawerDoQuiz from './drawerDoQuiz';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const useStyles = makeStyles(theme => ({
  root: {},
  markdown: {
    fontFamily: theme.typography.fontFamily,
    '& p': {
      marginBottom: theme.spacing(2)
    }
  }
}));

const FIND_ONE_EXAM = gql`
  query aggregateExamination($filterExamination: FilterExamination) {
    aggregateExamination(filterExamination: $filterExamination) {
      _id
      lesson {
        title
      }
      title
      type
      isActive
      flag
    }
  }
`;

const GET_ONE_CONTENT_LESSON = gql`
  query findManyContentLesson($filterContentLesson: FilterContentLesson) {
    findManyContentLesson(filterContentLesson: $filterContentLesson) {
      _id
      idLesson
      title
      idMedia
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const GET_ONE_MEDIA = gql`
  query findOneMedia($idMedia: ID!) {
    findOneMedia(idMedia: $idMedia) {
      _id
      title
      source
      isActive
      type
    }
  }
`;

const UPDATE_PROGRESS = gql`
  mutation updateOneLessonUser(
    $idLessonUser: InputIndexLessonUser!
    $inputLessonUser: InputLessonUser!
  ) {
    updateOneLessonUser(
      idLessonUser: $idLessonUser
      inputLessonUser: $inputLessonUser
    )
  }
`;

const CREATE_PROGRESS = gql`
  mutation createOneLessonUser($inputLessonUser: InputLessonUser!) {
    createOneLessonUser(inputLessonUser: $inputLessonUser) {
      idLesson
      idUser
    }
  }
`;

const GET_DATA_LESSON_USER = gql`
  query aggregateLessonUser($filterLessonUser: FilterLessonUser) {
    aggregateLessonUser(filterLessonUser: $filterLessonUser) {
      lesson {
        _id
        title
        idCourse
      }
      flag
      user {
        _id
        idRole
        username
        firstname
        lastname
        email
      }
    }
  }
`;

function Brief({
  setHaveAVideo,
  haveAVideo,
  upload,
  setUpload,
  idLesson,
  project,
  className,
  setArrayFlag,
  arrayFlag,
  ...rest
}) {
  const classes = useStyles();
  const { user } = useSelector(state => state.account);
  const [updateOneLessonUser] = useMutation(UPDATE_PROGRESS);
  const [createOneLessonUser] = useMutation(CREATE_PROGRESS);

  const { data: contentLesson, refetch: refetchContentLeson } = useQuery(
    GET_ONE_CONTENT_LESSON,
    {
      variables: {
        filterContentLesson: {
          idLesson: idLesson
        }
      }
    }
  );

  const { data: lessonUser, refetch: refetchLessonUser } = useQuery(
    GET_DATA_LESSON_USER,
    {
      variables: {
        filterLessonUser: {
          idLesson: idLesson,
          idUser: user._id
        }
      }
    }
  );

  const [flag, setFlag] = useState('');

  useEffect(() => {
    console.log(lessonUser && lessonUser.aggregateLessonUser);
    let tmp =
      lessonUser &&
      lessonUser.aggregateLessonUser &&
      lessonUser.aggregateLessonUser[0] &&
      lessonUser.aggregateLessonUser[0].flag;
    // console.log(lessonUser && lessonUser.aggregateLessonUser[0].flag);
    tmp = tmp && tmp.slice(0, -1);
    console.log(tmp);
    setFlag(tmp);
  }, [lessonUser]);

  useEffect(() => {
    createOneLessonUser({
      variables: {
        inputLessonUser: {
          idLesson: idLesson,
          idUser: user._id,
          flag: '0%'
        }
      }
    })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }, []);

  const { data: allExam, refetch: refetchExam } = useQuery(FIND_ONE_EXAM, {
    variables: {
      filterExamination: {
        idLesson: idLesson
      }
    }
  });

  const { data: media, refetch } = useQuery(GET_ONE_MEDIA, {
    variables: {
      idMedia:
        contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson.length > 0
          ? contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson[0].idMedia
          : ''
    }
  });

  useEffect(() => {
    refetchContentLeson();
  }, [upload]);

  useEffect(() => {
    refetch();
  }, [contentLesson]);

  useEffect(() => {
    if (media && media.findOneMedia && media.findOneMedia.source) {
      setHaveAVideo(true);
    }
  }, [media]);

  useEffect(() => {
    console.log(idLesson);
  }, [idLesson]);

  const [play, setPlay] = useState(false);
  const [open, setOpen] = useState(false);
  const [exam, setExam] = useState({});

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardContent>
        <Grid container spacing={3}>
          <ButtonCustom
            onClick={() => {
              setPlay(true);
            }}
          >
            Play
          </ButtonCustom>
          {media && media.findOneMedia && media.findOneMedia.source ? (
            <ReactPlayer
              playing={play}
              onProgress={e => {
                console.log(e);
                arrayFlag.map((item, idx) => {
                  console.log(Math.round(e.playedSeconds));
                  console.log(play);
                  if (Math.round(item.flag) === Math.round(e.playedSeconds)) {
                    setPlay(false);
                    setExam(item);
                    setOpen(true);
                  }
                  updateOneLessonUser({
                    variables: {
                      idLessonUser: {
                        idLesson: idLesson,
                        idUser: user._id
                      },
                      inputLessonUser: {
                        idLesson: idLesson,
                        idUser: user._id,
                        flag:
                          flag === 100
                            ? '100%'
                            : flag > Math.round(e.played * 100) && flag !== undefined
                              ? flag + '%'
                              : e.playedSeconds !== 0
                                ? Math.round(e.played * 100) + '%'
                                : flag + '%'
                      }
                    }
                  })
                    .then(res => console.log(res))
                    .catch(err => console.log(err));
                });
              }}
              playsinline={true}
              url={media && media.findOneMedia && media.findOneMedia.source}
              controls={true}
            />
          ) : (
            <span
              style={{
                margin: '5rem',
                color: '#5850EC',
                fontSize: '2rem',
                fontWeight: '500'
              }}
            >
              Nothing to display!
            </span>
          )}
        </Grid>
      </CardContent>
      <DrawerDoQuiz open={open} setOpen={setOpen} item={exam} />
    </Card>
  );
}

Brief.propTypes = {
  project: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Brief;

const ButtonCustom = styled(Button)`
  background-color: #38aae8;

  &:hover {
    background-color: #a1ada7;
  }
`;