import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  // Drawer,
  // Drawer,
  FormHelperText,
  Grid,
  makeStyles,
  Paper,
  TextField,
  Typography
} from '@material-ui/core';
import { Drawer } from 'rsuite';
import WriteQuestion from './writeQuestion';
import ModalAddQuestion from './modalAddQuestion';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import styled from '@emotion/styled'

const GET_ALL_QUESTION = gql`
  query aggregateExaminationQuestion(
    $filterExaminationQuestion: FilterExaminationQuestion
  ) {
    aggregateExaminationQuestion(
      filterExaminationQuestion: $filterExaminationQuestion
    ) {
      question {
        _id
        title
        type
        isActive
      }
      examination {
        _id
        idLesson
        idContentLesson
        idCourse
        idDepartment
        type
      }
    }
  }
`;

const useStyles = makeStyles(() => ({
  drawerDesktopRoot: {
    width: '1000px',
  },
  title: {
    color: "#000",
  },
  drawerDesktopPaper: {
    position: 'relative',
    padding: 100,
  },
  drawerMobilePaper: {
    position: 'relative',
    width: 280
  },
  drawerMobileBackdrop: {
    position: 'absolute'
  }
}));

const DrawerQuizDetail = props => {
  const { open, setOpen, item } = props;
  const classes = useStyles();

  const [openApplication, setOpenApplication] = useState(false);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };
  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  console.log(item && item._id);
  const { data: allQuestion, refetch: refetchQuestion } = useQuery(
    GET_ALL_QUESTION,
    {
      variables: {
        filterExaminationQuestion: {
          idExamination: item && item._id
        }
      },
      fetchPolicy: 'no-cache'
    }
  );

  console.log(allQuestion && allQuestion);

  return (
    <>
      <Drawer
        // anchor="right"
        show={open}
        onHide={() => setOpen(false)}
        classes={{
          root: classes.drawerDesktopRoot,
          paper: classes.drawerDesktopPaper
        }}
      >
        <div
          style={{
            padding: '2rem',
            marginTop: '5rem',
            height: '70%',
            overflowY: 'auto'
          }}
        >
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
            color="textPrimary"
            style={{ marginBottom: '2rem' }}
          >
            {item.title}
          </Typography>
          {allQuestion &&
            allQuestion.aggregateExaminationQuestion.map((item, idx) => {
              if (item.question !== null) {
                return (
                  <WriteQuestion
                    item={item}
                    setOpenDrawer={setOpen}
                    refetchQuestion={refetchQuestion}
                  />
                );
              }
            })}

          <ModalAddQuestion
            onApply={handleApplicationClose}
            onClose={handleApplicationClose}
            open={openApplication}
            item={item}
            //   onDelete={handleDelete}
            refetchQuestion={refetchQuestion}
          />
        </div>
        <Drawer.Footer>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginTop: '20px'
            }}
            align="center"
          >
            <ButtonCustom
              color="primary"
              variant="contained"
              onClick={() => handleApplicationOpen(true)}
              style={{ marginRight: '1rem' }}
            >
              Create new question
            </ButtonCustom>
            <Button
              color="default"
              variant="contained"
              onClick={() => setOpen(false)}
            >
              Close
            </Button>
          </div>
        </Drawer.Footer>
      </Drawer>
    </>
  );
};

export default DrawerQuizDetail;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;