import React, { useEffect, useState } from 'react';
import ReactPlayer from 'react-player/lazy';
import { Modal } from 'rsuite';
import { Button, Box, TextField } from '@material-ui/core';
import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import { useSnackbar } from 'notistack';
import styled from "@emotion/styled"

const GET_ONE_CONTENT_LESSON = gql`
  query findManyContentLesson($filterContentLesson: FilterContentLesson) {
    findManyContentLesson(filterContentLesson: $filterContentLesson) {
      _id
      idLesson
      title
      idMedia
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const GET_ONE_MEDIA = gql`
  query findOneMedia($idMedia: ID!) {
    findOneMedia(idMedia: $idMedia) {
      _id
      title
      source
      isActive
      type
    }
  }
`;

const UPDATE_ONE_EXAM = gql`
  mutation updateOneExamination(
    $idExamination: ID!
    $inputExamination: InputExamination!
  ) {
    updateOneExamination(
      idExamination: $idExamination
      inputExamination: $inputExamination
    )
  }
`;

const ModalEditQuiz = props => {
  const { setOpenEdit, openEdit, item, refetchExam } = props;
  const [flag, setFlag] = useState(item.flag);
  const [updateOneExamination] = useMutation(UPDATE_ONE_EXAM);
  const { data: contentLesson, refetch: refetchContentLeson } = useQuery(
    GET_ONE_CONTENT_LESSON,
    {
      variables: {
        filterContentLesson: {
          idLesson: item && item.lesson && item.lesson._id
        }
      }
    }
  );

  const { data: media, refetch } = useQuery(GET_ONE_MEDIA, {
    variables: {
      idMedia:
        contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson.length > 0
          ? contentLesson &&
          contentLesson.findManyContentLesson &&
          contentLesson.findManyContentLesson[0].idMedia
          : ''
    }
  });
  const [type, setType] = useState('QUIZ');
  const [title, setTitle] = useState(item.title);
  const { enqueueSnackbar } = useSnackbar();
  useEffect(() => {
    console.log(item);
  }, [item]);

  useEffect(() => {
    setFlag(item.flag);
    setType(item.type);
  }, [openEdit]);

  const optionType = [
    {
      label: 'Mid Term',
      value: 'MID_TERM'
    },
    {
      label: 'Final',
      value: 'FINAL'
    },
    {
      label: 'Assignment',
      value: 'ASSIGNMENT'
    },
    {
      label: 'Quiz',
      value: 'QUIZ'
    }
  ];

  const handleSubmit = () => {
    console.log(flag, title, type);

    if (flag === 0 || title === '' || type === '') {
      enqueueSnackbar('Data can not null', {
        variant: 'err'
      });
    } else {
      updateOneExamination({
        variables: {
          idExamination: item._id,
          inputExamination: {
            flag: flag,
            type: type,
            title: title
          }
        }
      }).then(res => {
        if (res.data.updateOneExamination === true) {
          enqueueSnackbar(type + ' created successfully!', {
            variant: 'success'
          });
          setOpenEdit(false);
          refetchExam();
        }
      });
    }
  };

  return (
    <>
      <Modal show={openEdit} onHide={() => setOpenEdit(false)} size="md">
        <Modal.Header>
          <Modal.Title>Edit quiz</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {media && media.findOneMedia && media.findOneMedia.source ? (
            <ReactPlayer
              // onReady={e => console.log(e)}
              // onDuration={e => setFlag(e)}
              progressInterval={1000}
              style={{ width: '100%' }}
              onProgress={e => {
                setFlag(e.playedSeconds);
              }}
              playsinline={true}
              url={media && media.findOneMedia && media.findOneMedia.source}
              controls={true}
            />
          ) : (
            <span
              style={{
                margin: '5rem',
                color: '#5850EC',
                fontSize: '2rem',
                fontWeight: '500'
              }}
            >
              Nothing to display!
            </span>
          )}
          <Box
            mt={3}
          // p={3}
          >
            <TextFieldCustom
              fullWidth
              label="Quiz at"
              multiline
              variant="outlined"
              value={flag}
              disabled
            />
          </Box>
          <Box
            mt={3}
          // p={3}
          >
            <TextFieldCustom
              fullWidth
              label="Title"
              multiline
              variant="outlined"
              onChange={e => setTitle(e.target.value)}
              value={title}
            />
          </Box>
          <Box
            mt={3}
          // p={3}
          >
            <TextFieldCustom
              fullWidth
              label="Type"
              multiline
              variant="outlined"
              select
              SelectProps={{ native: true }}
              onChange={e => setType(e.target.value)}
              value={type}
            >
              {optionType.map(option => (
                <option key={option.value} value={option.value}>
                  {option.label}
                </option>
              ))}
            </TextFieldCustom>
          </Box>
        </Modal.Body>
        <Modal.Footer>
          <ButtonCustom
            color="primary"
            variant="contained"
            style={{ marginRight: '1rem' }}
            onClick={() => handleSubmit()}
          >
            Save
          </ButtonCustom>
          <ButtonCancel onClick={() => setOpenEdit(false)}>Cancel</ButtonCancel>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalEditQuiz;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;

const TextFieldCustom = styled(TextField)`
  .MuiTypography-colorTextPrimary {
    color: #000;
  }


  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }

  .MuiInputBase-input {
    color: #000;
  }

  .MuiFormLabel-root.Mui-disabled {
    color: #000;
  }

  .MuiOutlinedInput-root.Mui-disabled {
    .MuiOutlinedInput-notchedOutline {
      border-color: #000;
    }
  }
`;

const ButtonCancel = styled(Button)`
  .MuiButton-label {
    color: #000;
  }
`;