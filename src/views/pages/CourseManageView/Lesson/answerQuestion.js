import React, { useState, useEffect } from 'react';
import {
  TextField,
  Typography,
  Paper,
  Box,
  Radio,
  FormControl,
  RadioGroup,
  FormControlLabel
} from '@material-ui/core';

import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import styled from '@emotion/styled'

const GET_ALL_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
        isActive
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const AnswerQuestion = props => {
  const {
    values,
    item,
    setArrayAnswer,
    arrayAnswer,
    handleBlur,
    handleChange,
    touched,
    open,
    index
  } = props;

  const [idQuestion, setIdQuestion] = useState(
    item && item.question && item.question._id
  );
  const { data: allAnswer } = useQuery(GET_ALL_ANSWER, {
    variables: {
      filterQuestionAnswer: {
        idQuestion: idQuestion
      }
    }
  });

  //   useEffect(() => {
  //     console.log(arrayAnswer);
  //     let arr = [...arrayAnswer];
  //     arr[index].answer = answer;
  //     setArrayAnswer(arr);
  //   }, [answer]);
  const handleChangeEssay = e => {
    let arr = [...arrayAnswer];
    arr[index].answer = e.target.value;
    setArrayAnswer(arr);
  };

  useEffect(() => {
    console.log(arrayAnswer);
  }, [arrayAnswer]);

  const handleChangeMultiple = e => {
    let arr = [...arrayAnswer];
    console.log(arr, index);
    arr[index]['answer'] = e.target.value;
    setArrayAnswer(arr);
  };

  return (
    <Wrapper>
      <div style={{ fontSize: '1rem', marginBottom: '1rem', color: '#000' }}>
        {item && item.question && item.question.title}
      </div>
      {item && item.question && item.question.type === 'ESSAY' ? (
        <TextField
          fullWidth
          label="Question"
          name={item && item.question._id}
          variant="outlined"
          style={{ marginBottom: '2rem' }}
          fullWidth
          //   helperText={touched.item && item.question._id && item.question._id}
          onBlur={handleBlur}
          onChange={e => handleChangeEssay(e)}
          value={
            (arrayAnswer && arrayAnswer[index] && arrayAnswer[index].answer) ||
            ''
          }
          variant="outlined"
        />
      ) : (
        <FormControl component="fieldset">
          <RadioGroup onChange={e => handleChangeMultiple(e)}>
            {allAnswer &&
              allAnswer.aggregateQuestionAnswer.map((item, idx) => {
                if (item && item.answer !== null) {
                  return (
                    <FormControlLabelCustom
                      value={item && item.answer && item.answer._id}
                      control={<Radio />}
                      label={item && item.answer && item.answer.title}
                    />
                  );
                }
              })}
          </RadioGroup>
        </FormControl>
      )}
    </Wrapper>
  );
};

export default AnswerQuestion;

const FormControlLabelCustom = styled(FormControlLabel)`
  .MuiTypography-body1 {
    color: #000;
  }
`;

const Wrapper = styled.div`
  border: 1px solid #000;
  margin-bottom: 20px;
  padding: 1rem;
`;