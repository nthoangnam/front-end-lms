import {
  Box,
  Button,
  Dialog,
  makeStyles,
  Typography,
  TextField,
  Paper,
  Radio
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { useMutation } from 'react-apollo';
import gql from 'graphql-tag';
import { result } from 'lodash';
import styled from "@emotion/styled"

const CREATE_ONE_QUESTION = gql`
  mutation createOneQuestion($inputQuestion: InputQuestion!) {
    createOneQuestion(inputQuestion: $inputQuestion)
  }
`;

const CREATE_ONE_EXAM_QUESTION = gql`
  mutation createOneExaminationQuestion(
    $inputExaminationQuestion: InputExaminationQuestion!
  ) {
    createOneExaminationQuestion(
      inputExaminationQuestion: $inputExaminationQuestion
    ) {
      idExamination
      idQuestion
    }
  }
`;

const CREATE_ONE_ANSWER = gql`
  mutation createOneAnswer($inputAnswer: InputAnswer!) {
    createOneAnswer(inputAnswer: $inputAnswer)
  }
`;

const CREATE_ONE_QUESTION_ANSWER = gql`
  mutation createOneQuestionAnswer($inputQuestionAnswer: InputQuestionAnswer!) {
    createOneQuestionAnswer(inputQuestionAnswer: $inputQuestionAnswer) {
      idQuestion
      idAnswer
    }
  }
`;
function ModalAddQuestion({
  refetchQuestion,
  item,
  refetchExam,
  project,
  idLesson,
  setUpload,
  upload,
  setHaveAVideo,
  haveAVideo,
  open,
  onClose,
  onApply,
  FIND_ONE_EXAM,
  className,
  ...rest
}) {
  const [createOneQuestion] = useMutation(CREATE_ONE_QUESTION);
  const [createOneExaminationQuestion] = useMutation(CREATE_ONE_EXAM_QUESTION);
  const [createOneAnswer] = useMutation(CREATE_ONE_ANSWER);
  const [createOneQuestionAnswer] = useMutation(CREATE_ONE_QUESTION_ANSWER);

  const useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(3)
    },
    helperText: {
      textAlign: 'right',
      marginRight: 0
    }
  }));
  const classes = useStyles();

  const optionQuestionType = [
    {
      label: 'Multiple Choice',
      value: 'MULTIPLE_CHOICE'
    },
    {
      label: 'Essay question',
      value: 'ESSAY'
    }
  ];

  const [type, setType] = useState('MULTIPLE_CHOICE');
  const [question, setQuestion] = useState('');
  const [score, setScore] = useState(0);
  const { enqueueSnackbar } = useSnackbar();

  const checkNull = () => {
    let flag = [];
    answer.map((item, idx) => {
      if (item.title === '') {
        enqueueSnackbar('Answer can not be null!', {
          variant: 'error'
        });
        flag.push('Error');
      }
    });
    if (flag.length === 0) {
      return false;
    } else {
      return true;
    }
  };

  const handleSubmit = () => {
    if (type === 'ESSAY') {
      if (question === '' || score < 0) {
        enqueueSnackbar('Something wrong!', {
          variant: 'error'
        });
      } else {
        console.log(question, type);
        createOneQuestion({
          variables: {
            inputQuestion: {
              title: question,
              type: type
            }
          }
        })
          .then(res => {
            console.log(res);
            if (res && res.data.createOneQuestion !== null) {
              // enqueueSnackbar('Course Update successfully!', {
              //   variant: 'success'
              // });
              console.log(item && item._id, res.data.createOneQuestion);
              createOneExaminationQuestion({
                variables: {
                  inputExaminationQuestion: {
                    idExamination: item && item._id,
                    idQuestion: res.data.createOneQuestion,
                    score: 100
                  }
                }
              })
                .then(res => {
                  if (res && res.data.createOneExaminationQuestion !== null) {
                    refetchQuestion();
                    enqueueSnackbar('Question create successfully!', {
                      variant: 'success'
                    });
                    onClose();
                  }
                })
                .catch(err => console.log(err));
            }
          })
          .catch(err => console.log(err));
      }
    } else {
      if (answer.length <= 1) {
        enqueueSnackbar('At least two answer!', {
          variant: 'error'
        });
      } else if (answer.length > 1) {
        console.log(checkNull());
        if (checkNull() === false) {
          console.log('ok');
          if (question === '' || score < 0) {
            enqueueSnackbar('Something wrong!', {
              variant: 'error'
            });
          } else {
            console.log(question, type);
            createOneQuestion({
              variables: {
                inputQuestion: {
                  title: question,
                  type: type
                }
              }
            })
              .then(result => {
                console.log(result);
                if (result && result.data.createOneQuestion !== null) {
                  createOneExaminationQuestion({
                    variables: {
                      inputExaminationQuestion: {
                        idExamination: item && item._id,
                        idQuestion: result.data.createOneQuestion
                      }
                    }
                  })
                    .then(res => {
                      if (
                        res &&
                        res.data.createOneExaminationQuestion !== null
                      ) {
                        answer.map(async (item, idx) => {
                          await createOneAnswer({
                            variables: {
                              inputAnswer: {
                                title: item.title
                              }
                            }
                          })
                            .then(async res => {
                              console.log(res);
                              console.log(
                                result.data.createOneQuestion,
                                res.data.createOneAnswer,
                                item.correct
                              );
                              await createOneQuestionAnswer({
                                variables: {
                                  inputQuestionAnswer: {
                                    idQuestion: result.data.createOneQuestion,
                                    idAnswer: res.data.createOneAnswer,
                                    correct: item.correct
                                  }
                                }
                              })
                                .then(res => {
                                  if (
                                    res.data.createOneQuestionAnswer !== null
                                  ) {
                                    //   enqueueSnackbar('Question create successfully!', {
                                    //     variant: 'success'
                                    //   });
                                    //   onClose();
                                    if (idx === answer.length - 1) {
                                      enqueueSnackbar(
                                        'Question create successfully!',
                                        {
                                          variant: 'success'
                                        }
                                      );
                                    }
                                  }
                                })
                                .catch(err => console.log(err));
                            })
                            .catch(err => console.log(err));
                        });
                        refetchQuestion();
                        onClose();
                      }
                    })
                    .catch(err => console.log(err));
                }
              })
              .catch(err => console.log(err));
          }
        }
      }
    }
  };

  const [answer, setAnswer] = useState([
    {
      correct: true,
      title: ''
    }
  ]);

  const handleAddAnswer = () => {
    const newItem = {
      correct: false,
      title: ''
    };

    let arr = [...answer, newItem];
    setAnswer(arr);
  };

  const handleDelete = idx => {
    let arr = [...answer];
    arr.splice(idx, 1);
    setAnswer(arr);
  };

  const handleChangeValue = index => {
    let arr = [...answer];
    arr.map((item, idx) => {
      if (idx === index) {
        item.correct = true;
      } else {
        item.correct = false;
      }
    });
    setAnswer(arr);
  };

  const handleChangeValueOption = index => e => {
    let arr = [...answer];
    arr.map((item, idx) => {
      if (idx === index) {
        item.title = e.target.value;
      }
    });
    setAnswer(arr);
  };

  useEffect(() => {
    console.log(answer);
  }, [answer]);

  return (
    <DialogCustom
      maxWidth="lg"
      onClose={onClose}
      open={open}
      style={{ padding: '1rem' }}
    >
      <div className={clsx(classes.root, className)} {...rest}>
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
          style={{
            paddingRight: '5rem',
            paddingLeft: '5rem',
            marginTop: '1rem'
          }}
        >
          CREATE NEW QUESTION
        </Typography>
        <Box mt={3}>
          <TextField
            fullWidth
            label="Type of question"
            name="type"
            select
            // onBlur={handleBlur}
            onChange={e => setType(e.target.value)}
            value={type}
            variant="outlined"
          >
            {optionQuestionType.map(option => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </TextField>
        </Box>
        <Box mt={3}>
          <TextField
            fullWidth
            label="Question"
            name="question"
            onChange={e => setQuestion(e.target.value)}
            value={question}
            variant="outlined"
          />
        </Box>
        {type === 'MULTIPLE_CHOICE'
          ? answer.map((item, idx) => {
            return (
              <Paper
                // display="flex"
                // alignItems="center"
                mt={3}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                p={2}
                mb={2}
                component={Box}
              // elevation={type === typeOption.value ? 10 : 1}
              // key={typeOption.value}
              >
                <Radio
                  checked={item.correct}
                  onClick={() => handleChangeValue(idx)}
                />
                <TextField
                  fullWidth
                  label="Answer"
                  name={idx}
                  onChange={handleChangeValueOption(idx)}
                  // value={question}
                  variant="outlined"
                />
                <Button
                  style={{ marginLeft: '10px' }}
                  onClick={() => handleDelete(idx)}
                >
                  Delete
                </Button>
              </Paper>
            );
          })
          : ''}
        {/* <Box mt={3}>
          <TextField
            type="number"
            fullWidth
            label="Score"
            name="score"
            onChange={e => setScore(e.target.value)}
            value={score}
            variant="outlined"
          />
        </Box> */}
        <Box mt={3} p={3} style={{ display: 'flex' }}>
          {type === 'MULTIPLE_CHOICE' ? (
            <ButtonCustom
              variant="contained"
              fullWidth
              color="primary"
              style={{ marginRight: '1rem' }}
              onClick={() => handleAddAnswer()}
            >
              Add more answer
            </ButtonCustom>
          ) : (
            ''
          )}
          <ButtonCustom
            variant="contained"
            fullWidth
            color="primary"
            style={{ marginRight: '1rem' }}
            onClick={() => handleSubmit()}
          >
            SUBMIT
          </ButtonCustom>
          <Button variant="contained" fullWidth onClick={onClose}>
            CANCEL
          </Button>
        </Box>
      </div>
    </DialogCustom>
  );
}

ModalAddQuestion.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

ModalAddQuestion.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default ModalAddQuestion;

const DialogCustom = styled(Dialog)`
  .MuiPaper-root {
    background-color: #fff;
  }

  .MuiFormLabel-root {
    color: #000;
  }

  .MuiFormLabel-root.Mui-disabled {
    color: #000;
  }

  .MuiTypography-colorTextPrimary {
    color: #000;
  }


  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }

  .MuiOutlinedInput-root.Mui-disabled {
    .MuiOutlinedInput-notchedOutline {
      border-color: #000;
    }
  }

  .MuiInputBase-input {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;