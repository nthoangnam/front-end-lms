import React, { useEffect, useState } from 'react';
import {
  Card,
  CardContent,
  Grid,
  makeStyles,
  Button,
  SvgIcon
} from '@material-ui/core';
import clsx from 'clsx';
import { PlusCircle as PlusCircleIcon } from 'react-feather';
import ModalAddQuiz from './modalAddQuiz';
import { useHistory } from 'react-router';
import QuizItem from './quizItem';
import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import { useSelector } from 'react-redux';
import styled from "@emotion/styled"

const FIND_ONE_EXAM = gql`
  query aggregateExamination($filterExamination: FilterExamination) {
    aggregateExamination(filterExamination: $filterExamination) {
      _id
      lesson {
        title
        _id
      }
      title
      type
      isActive
      flag
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {},
  markdown: {
    fontFamily: theme.typography.fontFamily,
    '& p': {
      marginBottom: theme.spacing(2)
    }
  }
}));

const QuizList = ({
  project,
  idLesson,
  setUpload,
  upload,
  setHaveAVideo,
  haveAVideo,
  className,
  arrayFlag,
  setArrayFlag,
  ...rest
}) => {
  const history = useHistory();
  const classes = useStyles();
  const [openApplication, setOpenApplication] = useState(false);
  const { user } = useSelector(state => state.account);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };
  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const { data: allExam, refetch: refetchExam } = useQuery(FIND_ONE_EXAM, {
    variables: {
      filterExamination: {
        idLesson: idLesson
      }
    }
  });

  useEffect(() => {
    let arr = [];
    allExam &&
      allExam.aggregateExamination.map((item, idx) => {
        arr.push(item);
      });
    setArrayFlag(arr);
  }, [allExam && allExam]);

  useEffect(() => {
    console.log(arrayFlag);
  }, [arrayFlag]);

  console.log(allExam && allExam);

  return (
    <>
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Grid container spacing={3} style={{ display: 'flex' }}>
            {/* <QuizItem /> */}
            {/* <QuizItem /> */}
            {allExam &&
              allExam.aggregateExamination.map((item, idx) => {
                return <QuizItem item={item} refetchExam={refetchExam} />;
              })}
            {user && user.idRole !== 'STUDENT' ? (
              <ButtonCustom
                style={{
                  width: '100%',
                  fontSize: '1rem',
                  borderStyle: 'dashed',
                  borderWidth: '2px',
                  borderColor: '#707070',
                  marginRight: '0.5rem'
                }}
                //   className={classes.action}
                //   to="/app/courseManagement/create"
                color="primary"
                variant="contained"
                onClick={() => handleApplicationOpen()}
              >
                <SvgIcon
                  fontSize="medium"
                  className={classes.actionIcon}
                  style={{ marginRight: '1rem' }}
                >
                  <PlusCircleIcon />
                </SvgIcon>
                New Quiz
              </ButtonCustom>
            ) : (
              ''
            )}
          </Grid>
        </CardContent>
        <ModalAddQuiz
          onApply={handleApplicationClose}
          onClose={handleApplicationClose}
          open={openApplication}
          //   onDelete={handleDelete}
          FIND_ONE_EXAM={FIND_ONE_EXAM}
          project={project}
          idLesson={history.location.pathname.slice(59)}
          setUpload={setUpload}
          upload={upload}
          setHaveAVideo={setHaveAVideo}
          haveAVideo={haveAVideo}
          refetchExam={refetchExam}
        />
      </Card>
    </>
  );
};

export default QuizList;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;