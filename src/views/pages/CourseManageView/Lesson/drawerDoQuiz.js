import React, { useState, useEffect } from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Drawer,
  FormHelperText,
  Grid,
  makeStyles,
  Paper,
  TextField,
  Typography
} from '@material-ui/core';
import clsx from 'clsx';
import { Formik } from 'formik';
import WriteQuestion from './writeQuestion';
import ModalAddQuestion from './modalAddQuestion';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import AnswerQuestion from './answerQuestion';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const GET_ALL_QUESTION = gql`
  query aggregateExaminationQuestion(
    $filterExaminationQuestion: FilterExaminationQuestion
  ) {
    aggregateExaminationQuestion(
      filterExaminationQuestion: $filterExaminationQuestion
    ) {
      question {
        _id
        title
        type
        isActive
      }
      examination {
        _id
        idLesson
        idContentLesson
        idCourse
        idDepartment
        type
      }
    }
  }
`;

const CREATE_ONE_ANSWER = gql`
  mutation createOneAnswer($inputAnswer: InputAnswer!) {
    createOneAnswer(inputAnswer: $inputAnswer)
  }
`;

const CREATE_ONE_QUESTION_ANSWER = gql`
  mutation createOneQuestionAnswer($inputQuestionAnswer: InputQuestionAnswer!) {
    createOneQuestionAnswer(inputQuestionAnswer: $inputQuestionAnswer) {
      idQuestion
      idAnswer
    }
  }
`;

const CREATE_ONE_QUESTION_USER = gql`
  mutation createOneQuestionUser($inputQuestionUser: InputQuestionUser!) {
    createOneQuestionUser(inputQuestionUser: $inputQuestionUser) {
      idExamination
      idQuestion
      idUser
    }
  }
`;

const AGGREGATE_QUESTION_ANSWER = gql`
  query aggregateQuestionAnswer($filterQuestionAnswer: FilterQuestionAnswer) {
    aggregateQuestionAnswer(filterQuestionAnswer: $filterQuestionAnswer) {
      question {
        _id
        title
        type
        isActive
      }
      answer {
        _id
        title
        isActive
      }
      correct
    }
  }
`;

const useStyles = makeStyles(() => ({
  drawerDesktopRoot: {
    width: '1000px'
  },
  drawerDesktopPaper: {
    position: 'relative',
    padding: 100,
    background: '#fff'
  },
  drawerMobilePaper: {
    position: 'relative',
    width: 280
  },
  drawerMobileBackdrop: {
    position: 'absolute'
  }
}));

const DrawerDoQuiz = props => {
  const { open, setOpen, item, className, rest } = props;
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useSelector(state => state.account);
  console.log(user);
  const [openApplication, setOpenApplication] = useState(false);
  const [arrayAnswer, setArrayAnswer] = useState([]);
  const [createOneAnswer] = useMutation(CREATE_ONE_ANSWER);
  const [createOneQuestionAnswer] = useMutation(CREATE_ONE_QUESTION_ANSWER);
  const [createOneQuestionUser] = useMutation(CREATE_ONE_QUESTION_USER);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };
  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  console.log(item && item._id);
  console.log(item);
  const { data: allQuestion, refetch: refetchQuestion } = useQuery(
    GET_ALL_QUESTION,
    {
      variables: {
        filterExaminationQuestion: {
          idExamination: item && item._id
        }
      }
    }
  );

  console.log(allQuestion && allQuestion);

  useEffect(() => {
    console.log(arrayAnswer);
  }, [arrayAnswer]);

  const [initValue, setInitValue] = useState({});

  useEffect(() => {
    let item = { ...initValue };
    allQuestion &&
      allQuestion.aggregateExaminationQuestion.map((ele, idx) => {
        item[ele.question._id] = '';
      });
    setInitValue(item);
    console.log(item);
  }, [open]);

  useEffect(() => {
    console.log(initValue);
  }, [initValue]);

  const [listQuestionID, setListQuestionID] = useState([]);

  const { data: dataQuestionAnswer, refetch: refetchQuestionAnswer } = useQuery(
    AGGREGATE_QUESTION_ANSWER
  );

  useEffect(() => {
    let arr = [];
    let questionID = [];
    allQuestion &&
      allQuestion.aggregateExaminationQuestion.map((ele, idx) => {
        console.log(ele);
        questionID.push(ele.question._id);
        arr.push(ele.question);
      });
    console.log(arr);
    setArrayAnswer(arr);
    setListQuestionID(questionID);
  }, [open, allQuestion]);

  const [correctAns, setCorrectAns] = useState([]);

  useEffect(() => {
    if (listQuestionID.length > 0) {
      listQuestionID.map((item, idx) => {
        refetchQuestionAnswer({
          filterQuestionAnswer: {
            idQuestion: item
          }
        })
          .then(res => {
            console.log(res.data.aggregateQuestionAnswer);
            let arr = [];
            let tmp = '1';
            res.data &&
              res.data.aggregateQuestionAnswer.map((item, idx) => {
                console.log(item);

                // item.map((ele, index) => {
                if (item.correct === true) {
                  tmp = item.answer._id;
                }
                // });
              });
            arr.push(tmp);
            console.log(tmp);
            let tmpArr = [...correctAns, ...arr];
            setCorrectAns(tmpArr);
          })
          .catch(err => console.log(err));
      });
    }
  }, [listQuestionID]);

  const [final, setFinal] = useState([]);

  useEffect(() => {
    console.log(correctAns);
    let tmp = [...final, ...correctAns];
    console.log(tmp);
    setFinal(tmp);
  }, [correctAns]);

  return (
    <>
      <Drawer
        anchor="right"
        open={open}
        classes={{
          root: classes.drawerDesktopRoot,
          paper: classes.drawerDesktopPaper
        }}
      >
        <TypographyCustom
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
          style={{ marginBottom: '2rem' }}
        >
          {item.title}
        </TypographyCustom>
        <Formik
          initialValues={initValue}
          onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
            try {
              // Do api call
              arrayAnswer.map((question, idx) => {
                if (question.type === 'ESSAY') {
                  createOneAnswer({
                    variables: {
                      inputAnswer: {
                        title: question.answer
                      }
                    }
                  })
                    .then(res => {
                      createOneQuestionAnswer({
                        variables: {
                          inputQuestionAnswer: {
                            idQuestion: question._id,
                            idAnswer: res.data.createOneAnswer
                          }
                        }
                      }).then(result => {
                        if (result.data.createOneQuestionAnswer !== null) {
                          createOneQuestionUser({
                            variables: {
                              inputQuestionUser: {
                                idExamination:
                                  allQuestion &&
                                  allQuestion.aggregateExaminationQuestion[idx]
                                    .examination._id,
                                idQuestion: question._id,
                                idAnswer: res.data.createOneAnswer,
                                idUser: user._id
                              }
                            }
                          })
                            .then(res => {
                              console.log(res);
                            })
                            .catch(err => {
                              console.log(err);
                            });
                        }
                      });
                    })
                    .catch(err => {
                      console.log(err);
                    });
                } else if (question.type === 'MULTIPLE_CHOICE') {
                  let flag = false
                  final.map((item, idx) => {
                    if (item === question.answer) {
                      flag = true
                    }
                  })
                  createOneQuestionUser({
                    variables: {
                      inputQuestionUser: {
                        idExamination:
                          allQuestion &&
                          allQuestion.aggregateExaminationQuestion[idx]
                            .examination._id,
                        idQuestion: question._id,
                        idAnswer: question.answer,
                        idUser: user._id,
                        correct: flag
                      }
                    }
                  })
                    .then(res => {
                      console.log(res);
                    })
                    .catch(err => {
                      console.log(err);
                    });
                }
              });
              enqueueSnackbar('Submit successfully!', {
                variant: 'success'
              });
              setOpen(false);
            } catch (err) {
              console.log(err);
              setErrors({ submit: err.message });
              setStatus({ success: false });
              setSubmitting(false);
            }
          }}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            touched,
            values
          }) => (
            <form
              onSubmit={handleSubmit}
              className={clsx(classes.root, className)}
              {...rest}
            >
              {allQuestion &&
                allQuestion.aggregateExaminationQuestion.map((item, idx) => {
                  return (
                    <AnswerQuestion
                      item={item}
                      arrayAnswer={arrayAnswer}
                      setArrayAnswer={setArrayAnswer}
                      handleBlur={handleBlur}
                      handleChange={handleChange}
                      touched={touched}
                      open={open}
                      values={values}
                      index={idx}
                    />
                  );
                })}

              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  marginTop: '20px'
                }}
                align="center"
              >
                {/* <Button
            color="primary"
            variant="contained"
            onClick={() => handleApplicationOpen(true)}
            style={{ marginRight: '1rem' }}
          >
            Create new question
          </Button> */}
                <ButtonCustom
                  color="primary"
                  variant="contained"
                  // onClick={() => handleApplicationOpen(true)}
                  style={{ marginRight: '1rem' }}
                  type="submit"
                >
                  Submit
                </ButtonCustom>
                <Button
                  color="default"
                  variant="contained"
                  onClick={() => setOpen(false)}
                >
                  Close
                </Button>
              </div>
            </form>
          )}
        </Formik>
        <ModalAddQuestion
          onApply={handleApplicationClose}
          onClose={handleApplicationClose}
          open={openApplication}
          item={item}
          //   onDelete={handleDelete}
          refetchQuestion={refetchQuestion}
        />
      </Drawer>
    </>
  );
};

export default DrawerDoQuiz;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedPrimary {
    background-color: #38aae8;
  }
`;