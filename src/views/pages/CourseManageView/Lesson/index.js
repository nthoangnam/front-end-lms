import { Box, Grid, makeStyles, Container } from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Page from 'src/components/Page';
import React, { useState, useEffect } from 'react';
import Brief from './Brief';
import Members from './Members';
import Header from './Header';
import gql from 'graphql-tag';
import { useQuery, useMutation } from 'react-apollo';
import { useHistory } from 'react-router';
import QuizList from './quizList';
import QuizItem from './quizItem';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "#ebe8e4",
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

const GET_DETAIL_LESSON = gql`
  query findOneLesson($idLesson: ID!) {
    findOneLesson(idLesson: $idLesson) {
      _id
      title
      idCourse
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

const CREATE_ONE_MEDIA = gql`
  mutation createOneMedia($inputMedia: InputMedia!) {
    createOneMedia(inputMedia: $inputMedia)
  }
`;

function LessonDetail({ className, ...rest }) {
  const classes = useStyles();
  const history = useHistory();
  const { user } = useSelector(state => state.account);
  const { data: lessonData } = useQuery(GET_DETAIL_LESSON, {
    variables: {
      idLesson: history.location.pathname.slice(59)
    }
  });


  console.log(lessonData);

  const [project, setProject] = useState({
    active: true,
    activities: [
      {
        id: '5e8dd0828d628e6f40abdfe8',
        subject: 'Project owner',
        type: 'upload_file',
        description: 'has uploaded a new file',
        createdAt: 1599893053613
      },
      {
        id: '5e8dd0893a6725f2bb603617',
        subject: 'Adrian Stefan',
        type: 'join_team',
        description: 'joined team as a Front-End Developer',
        createdAt: 1599887233613
      },
      {
        id: '5e8dd08f44603e3300b75cf1',
        subject: 'Alexndru Robert',
        type: 'join_team',
        description: 'joined team as a Full Stack Developer',
        createdAt: 1599862033613
      },
      {
        id: '5e8dd0960f3f0fe04e64d8f4',
        subject: 'Project owner',
        type: 'price_change',
        description: 'raised the project budget',
        createdAt: 1599721633613
      },
      {
        id: '5e8dd09db94421c502c53d13',
        subject: 'Contest',
        type: 'contest_created',
        description: 'created',
        createdAt: 1599548833613
      }
    ],
    author: {
      id: '5e887d0b3d090c1b8f162003',
      name: 'Emilee Simchenko',
      avatar: '/static/images/avatars/avatar_9.png',
      bio: 'We build beautiful functional themes for web professionals'
    },
    brief: `"↵Design files are attached in the files tab.↵↵Develop the web app screens for our product called "'PDFace'". Please look at the wireframes, system activity workflow and read the section above to understand what we're trying to archive.↵↵There's not many screens we need designed, but there will be modals and various other system triggered events that will need to be considered.↵↵The project has been created in Sketch so let me know if there are any problems opening this project and I'll try to convert into a usable file.↵    "`,
    currency: 'USD',
    deadline: 1600499233613,
    endDate: 1601104033613,
    files: [
      {
        id: '5e8dd0721b9e0fab56d7238b',
        name: 'example-project1.jpg',
        url: '/static/images/projects/project_4.png',
        mimeType: 'image/png',
        size: 3145728
      },
      {
        id: '5e8dd0784431995a30eb2586',
        name: 'docs.zip',
        url: '#',
        mimeType: 'application/zip',
        size: 26214400
      },
      {
        id: '5e8dd07cbb62749296ecee1c',
        name: 'example-project2.jpg',
        url: '/static/images/projects/project_1.png',
        mimeType: 'image/png',
        size: 2097152
      }
    ],
    members: [
      {
        id: '5e887a62195cc5aef7e8ca5d',
        name: 'Ekaterina Tankova',
        avatar: '/static/images/avatars/avatar_2.png',
        bio: 'Front End Developer'
      },
      {
        id: '5e887ac47eed253091be10cb',
        name: 'Cao Yu',
        avatar: '/static/images/avatars/avatar_3.png',
        bio: 'UX Designer'
      },
      {
        id: '5e887b7602bdbc4dbb234b27',
        name: 'Anje Keizer',
        avatar: '/static/images/avatars/avatar_5.png',
        bio: 'Copyright'
      }
    ],
    price: '12,500',
    subscribers: [
      {
        id: '5e887a62195cc5aef7e8ca5d',
        name: 'Ekaterina Tankova',
        avatar: '/static/images/avatars/avatar_2.png',
        cover: '/static/images/covers/cover_2.jpg',
        commonContacts: 12
      },
      {
        id: '5e887ac47eed253091be10cb',
        name: 'Cao Yu',
        avatar: '/static/images/avatars/avatar_3.png',
        cover: '/static/images/covers/cover_3.jpg',
        commonContacts: 5
      },
      {
        id: '5e86809283e28b96d2d38537',
        name: 'Katarina Smith',
        avatar: '/static/images/avatars/logo.png',
        cover: '/static/images/covers/cover.png',
        commonContacts: 17
      }
    ],
    tags: ['React JS'],
    title: 'Develop a PDF Export App',
    updatedAt: 1599893053613
  });

  const [createOneMedia] = useMutation(CREATE_ONE_MEDIA);

  const [upload, setUpload] = useState(false);
  const [haveAVideo, setHaveAVideo] = useState(false);
  const [arrayFlag, setArrayFlag] = useState([]);

  return (
    <Page className={classes.root} title="Course Details">
      <Container maxWidth="lg">
        <Header
          project={project}
          data={lessonData && lessonData.findOneLesson}
          createOneMedia={createOneMedia}
          setUpload={setUpload}
          upload={upload}
          setHaveAVideo={setHaveAVideo}
          haveAVideo={haveAVideo}
        />
        <Grid
          className={clsx(classes.root, className)}
          container
          spacing={3}
          {...rest}
        >
          <Grid item lg={8} xl={9} xs={12}>
            <Brief
              project={project}
              idLesson={history.location.pathname.slice(59)}
              setUpload={setUpload}
              upload={upload}
              setHaveAVideo={setHaveAVideo}
              haveAVideo={haveAVideo}
              arrayFlag={arrayFlag}
            />
          </Grid>
          <Grid item lg={4} xl={3} xs={12}>
            <Members members={project.members} />
            <Box mt={3}></Box>
          </Grid>
          {/* {user && user.idRole === 'STUDENT' ? (
            ''
          ) : ( */}
          <Grid item lg={8} xl={9} xs={12}>
            <QuizList
              project={project}
              idLesson={history.location.pathname.slice(59)}
              setUpload={setUpload}
              upload={upload}
              setHaveAVideo={setHaveAVideo}
              haveAVideo={haveAVideo}
              setArrayFlag={setArrayFlag}
            />
          </Grid>
          {/* )} */}
        </Grid>
      </Container>
    </Page>
  );
}

LessonDetail.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default LessonDetail;
