import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import {
  Avatar,
  Box,
  Button,
  Dialog,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import getInitials from 'src/utils/getInitials';
import FieldDropzone from './FilesDropzone'
import gql from 'graphql-tag'
import { useMutation, useQuery } from 'react-apollo';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  }
}));

const CREATE_ONE_CONTENT_LESSON = gql`
mutation createOneContentLesson($inputContentLesson: InputContentLesson!){
  createOneContentLesson(inputContentLesson:$inputContentLesson)
}
`

const GET_ONE_CONTENT_LESSON = gql`
query findManyContentLesson($filterContentLesson: FilterContentLesson){
  findManyContentLesson(filterContentLesson: $filterContentLesson){
    _id
    idLesson
    title
    idMedia
    isActive
    createdAt
    updatedAt
    createdBy
    updatedBy
  }
}
`

function Application({
  setUpload,
  data,
  createOneMedia,
  author,
  open,
  onClose,
  onApply,
  className,
  ...rest
}) {
  const [value, setValue] = useState('');
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [isSubmit, setIsSubmit] = useState(false)

  const { data: contentLesson, refetch: refetchContentLeson } = useQuery(GET_ONE_CONTENT_LESSON, {
    variables: {
      filterContentLesson: {
        idLesson: data && data._id
      }
    }
  })

  // const handleChange = (event) => {
  //   event.persist();
  //   setValue(event.target.value);
  // };

  // const handleApply = () => {
  //   enqueueSnackbar('Request sent', {
  //     variant: 'success'
  //   });
  //   onApply();
  // };

  const [idImage, setIdImage] = useState('')

  const [createOneContentLesson] = useMutation(CREATE_ONE_CONTENT_LESSON)
  const [idMedia, setIdMedia] = useState('')

  useEffect(() => {
  }, [idMedia])

  const handleSubmitUploadFile = () => {
    console.log(data && data.title, data && data._id, idMedia)
    createOneContentLesson({
      variables: {
        inputContentLesson: {
          title: data && data.title,
          idLesson: data && data._id,
          idMedia: idMedia
        }
      }
    }).then(res => {
      if (res.data.createOneContentLesson !== null) {
        refetchContentLeson()
        enqueueSnackbar('Upload video successfully!', {
          variant: 'success'
        })
        setUpload(true)
        onClose()
      }
    })
      .catch(err => console.log(err))
  }
  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        className={clsx(classes.root, className)}
        {...rest}
      >
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h3"
          color="textPrimary"
        >
          Upload Lesson Video
        </Typography>
        <Typography
          align="center"
          variant="subtitle2"
          color="textSecondary"
        >
          Write down a short note with your application regarding why you
          think you&apos;d be a good fit for this position.
        </Typography>
        <Box mt={3}>
          <FieldDropzone 
          idLesson={data && data._id}
          setUpload={setUpload} setIsSubmit={setIsSubmit} isSubmit={isSubmit} setIdImage={setIdImage} idImage={idImage} createOneMedia={createOneMedia} data={data && data} setIdMedia={setIdMedia} idMedia={idMedia} />
        </Box>
        <Box
          mt={3}
          p={3}
        >
          <Button
            onClick={() => handleSubmitUploadFile()}
            variant="contained"
            fullWidth
            color="primary"
            disabled={isSubmit === true ? false : true}
          >
            UPLOAD VIDEO
          </Button>
        </Box>
      </div>
    </Dialog>
  );
}

Application.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

Application.defaultProps = {
  onApply: () => { },
  onClose: () => { }
};

export default Application;
