import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Box,
  Button,
  Grid,
  SvgIcon,
  Typography,
  makeStyles
} from '@material-ui/core';
import {
  Share2 as ShareIcon,
  Check as CheckIcon,
  Calendar as CalendarIcon,
  AlertTriangle as AlertIcon,
  DollarSign as DollarSignIcon,
  Send as SendIcon
} from 'react-feather';
import Application from './Application';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const useStyles = makeStyles(theme => ({
  root: {},
  badge: {
    display: 'flex',
    alignItems: 'center',
    margin: theme.spacing(2),
    color: "#000"
  },
  badgeIcon: {
    marginRight: theme.spacing(1)
  },
  action: {
    marginBottom: theme.spacing(1),
    '& + &': {
      marginLeft: theme.spacing(1)
    }
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

function Header({
  setHaveAVideo,
  haveAVideo,
  setUpload,
  title,
  setTitle,
  createOneMedia,
  data,
  project,
  className,
  ...rest
}) {
  const { user } = useSelector(state => state.account);
  const classes = useStyles();
  const [openApplication, setOpenApplication] = useState(false);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  return (
    <Grid
      container
      spacing={3}
      justify="space-between"
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid item>
        <TypographyCustom variant="h3" color="textPrimary">
          {data && data.title}
        </TypographyCustom>
        <Box
          mx={-2}
          display="flex"
          color="text.secondary"
          alignItems="center"
          flexWrap="wrap"
        >
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              {project.active ? <CheckIcon /> : <AlertIcon />}
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {'Active'}
            </Typography>
          </div>
          <div className={classes.badge}>
            <SvgIcon fontSize="small" className={classes.badgeIcon}>
              <CalendarIcon />
            </SvgIcon>
            <Typography variant="body2" color="inherit" component="span">
              {`Uploaded ${moment(data && data.createdAt).fromNow()}`}
            </Typography>
          </div>
          {/* <div className={classes.badge}>
            <SvgIcon
              fontSize="small"
              className={classes.badgeIcon}
            >
              <DollarSignIcon />
            </SvgIcon>
            <Typography
              variant="body2"
              color="inherit"
              component="span"
            >
              {`Budget: ${project.price}`}
            </Typography>
          </div> */}
        </Box>
      </Grid>
      <Grid item>
        <ButtonShare className={classes.action}>
          <SvgIcon fontSize="small" className={classes.actionIcon}>
            <ShareIcon />
          </SvgIcon>
          Share
        </ButtonShare>
        {user && user.idRole === 'STUDENT' ? (
          ''
        ) : (
          <ButtonUpload
            className={classes.action}
            onClick={handleApplicationOpen}
            variant="contained"
            color="secondary"
            disabled={haveAVideo}
          >
            <SvgIcon fontSize="small" className={classes.actionIcon}>
              <SendIcon />
            </SvgIcon>
            UPLOAD VIDEO
          </ButtonUpload>
        )}

        <Application
          author={project.author}
          onApply={handleApplicationClose}
          onClose={handleApplicationClose}
          open={openApplication}
          createOneMedia={createOneMedia}
          data={data}
          setUpload={setUpload}
        />
      </Grid>
    </Grid>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

Header.defaultProps = {};

export default Header;

const TypographyCustom = styled(Typography)`
  &.MuiTypography-colorTextPrimary {
    color: #000;
  }
`;

const ButtonShare = styled(Button)`
  .MuiButton-label {
    color: #000;
  }
`;

const ButtonUpload = styled(Button)`
  &.MuiButton-contained.Mui-disabled {
    background-color: #000;
  }

  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;