/* eslint-disable react/no-array-index-key */
import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useDropzone } from 'react-dropzone';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  IconButton,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Typography,
  makeStyles
} from '@material-ui/core';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import MoreIcon from '@material-ui/icons/MoreVert';
import bytesToSize from 'src/utils/bytesToSize';
import Axios, { post } from 'axios';
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo'

const useStyles = makeStyles((theme) => ({
  root: {},
  dropZone: {
    border: `1px dashed ${theme.palette.divider}`,
    padding: theme.spacing(6),
    outline: 'none',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      opacity: 0.5,
      cursor: 'pointer'
    }
  },
  dragActive: {
    backgroundColor: theme.palette.action.active,
    opacity: 0.5
  },
  image: {
    width: 130
  },
  info: {
    marginTop: theme.spacing(1)
  },
  list: {
    maxHeight: 320
  },
  actions: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'flex-end',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}));


const GET_ONE_CONTENT_LESSON = gql`
query findManyContentLesson($filterContentLesson: FilterContentLesson){
  findManyContentLesson(filterContentLesson: $filterContentLesson){
    _id
    idLesson
    title
    idMedia
    isActive
    createdAt
    updatedAt
    createdBy
    updatedBy
  }
}
`


function FilesDropzone({ idLesson, setUpload, isSubmit, setIsSubmit, idMedia, setIdMedia, data, createOneMedia, setIdImage, idImage, className, ...rest }) {
  const classes = useStyles();
  const [files, setFiles] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  
  const { data: contentLesson, refetch: refetchContentLeson } = useQuery(GET_ONE_CONTENT_LESSON, {
    variables: {
      filterContentLesson: {
        idLesson: idLesson
      }
    }
  })

  const handleDrop = useCallback((acceptedFiles) => {
    setFiles((prevFiles) => [...prevFiles].concat(acceptedFiles));
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop
  });

  const handleUploadFile = () => {
    if (files.length === 0) {
      enqueueSnackbar('Upload video has been error!', {
        variant: 'error'
      })
      setIsSubmit(false)
    } else {
      const formData = new FormData();
      formData.append('file', files[0])
      console.log(formData)
      Axios({
        method: 'POST',
        url: 'http://localhost:16043/upload-file/video',
        headers: {
          'content-type': 'multipart/form-data'
        },
        data: formData
      }).then(async res => {
        let id = res.data.slice(15)
        setIdImage(id)
        console.log(id)
        createOneMedia({
          variables: {
            inputMedia: {
              title: data && data.title,
              source: 'http://localhost:16043/videos/' + id,
              type: 'VIDEO'
            }
          }
        }).then(async res => {
          console.log(res)
          setIsSubmit(true)
          await setIdMedia(res.data.createOneMedia)
          console.log(idMedia)
          refetchContentLeson()
          setUpload(true)

        })
          .catch(err => console.log(err))
      })
        .catch(err => console.log(err))
    }
  }

  const handleDeleteFile = () => {
    setFiles([])
    console.log(idImage)
    Axios({
      method: 'get',
      url: 'http://localhost:16043/upload-file/remove-video/' + idImage
    }).then(res => {
      console.log(res)
      setFiles([])
    })
      .catch(err => console.log(err))
  }


  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <div
        className={clsx({
          [classes.dropZone]: true,
          [classes.dragActive]: isDragActive
        })}
        {...getRootProps()}
      >
        <input {...getInputProps()} />
        <div>
          <img
            alt="Select file"
            className={classes.image}
            src="/static/images/undraw_add_file2_gvbb.svg"
          />
        </div>
        <div>
          <Typography
            gutterBottom
            variant="h3"
            style={{ color: '#FFF' }}
          >
            Select video to upload
          </Typography>
          <Box mt={2}>
            <Typography
              color="textPrimary"
              variant="body1"
            >
              Drop video here or click
              {' '}
              <Link underline="always">browse</Link>
              {' '}
              thorough your machine
            </Typography>
          </Box>
        </div>
      </div>
      {files.length > 0 && (
        <>
          <PerfectScrollbar options={{ suppressScrollX: true }}>
            <List className={classes.list}>
              {files.map((file, i) => (
                <ListItem
                  divider={i < files.length - 1}
                  key={i}
                >
                  <ListItemIcon>
                    <FileCopyIcon />
                  </ListItemIcon>
                  <ListItemText
                    primary={file.name}
                    primaryTypographyProps={{ variant: 'h5' }}
                    secondary={bytesToSize(file.size)}
                  />
                  <Tooltip title="More options">
                    <IconButton edge="end">
                      <MoreIcon />
                    </IconButton>
                  </Tooltip>
                </ListItem>
              ))}
            </List>
          </PerfectScrollbar>
          {
            isSubmit === false ? <div className={classes.actions}>
              <Button
                onClick={() => handleDeleteFile()}
                size="small"
              >
                Remove all
           </Button>
              <Button
                color="secondary"
                size="small"
                variant="contained"
                onClick={() => {
                  handleUploadFile()
                }}
              >
                Upload files
           </Button>
            </div> : ''
          }
        </>
      )}
    </div>
  );
}

FilesDropzone.propTypes = {
  className: PropTypes.string
};

export default FilesDropzone;
