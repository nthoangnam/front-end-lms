import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import { Formik } from 'formik';
import { useQuery, useMutation } from 'react-apollo';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  FormControlLabel,
  FormHelperText,
  Grid,
  Paper,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import QuillEditor from 'src/components/QuillEditor';
import FilesDropzone from './FilesDropzone';
import gql from 'graphql-tag';
import styled from '@emotion/styled'

const useStyles = makeStyles(() => ({
  root: {},
  editor: {
    background: '#fff',
    '& .ql-editor': {
      height: 400,
      color: '#000!important',
    },

    '& .ql-picker-label': {
      color: '#000',
    },

    '& .ql-toolbar': {
      borderBottom: '1px solid #000',
    },

    '& svg': {
      filter: 'invert(1)'
    }
  }
}));

const CREATE_A_COURSE = gql`
  mutation createOneCourse($inputCourse: InputCourse!) {
    createOneCourse(inputCourse: $inputCourse)
  }
`;
const FIND_MANY_COURSE = gql`
  {
    findManyCourse {
      _id
      description
      name
      credit
      status
      title
      isActive
      final
      mid
      present
      urlImage
      idTeacher
    }
  }
`;

const FIND_MAYNY_DEPARTMENT = gql`
  query findManyDepartment($filterDepartment: FilterDepartment) {
    findManyDepartment(filterDepartment: $filterDepartment) {
      _id
      name
      description
      code
      numOfYear
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;
const GET_ALL_USER = gql`
  {
    findManyUser {
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`;

function CourseEditForm({ className, ...rest }) {
  const classes = useStyles();
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [createOneCourse] = useMutation(CREATE_A_COURSE);
  const [loading, setLoading] = useState(false);
  const { data: allCourse, refetch: refetchCourse } = useQuery(
    FIND_MANY_COURSE,
    {
      fetchPolicy: 'network-only'
    }
  );

  const { data: findManyUser } = useQuery(GET_ALL_USER);
  const [optionTeacher, setOptionTeacher] = useState([]);
  const [selectTeacher, setSelectTeacher] = useState('');
  const [selectOption, setSelectOption] = useState([]);
  const [sort, setSort] = useState('');

  const [imgUrl, setImgUrl] = useState('');

  useEffect(() => {
    if (selectOption.length > 0) {
      setSort(selectOption[0].value);
    }
  }, [selectOption]);

  useEffect(() => {
    console.log(optionTeacher && optionTeacher);
    if (optionTeacher.length > 0) {
      setSelectTeacher(optionTeacher[0].value);
    }
  }, [optionTeacher]);

  useEffect(() => {
    let array = [];
    console.log(findManyUser && findManyUser);
    findManyUser &&
      findManyUser.findManyUser.map((item, key) => {
        if (item.idRole === 'LECTURER') {
          const newItem = {
            value: item._id,
            label: item.username
          };
          array = [...array, newItem];
          setOptionTeacher(array);
        }
      });
    console.log(sort);
  }, [findManyUser]);

  const [idImage, setIdImage] = useState('');

  const { data: manyDepartment } = useQuery(FIND_MAYNY_DEPARTMENT);

  useEffect(() => {
    let array = [];
    manyDepartment &&
      manyDepartment.findManyDepartment.map((item, key) => {
        const newItem = {
          value: item._id,
          label: item.name
        };
        array = [...array, newItem];
        setSelectOption(array);
      });
  }, [manyDepartment]);

  const handleSortChange = event => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSortChangeTeacher = event => {
    event.persist();
    setSelectTeacher(event.target.value);
  };

  useEffect(() => {
    console.log(selectTeacher);
  }, [selectTeacher]);

  useEffect(() => {
    console.log(idImage);
  }, [idImage]);

  return (
    <Formik
      initialValues={{
        name: '',
        description: '',
        images: [],
        title: '',
        department: sort,
        teacher: selectTeacher,
        credit: 0
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string()
          .max(255)
          .required(),
        description: Yup.string()
          .max(5000)
          .required(),
        images: Yup.array(),
        title: Yup.string()
          .max(255)
          .required(),
        credit: Yup.string()
          .max(255)
          .required()
        // department: Yup.string().max(255).required(),
        // teacher: Yup.string().max(255).required()
      })}
      onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        try {
          // Do api call
          console.log(values);
          console.log('http://localhost:16043/images/' + idImage);
          const {
            name,
            description,
            title,
            credit,
            teacher,
            department
          } = values;
          setLoading(true);
          if (sort === '' || selectTeacher === '') {
            enqueueSnackbar('Teacher and Department can not null!', {
              variant: 'error'
            });
            setLoading(false);
          } else
            createOneCourse({
              variables: {
                inputCourse: {
                  name,
                  description,
                  title,
                  credit: parseFloat(credit),
                  idDepartment: sort,
                  idTeacher: selectTeacher,
                  urlImage: 'http://localhost:16043/images/' + idImage
                }
              },
              refetchQueries: [{ query: FIND_MANY_COURSE }]
            })
              .then(res => {
                if (res && res.data.createOneCourse !== null) {
                  refetchCourse();
                  console.log(allCourse && allCourse);
                  enqueueSnackbar('Course created successfully!', {
                    variant: 'success'
                  });
                  setLoading(false);
                  history.push('/app/courseManagement');
                }
              })
              .catch(err => console.log(err));
        } catch (err) {
          console.log(err);
          setErrors({ submit: err.message });
          setStatus({ success: false });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
        touched,
        values
      }) => (
        <form
          onSubmit={handleSubmit}
          className={clsx(classes.root, className)}
          {...rest}
        >
          <Grid container spacing={3}>
            <Grid item xs={12} lg={12}>
              <Card>
                <CardContentCustom>
                  <TextFieldCustom
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Course name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name}
                    variant="outlined"
                  />
                  <Box mt={3} mb={1}>
                    <TypographyCustom variant="subtitle2" color="textSecondary">
                      Description
                    </TypographyCustom>
                  </Box>
                  <Paper variant="outlined">
                    <QuillEditor
                      className={classes.editor}
                      value={values.description}
                      onChange={value => setFieldValue('description', value)}
                    />
                  </Paper>
                  {touched.description && errors.description && (
                    <Box mt={2}>
                      <FormHelperText error>
                        {errors.description}
                      </FormHelperText>
                    </Box>
                  )}
                </CardContentCustom>
              </Card>
              <Box mt={3}>
                <Card>
                  <CardHeaderCustom title="Upload Images" />
                  <Divider />
                  <CardContentCustom>
                    <FilesDropzone setIdImage={setIdImage} idImage={idImage} />
                  </CardContentCustom>
                </Card>
              </Box>
              <Box mt={3}>
                <TextField
                  error={Boolean(touched.title && errors.title)}
                  fullWidth
                  helperText={touched.title && errors.title}
                  label="Title"
                  name="title"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.title}
                  variant="outlined"
                />
              </Box>
              <Box mt={3}>
                <TextField
                  error={Boolean(touched.department && errors.department)}
                  helperText={touched.department && errors.department}
                  fullWidth
                  label="Department"
                  name="department"
                  onBlur={handleBlur}
                  onChange={e => handleSortChange(e)}
                  select
                  SelectProps={{ native: true }}
                  variant="outlined"
                  value={sort}
                >
                  {selectOption.map(option => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Box>

              <Box mt={3}>
                <TextField
                  error={Boolean(touched.teacher && errors.teacher)}
                  helperText={touched.teacher && errors.teacher}
                  fullWidth
                  label="Teacher"
                  name="teacher"
                  onBlur={handleBlur}
                  onChange={e => handleSortChangeTeacher(e)}
                  select
                  SelectProps={{ native: true }}
                  variant="outlined"
                  value={selectTeacher}
                >
                  {optionTeacher.map(option => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Box>
              <Box mt={3}>
                <TextField
                  error={Boolean(touched.credit && errors.credit)}
                  helperText={touched.credit && errors.credit}
                  fullWidth
                  label="Credit"
                  name="credit"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.credit}
                  variant="outlined"
                // error={Boolean(touched.title && errors.title)}
                // fullWidth
                // helperText={touched.title && errors.title}
                // label="Title"
                // name="title"
                // onBlur={handleBlur}
                // onChange={handleChange}
                // value={values.title}
                // variant="outlined"
                />
              </Box>
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.status && errors.status)}
                                        fullWidth
                                        helperText={touched.status && errors.status}
                                        label="Status"
                                        name="status"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.status}
                                        variant="outlined"
                                    />
                                </Box> */}
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.year && errors.year)}
                                        fullWidth
                                        helperText={touched.year && errors.year}
                                        label="Year"
                                        name="year"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.year}
                                        variant="outlined"
                                    />
                                </Box> */}
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.final && errors.final)}
                                        fullWidth
                                        helperText={touched.final && errors.final}
                                        label="Final Percent"
                                        name="final"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.final}
                                        variant="outlined"
                                    />
                                </Box> */}
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.mid && errors.mid)}
                                        fullWidth
                                        helperText={touched.mid && errors.mid}
                                        label="Mid Percent"
                                        name="mid"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.mid}
                                        variant="outlined"
                                    />
                                </Box> */}
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.present && errors.present)}
                                        fullWidth
                                        helperText={touched.present && errors.present}
                                        label="Present Percent"
                                        name="present"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.present}
                                        variant="outlined"
                                    />
                                </Box> */}
              {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.quiz && errors.quiz)}
                                        fullWidth
                                        helperText={touched.quiz && errors.quiz}
                                        label="Quiz Percent"
                                        name="quiz"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.quiz}
                                        variant="outlined"
                                    />
                                </Box> */}
            </Grid>
          </Grid>
          {errors.submit && (
            <Box mt={3}>
              <FormHelperText error>{errors.submit}</FormHelperText>
            </Box>
          )}
          <Box mt={2}>
            <Button
              color="secondary"
              variant="contained"
              type="submit"
              disabled={loading}
            >
              Create course
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
}

CourseEditForm.propTypes = {
  className: PropTypes.string
};

export default CourseEditForm;

const TextFieldCustom = styled(TextField)`
  .MuiOutlinedInput-input {
    color: #000;
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: #000;
  }
`;

const CardContentCustom = styled(CardContent)`
  background: #fff;
`;

const TypographyCustom = styled(Typography)`
  color: #000;
`;

const CardHeaderCustom = styled(CardHeader)`
  background: #fff;
  color: #000;
`;