import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import CourseCreateForm from './courseCreateForm';
import Wallpaper from '../../../../assets/img/wallpaper.png';
import styled from '@emotion/styled';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

function CourseCreateView() {
  const classes = useStyles();

  return (
    <PageCustom
      className={classes.root}
      title="Course Create"
    >
      <Container maxWidth="lg">
        <Header />
        <CourseCreateForm />
      </Container>
    </PageCustom>
  );
}

export default CourseCreateView;

const PageCustom = styled(Page)`
  background-image: url(${Wallpaper})
`