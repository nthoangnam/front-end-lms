import React, { useEffect, useState } from 'react';
import './index.scss';
import backgroundConfirm from '../../../assets/img/login2.gif';
import { useHistory, useParams } from 'react-router';
import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import { Alert } from 'rsuite';
import { useSnackbar } from 'notistack';
import styled from '@emotion/styled'

const CONFIRM = gql`
  mutation confirmAccount($password: String!) {
    confirmAccount(password: $password)
  }
`;

const Confirm = props => {
  const { history } = props;
  //   const { history } = useHistory();
  const { token } = useParams();
  const [confirmAccount] = useMutation(CONFIRM, {
    context: {
      headers: {
        'access-token': token
      }
    }
  });
  const { enqueueSnackbar } = useSnackbar();
  useEffect(() => {
    console.log(token);
    localStorage.setItem('token', token);
  }, [token]);

  const [pass, setPass] = useState('');
  const [confirmPass, setConfirmPass] = useState('');

  const handleSubmit = () => {
    if (pass === confirmPass) {
      confirmAccount({
        variables: {
          password: pass
        },
        context: {
          headers: {
            token: token
          }
        }
      })
        .then(res => {
          if (res.data.confirmAccount === true) {
            enqueueSnackbar('Verify successfully!', {
              variant: 'success'
            });
            history.push('/login');
          }
        })
        .catch(err => {
          console.log({ err });
        });
    } else {
      Alert.warning('Password does not match!');
    }
  };

  return (
    <>
      <div className="confirm">
        <div className="confirm-content">
          <Wrapper>
            <div className="confirm-password">Create New Password</div>
            <input
              placeholder="New password"
              onChange={e => setPass(e.target.value)}
              value={pass}
            />
            <input
              placeholder="Confirm password"
              onChange={e => setConfirmPass(e.target.value)}
              value={confirmPass}
            />
            <button onClick={() => handleSubmit()}>Submit</button>
          </Wrapper>
        </div>
      </div>
    </>
  );
};

export default Confirm;

const Wrapper = styled.div`
  border: 1px solid #000;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 70%;
  background-color: #fff;
  height: 60%;
  border-radius: 5px;
`