import {
  Container,
  Grid,
  makeStyles,
  Button,
  SvgIcon
} from '@material-ui/core';
import React, { useState } from 'react';
import Page from 'src/components/Page';
import Header from './Header';
import TodaysMoney from './Item';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import { Link as RouterLink } from 'react-router-dom';
import { PlusCircle as PlusCircleIcon } from 'react-feather';
import clsx from 'clsx';
import Application from './Application';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'
import Wallpaper from '../../../assets/img/wallpaper.png'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    // paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  container: {
    paddingTop: theme.spacing(3),
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 64,
      paddingRight: 64
    }
  }
}));

const GET_ALL_CATEGORIES = gql`
  query findManyDepartment($filterDepartment: FilterDepartment) {
    findManyDepartment(filterDepartment: $filterDepartment) {
      _id
      name
      description
      code
      numOfYear
      isActive
      createdAt
      updatedAt
      createdBy
      updatedBy
    }
  }
`;

function DashboardView({ className, ...rest }) {
  const classes = useStyles();
  const { user } = useSelector(state => state.account);
  const { data: allCategories, refetch: refetchAll } = useQuery(
    GET_ALL_CATEGORIES
  );

  const [openApplication, setOpenApplication] = useState(false);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };
  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  console.log(allCategories);

  return (
    <PageCustom className={classes.root} title="Catagories">
      <img src={Wallpaper} alt="background" />
      <Container maxWidth={false} className={classes.container}>
        <Grid
          className={clsx(classes.root, className)}
          container
          justify="space-between"
          spacing={2}
          {...rest}
        >
          <Header />
          <Grid item>
            {(user && user.idRole === 'STUDENT') ||
              (user && user.idRole === 'LECTURER') ? (
              ''
            ) : (
              <ButtonCustom
                component={RouterLink}
                color="secondary"
                variant="contained"
                className={classes.action}
                onClick={handleApplicationOpen}
              >
                <SvgIcon fontSize="small" className={classes.actionIcon}>
                  <PlusCircleIcon />
                </SvgIcon>
                New Item
              </ButtonCustom>
            )}
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          {allCategories &&
            allCategories.findManyDepartment.map((item, key) => {
              return (
                <GridCustom item lg={3} sm={6} xs={12} key={key}>
                  <TodaysMoney
                    item={item}
                    GET_ALL_CATEGORIES={GET_ALL_CATEGORIES}
                    refetch={refetchAll}
                  />
                </GridCustom>
              );
            })}
        </Grid>
      </Container>
      <Application
        onApply={handleApplicationClose}
        onClose={handleApplicationClose}
        open={openApplication}
        refetch={refetchAll}
      />
    </PageCustom>
  );
}

export default DashboardView;

const PageCustom = styled(Page)`
  position: relative;
  img {
    position: absolute;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`

const GridCustom = styled(Grid)`
  z-index: 2;
`;

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`;