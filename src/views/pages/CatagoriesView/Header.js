import { Breadcrumbs, Grid, Link, makeStyles, Typography } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const useStyles = makeStyles((theme) => ({
  root: {
    '& a': {
      color: '#fff'
    }
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

function Header({ className, ...rest }) {
  const classes = useStyles();
  const { user } = useSelector(state => state.account);
  return (
    <Wrapper
      className={clsx(classes.root, className)}
      {...rest}
    >
      <BreadcrumbsCustom
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        <Link color="inherit" to="/app" component={RouterLink}>
          Dashboard
        </Link>
        <Typography color="textPrimary">

        </Typography>
      </BreadcrumbsCustom>
      <Typography
        variant="h3"
        color="textPrimary"
      >
        Categories Management
      </Typography>
    </Wrapper>
  );
}

Header.propTypes = {
  className: PropTypes.string
};

export default Header;

const Wrapper = styled.div`
  z-index: 10;
`

const BreadcrumbsCustom = styled(Breadcrumbs)`
  color: #fff;
`