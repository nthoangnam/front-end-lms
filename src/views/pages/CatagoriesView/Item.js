import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  Typography,
  makeStyles,
  Button
} from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/Book';
import Label from 'src/components/Label';
import { useSnackbar } from 'notistack';
import { useMutation, Query } from 'react-apollo';
import gql from 'graphql-tag';
import DeleteModal from './deleteConfirm';
import Edit from './editModal';
import { useSelector } from 'react-redux';
import styled from '@emotion/styled'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    background: '#fff'
  },
  label: {
    marginLeft: theme.spacing(1)
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    height: 48,
    width: 48
  }
}));

const DELETE_ITEM = gql`
  mutation deleteOneDepartment($idDepartment: ID!) {
    deleteOneDepartment(idDepartment: $idDepartment)
  }
`;

function TodaysMoney({
  item,
  GET_ALL_CATEGORIES,
  refetch,
  className,
  ...rest
}) {
  const classes = useStyles();
  const data = {
    value: '24,000',
    currency: '$',
    difference: 4
  };

  const { user } = useSelector(state => state.account);
  const { enqueueSnackbar } = useSnackbar();

  const [deleteOneDepartment] = useMutation(DELETE_ITEM);

  const [openApplication, setOpenApplication] = useState(false);
  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const [openEdit, setOpenEdit] = useState(false);
  const handleEditOpen = () => {
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const handleDelete = () => {
    deleteOneDepartment({
      variables: {
        idDepartment: item._id
      },
      refetchQueries: [{ query: GET_ALL_CATEGORIES }]
    })
      .then(res => {
        if (res && res.data.deleteOneDepartment !== null) {
          enqueueSnackbar('Item delete successfully!', {
            variant: 'success'
          });
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Box flexGrow={1}>
        <TypographyCustom
          component="h3"
          gutterBottom
          variant="overline"
          color="textSecondary"
        >
          categories
        </TypographyCustom>
        <Box display="flex" alignItems="center" flexWrap="wrap">
          <TypographyCustom variant="h3" color="textPrimary">
            {/* {data.currency} */}
            {/* {data.value} */}
            {item.name}
          </TypographyCustom>
          {/* <Label
            className={classes.label}
            color={data.difference > 0 ? 'success' : 'error'}
          >
            {data.difference > 0 ? '+' : ''}
            {data.difference}
            %
          </Label> */}
        </Box>
      </Box>
      {(user && user.idRole === 'STUDENT') ||
        (user && user.idRole === 'LECTURER') ? (
        ''
      ) : (
        <>
          {' '}
          <Avatar className={classes.avatar} onClick={() => handleEditOpen()}>
            <AttachMoneyIcon />
          </Avatar>
          <ButtonCustom
            style={{ marginLeft: '1rem' }}
            onClick={() => handleApplicationOpen()}
          >
            Delete
          </ButtonCustom>
        </>
      )}

      <DeleteModal
        onApply={handleApplicationClose}
        onClose={handleApplicationClose}
        open={openApplication}
        onDelete={handleDelete}
      />
      <Edit
        onApply={handleEditClose}
        onClose={handleEditClose}
        open={openEdit}
        item={item}
        refetch={refetch}
      />
    </Card>
  );
}

TodaysMoney.propTypes = {
  className: PropTypes.string
};

export default TodaysMoney;

const TypographyCustom = styled(Typography)`
  color: #000;
`

const ButtonCustom = styled(Button)`
  color: #000;
`