import { Box, Button, FormHelperText, Grid, makeStyles, TextField, Drawer } from '@material-ui/core';
import clsx from 'clsx';
import { Formik } from 'formik';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-apollo';
import { useHistory } from 'react-router';
import * as Yup from 'yup';
import { client } from 'src/utils/apollo'
import styled from '@emotion/styled'
import Wallpaper from '../../../../assets/img/wallpaper.png'

const useStyles = makeStyles(() => ({
  root: {},
  editor: {
    '& .ql-editor': {
      height: 400
    }
  },
  drawerDesktopRoot: {
    width: window.innerWidth,
  },
  drawerDesktopPaper: {
    position: 'relative',
    padding: 100
  },
  drawerMobilePaper: {
    position: 'relative',
    width: 280
  },
  drawerMobileBackdrop: {
    position: 'absolute'
  }
}));
const roleOption = [
  {
    label: 'STUDENT',
    value: 'STUDENT'
  },
  {
    label: 'TEACHER',
    value: 'TEACHER'
  }
]
const UPDATE_USER = gql`
mutation updateOneUser($userId: ID!, $update: UpdateUserInput! ){
    updateOneUser(userId:$userId, update:$update)
  }
`
const GET_ONE_USER = gql`
  query findOneUser($userId: ID!){
      findOneUser(userId: $userId){
        _id,
        idRole,
        idDepartment,
        idSponsor,
        username,
        urlAvatar,
        firstname,
        lastname,
        email,
        numberphone,
        address,
        isVerified,
        isLocked,
        createdAt,
        createdBy,
        updatedAt,
        updatedBy,
      }
  }
`

const GET_ALL_USER = gql`
{
    findManyUser{
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`


function TeacherEditForm(props, { className, ...rest }) {
  const { open, setOpen, data } = props

  const classes = useStyles();
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar()
  const [updateOneUser] = useMutation(UPDATE_USER)

  // const { data } = useQuery(GET_ONE_USER, {
  //     variables: {
  //         userId: history.location.pathname.slice(28)
  //     }
  // })
  // const [dataEdit, setDataEdit] = useState(data)
  // useEffect(() => {
  //     setDataEdit(data)
  // }, [data])

  // useEffect(() => {
  //     setDataEdit(data)
  // }, [dataEdit])

  // console.log(dataEdit)

  return (
    <DrawerCustom
      anchor='right'
      open={open}
      classes={{
        root: classes.drawerDesktopRoot,
        paper: classes.drawerDesktopPaper
      }}
    >
      <Formik
        initialValues={{
          username: data.username,
          idDepartment: data.idDepartment,
          idSponsor: data.idSponsor,
          password: data.password,
          urlAvatar: data.urlAvatar,
          firstname: data.firstname,
          lastname: data.lastname,
          email: data.email,
          numberphone: data.numberphone,
          address: data.address,
          idRole: data.idRole
        }}
        validationSchema={Yup.object().shape({
          username: Yup.string().max(255),
          idDepartment: Yup.string().max(255),
          idSponsor: Yup.string().max(255),
          password: Yup.string().max(255),
          urlAvatar: Yup.string().max(255),
          firstname: Yup.string().max(255),
          lastname: Yup.string().max(255),
          email: Yup.string().max(255),
          numberphone: Yup.string().max(255),
          address: Yup.string().max(255),
          idRole: Yup.string().max(255)
        })}
        onSubmit={async (values, {
          setErrors,
          setStatus,
          setSubmitting
        }) => {
          try {
            // Do api call
            console.log('okok')
            const { username, idDepartment, idSponsor, password, urlAvatar, firstname, lastname, email, numberphone, address, idRole } = values
            updateOneUser({
              variables: {
                update: {
                  username,
                  idDepartment,
                  idSponsor,
                  password,
                  urlAvatar,
                  firstname,
                  lastname,
                  email,
                  numberphone,
                  address,
                  idRole: 'LECTURER'
                },
                userId: data._id
              },
              refetchQueries: [{ query: GET_ALL_USER }]
            }).then(res => {
              console.log(res)
              if (res && res.data.updateOneUser !== null) {
                enqueueSnackbar('Update teacher successfully!', {
                  variant: 'success'
                })
                history.push('/app/teacherManagement')
                setOpen(false)
              }
            })
              .catch(err => console.log(err))
          } catch (err) {
            setErrors({ submit: err.message });
            setStatus({ success: false });
            setSubmitting(false);
          }
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue,
          touched,
          values
        }) => (
          <form
            onSubmit={handleSubmit}
            className={clsx(classes.root, className)}
            {...rest}
          >
            <Grid
              container
              spacing={3}
            >
              <Grid
                item
                xs={12}
                lg={12}
              >
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.username && errors.username)}
                    fullWidth
                    helperText={touched.username && errors.username}
                    label="Username"
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.username}
                    variant="outlined"
                  />
                </Box>
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.idDepartment && errors.idDepartment)}
                                            fullWidth
                                            helperText={touched.idDepartment && errors.idDepartment}
                                            label="Department"
                                            name="idDepartment"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.idDepartment}
                                            variant="outlined"
                                        />
                                    </Box> */}
                {/* <Box mt={3}>
                                        <TextField
                                            error={Boolean(touched.idSponsor && errors.idSponsor)}
                                            fullWidth
                                            helperText={touched.idSponsor && errors.idSponsor}
                                            label="Sponsor"
                                            name="idSponsor"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.idSponsor}
                                            variant="outlined"
                                        />
                                    </Box> */}
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.urlAvatar && errors.urlAvatar)}
                    fullWidth
                    helperText={touched.urlAvatar && errors.urlAvatar}
                    label="URL Avatar"
                    name="urlAvatar"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.urlAvatar}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.firstname && errors.firstname)}
                    fullWidth
                    helperText={touched.firstname && errors.firstname}
                    label="Firstname"
                    name="firstname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.firstname}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.lastname && errors.lastname)}
                    fullWidth
                    helperText={touched.lastname && errors.lastname}
                    label="Lastname"
                    name="lastname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.lastname}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.email && errors.email)}
                    fullWidth
                    helperText={touched.email && errors.email}
                    label="Email"
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.email}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.numberphone && errors.numberphone)}
                    fullWidth
                    helperText={touched.numberphone && errors.numberphone}
                    label="Number phone"
                    name="numberphone"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.numberphone}
                    variant="outlined"
                  />
                </Box>
                <Box mt={3}>
                  <TextField
                    error={Boolean(touched.address && errors.address)}
                    fullWidth
                    helperText={touched.address && errors.address}
                    label="Address"
                    name="address"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.address}
                    variant="outlined"
                  />
                </Box>
                {/* <Box mt={3}>
                                    <TextField
                                        error={Boolean(touched.quiz && errors.quiz)}
                                        fullWidth
                                        helperText={touched.quiz && errors.quiz}
                                        label="Role"
                                        name="idRole"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.quiz}
                                        variant="outlined"
                                    >
                                        {roleOption.map((option) => (
                                            <option
                                                key={option.value}
                                                value={option.value}
                                            >
                                                {option.label}
                                            </option>
                                        ))}
                                    </TextField>
                                </Box> */}
              </Grid>
            </Grid>
            {errors.submit && (
              <Box mt={3}>
                <FormHelperText error>
                  {errors.submit}
                </FormHelperText>
              </Box>
            )}
            <Box mt={2}>
              <Button
                color="secondary"
                variant="contained"
                type="submit"
                disabled={isSubmitting}
              >
                Submit
              </Button>
              <Button
                color="default"
                variant="contained"
                // type="submit"
                // disabled={isSubmitting}
                style={{ marginLeft: '1rem' }}
                onClick={() => setOpen(false)}
              >
                Cancel
              </Button>
            </Box>
          </form>
        )}
      </Formik>
    </DrawerCustom>
  );
}

TeacherEditForm.propTypes = {
  className: PropTypes.string
};

export default TeacherEditForm;

const DrawerCustom = styled(Drawer)`
  .MuiDrawer-paper {
    background-image: url(${Wallpaper});
  }
`;