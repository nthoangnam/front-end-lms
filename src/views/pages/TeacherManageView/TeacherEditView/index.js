import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import TeacherEditForm from './teacherEditForm';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

function TeacherCreateView() {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Teacher Create"
    >
      <Container maxWidth="lg">
        <Header />
        <TeacherEditForm />
      </Container>
    </Page>
  );
}

export default TeacherCreateView;
