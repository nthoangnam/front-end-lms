/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Header from './Header'
import Page from 'src/components/Page'
import { useSnackbar } from 'notistack'
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  Typography,
  makeStyles,
  Container,
  Grid
} from '@material-ui/core';
import {
  PlusCircle as PlusCircleIcon
} from 'react-feather';
import {
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import getInitials from 'src/utils/getInitials';
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo';
import DrawerEditForm from './TeacherEditView/teacherEditForm'
import styled from '@emotion/styled'
import Wallpaper from '../../../assets/img/wallpaper.png'
import { useSelector } from 'react-redux';

const tabs = [
  {
    value: 'all',
    label: 'All'
  },
  // {
  //     value: 'acceptsMarketing',
  //     label: 'Accepts Marketing'
  // },
  // {
  //     value: 'isProspect',
  //     label: 'Prospect'
  // },
  // {
  //     value: 'isReturning',
  //     label: 'Returning'
  // }
];


const sortOptions = [
  {
    value: 'updatedAt|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updatedAt|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  }
];

const GET_ALL_USER = gql`
{
    findManyUser{
      _id
      idRole
      idDepartment
      idSponsor
      username
      urlAvatar
      firstname
      lastname
      email
      numberphone
      address
      isVerified
      isLocked
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`


function applyFilters(customers, query, filters) {
  return customers.filter((customer) => {
    let matches = true;

    if (query) {
      const properties = ['email', 'username'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (customer[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && customer[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(customers, page, limit) {
  return customers.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(customers, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = customers.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {
    // backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    // paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    // marginTop: 20
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  container: {
    paddingTop: theme.spacing(3),
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 64,
      paddingRight: 64
    }
  },
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  drawerDesktopRoot: {
    width: window.innerWidth,
  },
  drawerDesktopPaper: {
    position: 'relative',
    padding: 100
  },
  drawerMobilePaper: {
    position: 'relative',
    width: 280
  },
  drawerMobileBackdrop: {
    position: 'absolute'
  }
}));

const DELETE_USER = gql`
mutation deleteOneUser($userId: ID!){
    deleteOneUser(userId: $userId)
  }
`

function TeacherManageView({ history, className, ...rest }) {
  const { enqueueSnackbar } = useSnackbar()
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedCustomers, setSelectedCustomers] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const { user: user1 } = useSelector(state => state.account);


  const [deleteOneUser] = useMutation(DELETE_USER)

  const [open, setOpen] = useState(false)
  const [user, setUser] = useState({})
  //useQuery

  const { data: allUsers } = useQuery(GET_ALL_USER)
  const customers = allUsers && allUsers.findManyUser || []

  const deleteUser = (_id) => {
    deleteOneUser({
      variables: {
        userId: _id
      },
      refetchQueries: [{ query: GET_ALL_USER }]
    }).then(res => {
      console.log(res)
      enqueueSnackbar('Deleted teacher successfully!', {
        variant: 'success'
      })
    })
      .catch(err => console.log(err))
  }

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedCustomers([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllCustomers = (event) => {
    setSelectedCustomers(event.target.checked
      ? customers.map((customer) => customer.id)
      : []);
  };

  const handleSelectOneCustomer = (event, customerId) => {
    if (!selectedCustomers.includes(customerId)) {
      setSelectedCustomers((prevSelected) => [...prevSelected, customerId]);
    } else {
      setSelectedCustomers((prevSelected) => prevSelected.filter((id) => id !== customerId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  // Usually query is done on backend with indexing solutions

  const filteredCustomers = applyFilters(customers, query, filters);
  const sortedCustomers = applySort(filteredCustomers, sort);
  const paginationAllUsers = applyPagination(sortedCustomers, page, limit);
  const enableBulkOperations = selectedCustomers.length > 0;
  const selectedSomeCustomers = selectedCustomers.length > 0 && selectedCustomers.length < customers.length;
  const selectedAllCustomers = selectedCustomers.length === customers.length;

  return (
    <>
      <PageCustom
        className={classes.root}
        title="Teacher Management"
      >
        <img src={Wallpaper} alt="background" />
        <Container
          maxWidth={false}
          className={classes.container}>
          <Grid
            className={clsx(classes.root, className)}
            container
            justify="space-between"
            spacing={2}
            {...rest}
          >
            <Header />
            {user1.idRole === 'ADMIN' && (
              <Grid item>
                <ButtonCustom
                  component={RouterLink}
                  color="secondary"
                  variant="contained"
                  className={classes.action}
                  to="/app/teacherManagement/create"
                >
                  <SvgIcon
                    fontSize="small"
                    className={classes.actionIcon}
                  >
                    <PlusCircleIcon />
                  </SvgIcon>
                  New Teacher
                </ButtonCustom>
              </Grid>
            )}
          </Grid>
          <Card
            className={clsx(classes.root, className)}
            {...rest}
          >
            <Tabs
              onChange={handleTabsChange}
              scrollButtons="auto"
              textColor="secondary"
              value={currentTab}
              variant="scrollable"
            >
              {tabs.map((tab) => (
                <Tab
                  key={tab.value}
                  value={tab.value}
                  label={tab.label}
                />
              ))}
            </Tabs>
            <Divider />
            <Box
              p={2}
              minHeight={56}
              display="flex"
              alignItems="center"
            >
              <TextField
                className={classes.queryField}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                onChange={handleQueryChange}
                placeholder="Search user"
                value={query}
                variant="outlined"
              />
              <Box flexGrow={1} />
              <TextField
                label="Sort By"
                name="sort"
                onChange={handleSortChange}
                select
                SelectProps={{ native: true }}
                value={sort}
                variant="outlined"
              >
                {sortOptions.map((option) => (
                  <option
                    key={option.value}
                    value={option.value}
                  >
                    {option.label}
                  </option>
                ))}
              </TextField>
            </Box>
            {enableBulkOperations && (
              <div className={classes.bulkOperations}>
                <div className={classes.bulkActions}>
                  <Checkbox
                    checked={selectedAllCustomers}
                    indeterminate={selectedSomeCustomers}
                    onChange={handleSelectAllCustomers}
                  />
                  <Button
                    variant="outlined"
                    className={classes.bulkAction}
                  >
                    Delete
                  </Button>
                  <Button
                    variant="outlined"
                    className={classes.bulkAction}
                  >
                    Edit
                  </Button>
                </div>
              </div>
            )}
            <PerfectScrollbar>
              <Box minWidth={700}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={selectedAllCustomers}
                          indeterminate={selectedSomeCustomers}
                          onChange={handleSelectAllCustomers}

                        />
                      </TableCell>
                      <TableCell>
                        Name
                      </TableCell>
                      <TableCell>
                        Role
                      </TableCell>
                      <TableCell>
                        Number Phone
                      </TableCell>
                      <TableCell>
                        Address
                      </TableCell>
                      {user1.idRole === 'ADMIN' && (
                        <TableCell align="right">
                          Actions
                        </TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {allUsers && allUsers.findManyUser.filter(item => item.idRole == 'LECTURER')
                      .map((user) => {
                        const isCustomerSelected = selectedCustomers.includes(user._id);

                        return (
                          <TableRow
                            hover
                            key={user._id}
                            selected={isCustomerSelected}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox
                                checked={isCustomerSelected}
                                onChange={(event) => handleSelectOneCustomer(event, user._id)}
                                value={isCustomerSelected}
                                onClick={() => console.log(user)}
                              />
                            </TableCell>
                            <TableCell>
                              <Box
                                display="flex"
                                alignItems="center"
                              >
                                <Avatar
                                  className={classes.avatar}
                                  src={user.urlAvatar}
                                >
                                  {getInitials(user.username)}
                                </Avatar>
                                <div>
                                  <Link
                                    color="inherit"
                                    component={RouterLink}
                                    to="/app/management/customers/1"
                                    variant="h6"
                                  >
                                    {user.username}
                                  </Link>
                                  <Typography
                                    variant="body2"
                                    color="textSecondary"
                                  >
                                    {user.email}
                                  </Typography>
                                </div>
                              </Box>
                            </TableCell>
                            <TableCell>
                              {user.idRole}
                            </TableCell>
                            <TableCell>
                              {user.numberphone}
                            </TableCell>
                            <TableCell>
                              {user.address}
                            </TableCell>

                            {user1.idRole === 'ADMIN' && (
                              <TableCell align="right">
                                <IconButton
                                  component={RouterLink}
                                  // to={`/app/teacherManagement/edit/${user._id}`}
                                  onClick={() => {
                                    setUser(user)
                                    setOpen(true)
                                  }}
                                >
                                  <SvgIcon fontSize="small">
                                    <EditIcon />
                                  </SvgIcon>
                                </IconButton>
                                <IconButton
                                  component={RouterLink}
                                  // to="/app/management/customers/1"
                                  onClick={() => deleteUser(user._id)}
                                >
                                  <SvgIcon fontSize="small">
                                    <ArrowRightIcon />
                                  </SvgIcon>
                                </IconButton>
                              </TableCell>
                            )}
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </Box>
            </PerfectScrollbar>
            <TablePagination
              component="div"
              count={filteredCustomers.length}
              onChangePage={handlePageChange}
              onChangeRowsPerPage={handleLimitChange}
              page={page}
              rowsPerPage={limit}
              rowsPerPageOptions={[5, 10, 25]}
            />
          </Card>
        </Container>
      </PageCustom>
      <DrawerEditForm setOpen={setOpen} open={open} data={user && user} />
    </>
  )
}

TeacherManageView.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array
};

TeacherManageView.defaultProps = {
  customers: []
};

export default TeacherManageView;

const PageCustom = styled(Page)`
  position: relative;
  img {
    position: absolute;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`

const ButtonCustom = styled(Button)`
  &.MuiButton-containedSecondary {
    background-color: #38aae8;
  }
`