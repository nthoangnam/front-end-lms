import React from 'react';
import { makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Hero from './Hero';
import Features from './Features';
import Testimonials from './Testimonials';
import CTA from './CTA';
import FAQS from './FAQS';
import backgroundIndex from '../../../assets/img/login2.gif';
import './index.scss';

const useStyles = makeStyles(() => ({
  root: {}
}));

function HomeView() {
  const classes = useStyles();

  return (
    <Page className={`background-index`} title="Home">
      {/* <img alt="" src={backgroundIndex} style={{ width: '100%' }} /> */}
      <div className="background-index-inside">
        <div>
          <span className="welcome">Welcome</span> to E-Learning
        </div>

        <div className='enjoy'>Enjoy your courses!</div>
      </div>
    </Page>
  );
}

export default HomeView;
