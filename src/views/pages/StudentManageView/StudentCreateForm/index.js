import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import StudentCreateForm from './studentCreateForm';
import styled from '@emotion/styled';
import Wallpaper from '../../../../assets/img/wallpaper.png';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    // paddingTop: theme.spacing(3),
    paddingBottom: 100
  }
}));

function TeacherCreateView() {
  const classes = useStyles();

  return (
    <PageCustom
      className={classes.root}
      title="Product Create"
    >
      <img src={Wallpaper} alt="background" />
      <Container maxWidth="lg">
        <Header />
        <StudentCreateForm />
      </Container>
    </PageCustom>
  );
}

export default TeacherCreateView;

const PageCustom = styled(Page)`
  position: relative;
  img {
    position: absolute;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`