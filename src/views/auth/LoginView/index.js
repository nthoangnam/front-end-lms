import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useHistory } from 'react-router';
import styled from '@emotion/styled'
import Background from 'src/assets/img/bg.jpg'
import {
  Avatar,
  Button,
  Box,
  Container,
  Card,
  CardContent,
  CardMedia,
  Divider,
  Link,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import LockIcon from '@material-ui/icons/Lock';
import Page from 'src/components/Page';
import Logo from 'src/components/Logo';
import LoginForm from './LoginForm';

const useStyles = makeStyles(theme => ({
  root: {
    justifyContent: 'center',
    backgroundColor: theme.palette.background.dark,
    display: 'flex',
    height: '100%',
    minHeight: '100%',
    flexDirection: 'column',
    paddingBottom: 80,
    paddingTop: 80,
    '& img': {
      width: '100%',
      height: '100%',
      position: 'absolute',
      objectFit: 'fill'
    }
  },
  backButton: {
    marginLeft: theme.spacing(2)
  },
  card: {
    overflow: 'visible',
    display: 'flex',
    position: 'relative',
    '& > *': {
      flexGrow: 1,
      flexBasis: '50%',
      width: '50%',
      background: '#fff'
    },

    '& h2': {
      color: '#000'
    },

    '& p': {
      color: '#000'
    },

    '& input': {
      color: '#000'
    }
  },
  content: {
    padding: theme.spacing(8, 4, 3, 4)
  },
  icon: {
    backgroundColor: colors.green[500],
    color: theme.palette.common.white,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    position: 'absolute',
    top: -32,
    left: theme.spacing(3),
    height: 64,
    width: 64
  },
  media: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    padding: theme.spacing(3),
    color: theme.palette.common.white,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  }
}));

function LoginView() {
  const classes = useStyles();
  const history = useHistory();

  const handleSubmitSuccess = () => {
    history.push('/app');
  };

  return (
    <Page className={`${classes.root} bg-login`} title="Login">
      <img src={Background} alt="background" />
      <ContainerCustom maxWidth="md">
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            {/* <Avatar className={classes.icon}>
              <LockIcon fontSize="large" />
            </Avatar> */}
            <WrapTitle>
              <TypographyCustom variant="h2" color="textPrimary">
                Blackboard
              </TypographyCustom>
              <Typography variant="h2" color="textPrimary">
                Login
              </Typography>
            </WrapTitle>
            <Box mt={2}>
              <LoginForm onSubmitSuccess={handleSubmitSuccess} />
            </Box>
            <Box my={2}>
              <Divider />
            </Box>
          </CardContent>
        </Card>
      </ContainerCustom>
    </Page>
  );
}

export default LoginView;

const ContainerCustom = styled(Container)`
  &.MuiContainer-root {
    max-width: 600px;
  }
`;

const WrapTitle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const TypographyCustom = styled(Typography)`
  margin-bottom: 10px;
  font-size: 2rem;
`;
