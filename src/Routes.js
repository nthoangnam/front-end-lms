/* eslint-disable react/no-array-index-key */
import React, { lazy, Suspense, Fragment } from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import LoadingScreen from 'src/components/LoadingScreen';
import AuthGuard from 'src/components/AuthGuard';
import GuestGuard from 'src/components/GuestGuard';

// function DefaultPath() {
//   const { user } = useSelector(state => state.account)
//   return (
//     <>
//       {user ? <Redirect to='/app' /> : <Redirect to='/login' />}
//     </>
//   )
// }

const routesCommon = [
  {
    exact: true,
    path: '/',
    component: () => <Redirect to="/login" />
  },
  {
    exact: true,
    path: '/404',
    component: lazy(() => import('src/views/pages/Error404View'))
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/login',
    component: lazy(() => import('src/views/auth/LoginView'))
  },
  {
    exact: true,
    path: '/login-unprotected',
    component: lazy(() => import('src/views/auth/LoginView'))
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/register',
    component: lazy(() => import('src/views/auth/RegisterView'))
  },
  {
    exact: true,
    path: '/register-unprotected',
    component: lazy(() => import('src/views/auth/RegisterView'))
  },
  {
    exact: true,
    path: '/verify/:token',
    component: lazy(() => import('src/views/pages/ConfirmView'))
  },
  {
    path: '/app',
    guard: AuthGuard,
    layout: DashboardLayout,
    routes: [
      {
        exact: true,
        path: '/app',
        component: () => <Redirect to="/app/home" />
      },
      {
        exact: true,
        path: '/app/courseManagement',
        component: lazy(() => import('src/views/pages/CourseManageView'))
      },
      {
        exact: true,
        path: '/app/courseManagement/create',
        component: lazy(() =>
          import('src/views/pages/CourseManageView/CourseCreateView')
        )
      },
      {
        exact: true,
        path: '/app/teacherManagement',
        component: lazy(() => import('src/views/pages/TeacherManageView'))
      },
      {
        exact: true,
        path: '/app/studentManagement',
        component: lazy(() => import('src/views/pages/StudentManageView'))
      },
      {
        exact: true,
        path: '/app/teacherManagement/create',
        component: lazy(() =>
          import('src/views/pages/TeacherManageView/TeacherCreateView')
        )
      },
      {
        exact: true,
        path: '/app/studentManagement/create',
        component: lazy(() =>
          import('src/views/pages/StudentManageView/StudentCreateForm')
        )
      },
      {
        exact: true,
        path: '/app/courseManagement/:id',
        component: lazy(() =>
          import('src/views/pages/CourseManageView/CourseDetail')
        )
      },
      {
        exact: true,
        path: '/app/courseManagement/:id/:idLesson',
        component: lazy(() => import('src/views/pages/CourseManageView/Lesson'))
      },
      {
        exact: true,
        path: '/app/courseManagement/:id/quiz/:idLesson',
        component: lazy(() =>
          import('src/views/pages/CourseManageView/Assignment')
        )
      },
      {
        exact: true,
        path: '/app/courseManagement/:id/quiz/:idLesson/:idQuiz',
        component: lazy(() =>
          import('src/views/pages/CourseManageView/AssignmentDetail')
        )
      },
      {
        exact: true,
        path: '/app/courseManagement/:id/progress/:idLesson',
        component: lazy(() =>
          import('src/views/pages/CourseManageView/ProgressDetail')
        )
      },
      {
        exact: true,
        path: '/app/categories',
        component: lazy(() => import('src/views/pages/CatagoriesView'))
      },
      {
        exact: true,
        path: '/app/home',
        component: lazy(() => import('src/views/pages/HomeView'))
      }
    ]
  }
];

const renderRoutes = routes =>
  routes ? (
    <Suspense fallback={<LoadingScreen />}>
      <Switch>
        {routes.map((route, i) => {
          const Guard = route.guard || Fragment;
          const Layout = route.layout || Fragment;
          const Component = route.component;

          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={props => (
                <Guard>
                  <Layout>
                    {route.routes ? (
                      renderRoutes(route.routes)
                    ) : (
                      <Component {...props} />
                    )}
                  </Layout>
                </Guard>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  ) : null;

function Routes() {
  return <>{renderRoutes(routesCommon)}</>;
}

export default Routes;
