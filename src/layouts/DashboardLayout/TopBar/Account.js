import React, {
  useRef,
  useState
} from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import styled from '@emotion/styled'
import {
  Avatar,
  Box,
  ButtonBase,
  Hidden,
  Menu,
  MenuItem,
  Typography,
  makeStyles
} from '@material-ui/core';
import { logout } from 'src/actions/accountActions';
import chandung from '../../../assets/img/avatar.png'

const useStyles = makeStyles((theme) => ({
  avatar: {
    height: 32,
    width: 32,
    marginRight: theme.spacing(1)
  },
  popover: {
    width: 200
  }
}));

function Account() {
  const classes = useStyles();
  const history = useHistory();
  const ref = useRef(null);
  const dispatch = useDispatch();
  const account = useSelector((state) => state.account);
  const { enqueueSnackbar } = useSnackbar();
  const [isOpen, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = async () => {
    try {
      handleClose();
      await dispatch(logout());
      history.push('/');
    } catch (error) {
      enqueueSnackbar('Unable to logout', {
        variant: 'error'
      });
    }
  };

  return (
    <>
      <Box
        display="flex"
        alignItems="center"
        component={ButtonBase}
        onClick={handleOpen}
        ref={ref}
      >
        <Avatar
          alt="User"
          className={classes.avatar}
          src={chandung}
        />
        <Hidden smDown>
          <TypographyCusom
            variant="h6"
            color="inherit"
          >
            {`${account.user.firstname} ${account.user.lastname}`}
          </TypographyCusom>
        </Hidden>
      </Box>
      <MenuCustom
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        keepMounted
        PaperProps={{ className: classes.popover }}
        getContentAnchorEl={null}
        anchorEl={ref.current}
        open={isOpen}
      >
        <MenuItemCustom
          component={RouterLink}
          to="/app/home"
        >
          Account
        </MenuItemCustom>
        <MenuItemCustom onClick={handleLogout}>
          Logout
        </MenuItemCustom>
      </MenuCustom>
    </>
  );
}

export default Account;

const TypographyCusom = styled(Typography)`
  color: #000;
`

const MenuItemCustom = styled(MenuItem)`
  color: #000;
`

const MenuCustom = styled(Menu)`
  .MuiPaper-root {
    background-color: #fff;
  }
`