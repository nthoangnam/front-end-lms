/* eslint-disable no-use-before-define */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useLocation, matchPath } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';
import styled from "@emotion/styled"
import {
  Avatar,
  Box,
  Chip,
  Divider,
  Drawer,
  Hidden,
  Link,
  List,
  ListSubheader,
  Typography,
  makeStyles
} from '@material-ui/core';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import {
  Briefcase as BriefcaseIcon,
  Calendar as CalendarIcon,
  ShoppingCart as ShoppingCartIcon,
  Folder as FolderIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  UserPlus as UserPlusIcon,
  Shield as ShieldIcon,
  AlertCircle as AlertCircleIcon,
  Trello as TrelloIcon,
  User as UserIcon,
  Layout as LayoutIcon,
  Edit as EditIcon,
  DollarSign as DollarSignIcon,
  Mail as MailIcon,
  MessageCircle as MessageCircleIcon,
  PieChart as PieChartIcon,
  Share2 as ShareIcon,
  Users as UsersIcon,
  User,
  Book,
  UserCheck
} from 'react-feather';
import Logo from 'src/components/Logo';
import NavItem from './NavItem';
import avatar from '../../../assets/img/avatar.png'

const navConfig = [
  {
    items: [
      {
        title: 'Course Management',
        icon: Book,
        href: '/app/courseManagement'
      },
      {
        title: 'Teacher Management',
        icon: UserCheck,
        href: '/app/teacherManagement'
      },
      {
        title: 'Student Management',
        icon: User,
        href: '/app/studentManagement'
      },
      {
        title: 'Department',
        icon: Book,
        href: '/app/categories'
      }
    ]
  }
];

const student = [
  {
    items: [
      {
        title: 'Course Management',
        icon: Book,
        href: '/app/courseManagement'
      }
    ]
  }
];

const teacher = [
  {
    items: [
      {
        title: 'Course Management',
        icon: Book,
        href: '/app/courseManagement'
      },
      {
        title: 'Teacher Management',
        icon: UserCheck,
        href: '/app/teacherManagement'
      },
      {
        title: 'Student Management',
        icon: User,
        href: '/app/studentManagement'
      }
    ]
  }
];



function renderNavItems({ items, ...rest }) {
  return (
    <List disablePadding>
      {items.reduce(
        (acc, item) => reduceChildRoutes({ acc, item, ...rest }),
        []
      )}
    </List>
  );
}

function reduceChildRoutes({ acc, pathname, item, depth = 0 }) {
  const key = item.title + depth;

  if (item.items) {
    const open = matchPath(pathname, {
      path: item.href,
      exact: false
    });

    acc.push(
      <NavItem
        depth={depth}
        icon={item.icon}
        key={key}
        info={item.info}
        open={Boolean(open)}
        title={item.title}
      >
        {renderNavItems({
          depth: depth + 1,
          pathname,
          items: item.items
        })}
      </NavItem>
    );
  } else {
    acc.push(
      <NavItem
        depth={depth}
        href={item.href}
        icon={item.icon}
        key={key}
        info={item.info}
        title={item.title}
      />
    );
  }

  return acc;
}

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)',
    backgroundColor: '#fff'
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64
  }
}));

function NavBar({ openMobile, onMobileClose }) {
  const classes = useStyles();
  const location = useLocation();
  const { user } = useSelector(state => state.account);
  console.log({ user });
  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line
  }, [location.pathname]);

  const content = (
    <Box height="100%" display="flex" flexDirection="column">
      <PerfectScrollbar options={{ suppressScrollX: true }}>
        <Hidden lgUp>
          <Box p={2} display="flex" justifyContent="center">
            <RouterLink to="/">
              <Logo />
            </RouterLink>
          </Box>
        </Hidden>
        <Box p={2}>
          <Box display="flex" justifyContent="center">
            <RouterLink to="/app/home">
              <Avatar alt="User" className={classes.avatar} src={avatar} />
            </RouterLink>
          </Box>
          <Box mt={2} textAlign="center">
            <LinkCustom
              component={RouterLink}
              to="/app/home"
              variant="h5"
              color="textPrimary"
              underline="none"
            >
              {`${user.firstname} ${user.lastname}`}
            </LinkCustom>
            <Role>{user.idRole}</Role>
            <Typography variant="body2" color="textSecondary">
              {user.bio}
            </Typography>
          </Box>
        </Box>
        <Divider />
        <Box p={2}>
          {user.idRole === 'STUDENT' ? (
            <>
              {
                student.map(config => (
                  <ListCustom
                    key={config.subheader}
                    subheader={
                      <ListSubheader disableGutters disableSticky>
                        {config.subheader}
                      </ListSubheader>
                    }
                  >
                    {renderNavItems({
                      items: config.items,
                      pathname: location.pathname
                    })}
                  </ListCustom>
                ))
              }
            </>
          ) : user.idRole === 'LECTURER' ? (
            <>
              {
                teacher.map(config => (
                  <ListCustom
                    key={config.subheader}
                    subheader={
                      <ListSubheader disableGutters disableSticky>
                        {config.subheader}
                      </ListSubheader>
                    }
                  >
                    {renderNavItems({
                      items: config.items,
                      pathname: location.pathname
                    })}
                  </ListCustom>
                ))
              }
            </>
          ) : (
            <>
              {
                navConfig.map(config => (
                  <ListCustom
                    key={config.subheader}
                    subheader={
                      <ListSubheader disableGutters disableSticky>
                        {config.subheader}
                      </ListSubheader>
                    }
                  >
                    {renderNavItems({
                      items: config.items,
                      pathname: location.pathname
                    })}
                  </ListCustom>
                ))
              }
            </>
          )}

        </Box>
        <Divider />
      </PerfectScrollbar>
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          classes={{ paper: classes.desktopDrawer }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
}

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

export default NavBar;

const LinkCustom = styled(Link)`
  &.MuiTypography-colorTextPrimary {
    color: #000;

    &:hover {
      color: #1675e0;
    }
  }
`

const ListCustom = styled(List)`
  .makeStyles-title-32 {
    color: #000;

    &:hover {
      color: #1675e0;
    }
  }

  .makeStyles-title-30 {
    color: #000;

    &:hover {
      color: #1675e0;
    }
  }
`

const Role = styled.div`
  color: #000;
`;