import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink, split } from 'apollo-link'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { WebSocketLink } from 'apollo-link-ws'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { getMainDefinition } from 'apollo-utilities'
// import crypto from 'crypto'
import { createPersistedQueryLink } from 'apollo-link-persisted-queries'
// import { RetryLink } from 'apollo-link-retry'

import { errorMiddleware } from './middleware'
/* eslint-disable max-len */

const protocol = "http://localhost:16043/graphqlsms".split('://').shift()
const urn = "http://localhost:16043/graphqlsms".split('://').pop()
const httpLink = createPersistedQueryLink().concat(createHttpLink({ uri: "http://localhost:16043/graphqlsms" }))

const wsClient = new SubscriptionClient(
  `${protocol === 'http' ? 'ws' : 'wss'}://${urn}`,
  {
    connectionParams: async () => {
      return {
        'access-token': await localStorage.getItem('accessToken') || ' '
      }
    }
  }
)

const wsLink = new WebSocketLink(wsClient)

const authLink = setContext(async (_, req) => {
  const { headers } = req
  return {
    forceFetch: true,
    headers: {
      ...headers,
      'access-token': await localStorage.getItem('accessToken') || ' '
    }
  }
})

const linkSplit = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  }, wsLink, httpLink
)

const link = ApolloLink.from([
  errorMiddleware,
  linkSplit
])

const client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache()
})

export { client }
