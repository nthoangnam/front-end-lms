import { onError } from 'apollo-link-error'

const errorMiddleware = onError(({
  networkError,
}) => {
  // if (networkError) {
  //   console.error(`[Network Error]: ${response}`)
  // }
})

export { errorMiddleware }
