import React from 'react';
import styled from '@emotion/styled'

function Logo(props) {
  return (
    <Wrapper>
      <img
        alt="Logo"
        src="/static/logo.png"
        {...props}
      />
    </Wrapper>
  );
}

export default Logo;

const Wrapper = styled.div`
  img {
    width: 40px;
    height: 40px;
  }
`